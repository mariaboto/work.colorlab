# Projects

Laboratorium develops projects in collaboration with artists and scientists applying the research from the [color biolab](http://laboratorium.bio/colorlab.html) project.