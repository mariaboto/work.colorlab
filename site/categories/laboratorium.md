# Laboratorium

Laboratorium is the experimental lab for art/design and biotechnology at KASK / School of Arts Ghent. As a biolab in art and design, it focuses on exploring different interactions between art, science and technology.

The lab is located at the Media Arts Studio of KASK, neighboring the Formlab. The Formlab is offering high-end tools and support for 3D additive technologies. Establishing links between the realms of biotechnology and 3D printing is one of their main key interests.

If you are interested in participating or using one of our facilities, send us an email through info@laboratorium.bio

Laboratorium is part of "The Color Biolab" project, funded by the Arts Research Fund of University College Ghent.