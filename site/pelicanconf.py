#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'LABORATORIUM'
SITENAME = 'LABORATORIUM'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Brussels'

DEFAULT_LANG = 'en'

THEME = "themes/colorlab"

PLUGIN_PATHS = ["plugins"]
PLUGINS = ["autopages", "image_class_fix", "image_process"]

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),
         ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

DIRECT_TEMPLATES = ['index', 'categories']

DEFAULT_PAGINATION = False

CATEGORY_SAVE_AS = '{slug}.html'
PAGE_SAVE_AS = '{slug}.html'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

from datetime import datetime
def format_datetime(value, *args):
    try:
        datetime_object = datetime.strptime(value, "%Y/%m/%d %H:%M")
    except ValueError as v:
        value = value[:-9]
        datetime_object = datetime.strptime(value, "%Y/%m/%d %H:%M")
        status = 'full'

    return datetime_object

def sort_by_order(value):
    return sorted(value, key=lambda article: article.order if hasattr(article, 'order') else 999999999)

JINJA_FILTERS = {
    'format_datetime': format_datetime,
    'sort_by_order': sort_by_order
}

SUMMARY_MAX_LENGTH = 20

DEFAULT_METADATA = {
    'date': '01-01-01'
}

IMAGE_PROCESS = {
    'thumb': {'type': 'responsive-image',
                    'sizes': '(orientation: portrait) 50vw, 25vw',
                    'srcset': [('250w', ["scale_in 250 None False"]),
                               ('500w', ["scale_in 500 None False"]),
                               ('1500w', ["scale_in 1500 None False"]),
                               ('2500w', ["scale_in 2500 None False"]),
                               ],
                    'default': '250w',
                   },
}

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.extra': {},
    },
    'output_format': 'html5'
}