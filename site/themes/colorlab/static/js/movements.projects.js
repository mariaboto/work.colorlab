window.addEventListener('load', function () {
  initMovements(document.getElementById('colors'),
    function (el, position) {
      el.style.setProperty('top', (25 - position * 25).toFixed(2) + 'vh');
      el.style.setProperty('right', (75 - position * 25).toFixed(2) + '%');
      el.style.setProperty('bottom', (75 - position * 25).toFixed(2) + 'vh');
      el.style.setProperty('left', (25 - position * 25).toFixed(2) + '%');
    },
    function (el, position) {
      el.style.setProperty('top', (75 - position * 25).toFixed(2) + 'vh');
      el.style.setProperty('right', (25 - position * 25).toFixed(2) + '%');
      el.style.setProperty('bottom', (25 - position * 25).toFixed(2) + 'vh');
      el.style.setProperty('left', (75 - position * 25).toFixed(2) + '%');
    }
  );
});