(function () {

  var movements = [], lastY;

  function addColor (wrapper) {
    var color = document.createElement('div');
    color.classList.add('patterned');
    wrapper.appendChild(color);
  }
  
  
  function initMovements (wrapper) {
    for (var i=1; i < arguments.length; i++) {
      (function (movement) {
        var el = document.createElement('div');
        el.className = 'patterned';
        wrapper.appendChild(el);
        movements.push(function (position) {
          movement(el, position);
        });
      })(arguments[i]);
    }
    initBackgrounds();
  }
  
  window.initMovements = initMovements;

  function tick(force) {
    if (window.scrollY !== lastY || force) {
      scrollDistance = document.body.getBoundingClientRect().height - window.innerHeight,
      position = window.scrollY / scrollDistance,
      lastY = window.scrollY;
  
      movements.forEach(function (callback) {
        callback(position);
      });
    }
    window.requestAnimationFrame(tick);
  }

  tick();

})();