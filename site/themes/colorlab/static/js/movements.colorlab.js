window.addEventListener('load', function () {
  initMovements(document.getElementById('colors'),
    function (el, position) {
      el.style.setProperty('top', Math.round(position * 60).toString() + 'vh');
      el.style.setProperty('bottom', Math.round(60 - position * 60).toString() + 'vh');
    }
  );
});