;(function () {
  var articles = document.querySelectorAll('section.article');
  for (var i = 0; i < articles.length; i++) {
    var article = articles[i],
        header = article.querySelector('h2'),
        figure = article.querySelector('figure');

    if (header && figure && (! header.nextElementSibling.isSameNode(figure))) {
      console.log('Moving figure');
      article.insertBefore(figure, header.nextElementSibling);
    }
  }
})();