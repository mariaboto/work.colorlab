Heavily modified version of: https://github.com/jhshi/pelican.plugins.category_order


To install, add "category_order" to the plugins list of the PLUGINS property in the pelicanconf

Add a CATEGORY_ORDER to the pelicanconf example:
CATEGORY_ORDER = ['colorlab', 'laboratorium', 'projects', 'activities']