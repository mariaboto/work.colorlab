import re
from pelican import signals
from bs4 import BeautifulSoup

def inject_img_class (contentObj):
  if contentObj._content:
    soup = BeautifulSoup(contentObj._content, "html.parser")

    for img in soup.find_all('img'):
      if not 'class' in img:
        img['class'] = 'image-process-booklet-image'
        print(img['src'])

    contentObj._content = str(soup)

def register():
  signals.content_object_init.connect(inject_img_class)
