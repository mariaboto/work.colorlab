Title: Workshop Hack the Pigment // Spirulina Session 
Tags: workshop
Date: 2 June 2018
Author: LABORATORIUM

<figure>
  <img src="images/wabilabo.jpg">
</figure>

Het Werkhuis, in collaboration with Laboratorium and Wabi.labo, organizes a workshop on Spirulina (cyanobacterium Arthrospira). Spirulina is not only a food supplement but a rich source of natural colors such as carotenes (orange), chlorophyll (green) and phycocyanins (blue). In this workshop, you will learn how to grow spirulina at home and use it as a pigment for textile printing. By controlling the degradation of the pigment, you can have a wide range of blue and green colors that will change on your fabric over time. Together we will look for the possibilities in different printing techniques, and you will go home with your own creation and the necessary knowledge for carrying on your own experimentation.
"Hack the pigment sessions" is an open source study of the use, creation and formal application of colors. 

[More info](https://www.facebook.com/events/884109831799085/)
