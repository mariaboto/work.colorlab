Title: TFG dyeing with live bacteria
Tags: users
Date: 6 February 2019
Author: LABORATORIUM

<figure>
  <img src="images/TFG.jpg">
</figure>

On February 6th we organized a workshop with bachelor students at ELISAVA on dyeing with living bacteria.

[More info](http://www.elisava.net)