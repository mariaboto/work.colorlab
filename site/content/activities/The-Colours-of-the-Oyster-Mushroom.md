Title: Project week 2018: The Colours of the Oyster Mushroom
Tags: exhibition
Date: 20 March 2018
Author: LABORATORIUM

<figure>
  <img src="images/PW_mushrooms.JPG">
</figure>

The experimental bio lab for art and design LABORATORIUM offered a workshop about oyster mushrooms. We focused on the life cycle of the oyster mushroom where waste becomes food. Meanwhile, different disciplines were touched: from biology, landscape architecture, economics, installation building, documenting colors, to the art of cooking.
Oysters are one of the most versatile mushrooms and very edible. They are easy to cultivate and common all over the world. Oyster mushrooms can be beneficial to the body and break down toxic chemicals. They are also stunning and grow in a broad spectrum of colors.
By starting to grow them on several objects, we were taught and led by artist and mushroom expert Zeger Reyers (NL) and the KASK LABORATORIUM to transform different objects in a substrate for this organism.
