Title: Color Biolab Workshop: Harbinger Subtle Collisions: 
Tags: users
Date: 6 July 2019
Author: LABORATORIUM

<figure>
  <img src="images/pigment33.jpg">
</figure>

What is color? Chemistry? Light? Perception? Language? 

During the Harbinger Subtle Collisions, the Color Biolab organizes a workshop in collaboration to Michael Tytgat (UGent) on the use of colors in quantum physics followed by a hands-on activity with three coloring plants that grow in the Ghent University Botanical Garden (madder, St John's-wort, and European indigo). During the workshop, colors will be extracted and applied in different mediums and formats: scientific publications, pigments, extracts, and dyes, will be used on glass, fabric, and paper.
Thanks to Michael Tytgat (UGent), Ghent University Botanical Garden, Laurence de Craene (Industrie Museum) and Huis van Kina.


[More info](https://www.facebook.com/events/2438282136383177/)
