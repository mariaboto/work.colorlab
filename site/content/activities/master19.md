Title: Master seminar Art Science and Technology Interactions 2019
Tags: master
Date: 20 February 2019
Author: LABORATORIUM

<figure>
  <img src="images/riotinto.jpg">
</figure>

In this third edition of the Master Seminar Art, science and technology interactions participants will have an introduction about bioart, synbio, technology and pop, together with a wide number of scientific experiments performed in the lab.

The guest speakers were [Pepa Ivanova](http://pepaivanova.com), [Tom Peeters](https://www.erasmushogeschool.be/nl/labs/open-biolab) and [Kristel Peters](http://shoedesigner.be/). 
