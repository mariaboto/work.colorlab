Title: RessObject in Kanal-Pompidou Brussels
Tags: users
Date: 22 March 2019
Author: LABORATORIUM

<figure>
  <img src="images/jerry2.jpg">
</figure>

RessObject will be exhibited by Gluon in Printemps Numérique, a 2-day festival for digital culture initiated by the Brussels Government at museum Kanal - Centre Pompidou in Brussels from 22 until 24 March 2019.

In this work of the artist [Jerry Galle](https://jerrygalle.com/projects/ressobject/), mycelium is growing inside of a PLA sculpture printed by [FORMLAB](http://www.formlab.schoolofarts.be/). The deformation is captured by a camera attached to a robot arm and recognized by an AI program.




