Title: Screen printing with algae and plants at Ghent Industrie Museum 
Date: 29/09/2018

<figure>
  <img src="images/algaeprint.jpg">
</figure>

In collaboration with the Museum of Industry in Ghent, we will screen print with algae and plants used in the color research during the museum opening festival. 
The demonstration will be the 29th and 30th of September from 15h to 17h.
{: .online-only}

[More info](https://www.industriemuseum.be/nl/agenda/industriefestival-opening-industriemuseum)