Title: LEVEND TEXTIEL: printen met algen
Tags: users
Date: 3 March 2019
Author: LABORATORIUM

<figure>
  <img src="images/Manel.jpg">
</figure>

Article in the magazine TxP (Magazine over Textilekunst) over one of our workshops on microalgae textile printing written by [Manelprints](https://www.manelprints.com/opleidingen).


[More info](https://www.textielplus.nl/artikelen/natuur-thema-txp-textiel-plus-247/)




