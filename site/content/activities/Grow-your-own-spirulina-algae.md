Title: Grow your own spirulina algae!
Tags: workshop
Date: 6 June 2018
Author: LABORATORIUM

<figure>
  <img src="images/reagent.jpg">
</figure>

First session in collaboration with ReaGent about how to grow spirulina at home: from the starter culture to the final product. 
On June 6th  at 19h.
{: .online-only}

Second session: Algae application on fabric. 
On June 20th at 19h.
{: .online-only}

Location: ReaGent (Wispelbergstraat 2 9000 Gent)

[More info]( https://reagentlab.org/)
