Title: Laboratorium at Onrust magazine
Tags: users
Date: 23 October 2019
Author: LABORATORIUM

<figure>
  <img src="images/onrust.jpg">
</figure>

Text made by Bert Puype & Alice Lefebvre about Laboratorium and the Color biolab research project.

[More info](https://medium.com/onrust/laboratorium-color-biolab-622c76231585)

