Title: Harbinger: Subtle Collisions
Tags: users
Date: 5 July 2019
Author: LABORATORIUM

<figure>
  <img src="images/harbinger.png">
</figure>

<i>A HARBINGER is a forerunner. Historically linked to the idea of providing lodgments in advance, it indicates something or someone foreshadowing events in the future. 
In the context of the EPS-HEP conference to be held in Ghent this summer, KASK’s ​ 2018 - 2019 Curatorial Studies designed this series of events drawing closer lines between art and science in partnership with art@CMS, a program developed under the CERN experiments in Geneva.</i>

For this project, from LABORATORIUM we will work with the red, green, and blue pigments produced in the Ghent Botanical Garden in collaboration with the Dept. of Physics and Astronomy of Ghent University, as an exercise of translation between different scales and fields. This project intends to be a space of freedom to reflect on the use of common terminology and the possibilities of generating new knowledge by combining both fields.  

[More info](https://harbinger.schoolofarts.be/subtle-collisions/)
