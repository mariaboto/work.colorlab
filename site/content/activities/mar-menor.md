Title: Mar Menor Lab: Art+Science in an emergency landscape
Tags: users
Date: 3 July 2018
Author: LABORATORIUM

<figure>
  <img src="images/marmenor.png">
</figure>

 From 09-11-2018 to 09-16-2018 in Mar Menor, Murcia (Spain), we will participate in the summer school about art and ecology, Mar Menor Lab: Art+Science in an emergency landscape with the project "Land of Diatoms" together with Susana Cámara Leret.
During this summer school, "Land of Diatoms" proposes the creation of a mobile lab, addressing the reconfigurations of the territory through the migrations of live diatoms in the Mar Menor.

[More info](http://www.um.es/unimar/ficha-curso.php?estado=V&cc=51721)