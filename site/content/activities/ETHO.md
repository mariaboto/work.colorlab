Title: ETHO Conference, Aalto University
Tags: users
Date: 5 April 2019
Author: LABORATORIUM

<figure>
  <img src="images/etho.jpg">
</figure>

The third Art and Design Academies Technical Service Workshop was organised by Aalto University in Espoo at 4 and 5 April 2019. 
Together with Elias Heuninck, [Formlab](http://www.formlab.schoolofarts.be/) and [Laboratorium](http://www.laboratorium.bio) were presented at the conference.

The conference is held by and for workshop masters and heads of technical staff. Besides attending lectures, we went on a tour of the workshops in Aalto’s new Väre building. The sessions were an excellent opportunity to learn, benchmark and raise questions about the changing roles of the workshop, the workshop master and the students working there.


[More info](https://www.aalto.fi/en/events/art-and-design-academies-technical-service-workshop)
