Title: Hack The Lab
Tags: users
Date: 4 October 2019
Author: LABORATORIUM

<figure>
  <img src="images/BannerHTL.jpg">
</figure>

[The Open BioLab Brussels](EhB, www.openbiolab.be) and [Laboratorium](KASK, Hogent, www.laboratorium.bio) are two biological laboratories respectively located in a scientific (biomedical laboratory sciences) and artistic education institution, with a particular focus on transdisciplinary education by offering the students new tools and methodologies. 

<b>October 3rd</b>
    LOCATION Zwarte Zaal, KASK, Campus Bijloke, 9000 Ghent
15h: Guided tour by present artist to the Art-Science exhibition "Seeing together: A Synopsis”.
Artists: Annemarie Maes, Peter Beyls, Jerry Galle, Sina Hensel, Pepa Ivanova, Vanessa Müller, Elias Heuninck,
Zeger Reyers, María Boto, Kristel Peters.

<b>October 4th</b>
    LOCATION Erasmushogeschool, Laarbeeklaan 121, 1090 Jette, Brussels
7.20 meet at Gent Sint Pieters
9h: Opening doors
9h20: Welcoming (Tom Peeters- Erasmushogeschool, Maria Boto-KASK, Hogent)
9h30: BioArtist Annemarie Maes (België): "Sensorial Skin: researching bacterial (bio)sensors"
10h15: BioHacker Channel Thomas (Denemarken) aka Little Pinkmaker: “sustainable biomaterials”
11h: Break
11h15: BioHacker Andreas Stuermer (Oostenrijk): "Biology beyond borders"
12h: BioArtist Christina Stadlbauer (Finland): "Ceramic Scar Tissue, an exploration of Kin Tsugi with life matter in
the microbiology"
12h45: Lunch
13h30: Workshop

[Workshop Channel Thomas](https://forms.gle/Srbgrai3J3T5yR2n8)
This hands-on workshop will give you the skills and knowledge to create your own array of vegan cheeses, milks,
creamy spreads and dairy-free desserts. Limitless options for flavours and styles, a great way to impress
yourself, your family and your friends.

[Workshop Andreas Stuermer](https://forms.gle/oEBZw3c7tpLu1dN49)
2 million dollars for a syringe? Zolgensma is a gene therapy that is used to treat spinal muscular atrophy. In this
workshop we learn how to design DNA with the SMN1 gene as an example, talk about Gene therapies,
regulatory hurdles, patents, and the ethics of commercial and potential DIY treatments.
This event is made possible with the financial support of the EhB-HOGENT Alliantiefonds