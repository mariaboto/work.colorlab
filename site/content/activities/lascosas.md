Title: Las cosas
Tags: users
Date: 18 April 2020
Author: LABORATORIUM

<figure>
  <img src="images/cosas.jpg">
</figure>

Las Cosas is a collection of objects and images created to commemorate the centenary of the birth of César Manrique. Through it the artists do a revision of his artistic work from a contemporary perspective.
The exhibition revolves around recognisable aspects of Manrique´s work, such as the relationship with the landscape or the vernacular architecture of Lanzarote, but also around other social, spatial and material aspects which result less tangible.
Microscopic images and videos were created in collaboration with the group Evolution And Optics Of Nanostructures, (University of Ghent),  School of Arts KASK (Hogent).

{: .credit}
Carlos Álvarez and Saray Ossorio
El Aljibe de Haría – Haría, Lanzarote
Opening 18.04.202
{: .credit}

[More info](https://carlosalvarezclemente.com/)
