Title: DDW @Plan B "Forget me Not"
Tags: users
Date: 18 October 2019
Author: LABORATORIUM

<figure>
  <img src="images/DDW2.jpg">
</figure>

‘We are born (originate), we live (exist), we die (decompose)’
A search into our desire to preserve what is. We know life comes to an end, yet there is the desire for eternity.
Rhythms of nature, of life, take us along babbling brooks, through storms, across mountains, down our own path. Everything we discover and experience along the way is kept in our memories, they are unique and are hidden in our hearts.
Deep down inside, lies a desire to stop these memories of becoming a memory. It is our wish to preserve them and keep them forever.
Suppose this was possible. What if we could capture everything in a glass cage, lock it up and believe we could preserve it all.
This series of prints is made with natural pigments of algae, they decompose by daylight. If you want to preserve them, you will have to protect them from daylight and keep them in the black box. They will not see the light of day. Therefore, if you want the prints to see daylight, you will have to accept the consequences.

They originate, they exist, but will they ever decompose?
This is a question we can ask ourselves in relation to the more and more feasible world that we are creating ourselves.
This project is the result of an artistic research conducted by Studio Kars + Boom, supported by the scientific research of School of Arts Ghent.


At Plan B the lab is presented as part of a collaboration with the illustrators “Kars en Boom” and their project “Forget me not”. For that they painted one of the rooms with the pigments from the color research.

[More info](https://www.ddw.nl/en/programme/1509/forget-me-not)

 
