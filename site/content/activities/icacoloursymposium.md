Title: ICA-Belgium COLOUR SYMPOSIUM
Tags: activities
Date: 21 May 2019
Author: LABORATORIUM

<figure>
  <img src="images/yearcolour.png">
</figure>

The Colour Symposium brings together Belgian and international speakers from various disciplines spanning the fields of art, design, science and technology. A 3-day event encompasses two days of presentations, interactive color exploration, a tour of the Light & Lighting Laboratory and its latest research, film screening and a full day workshop on color education.

Presentations cover the latest color research and application in areas such as art, design, architectural and spatial design, lighting design, film, science and technology, color education and psychology.


LABORATORIUM will be present during the ICA-Belgium Colour Symposium together with other professionals of the color field. 

[More info](https://coloursymposium.org/)

Picture done with yearofcolour application