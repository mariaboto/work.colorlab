Title: FARO
Tags: conference
Date: 17 November 2018
Author: LABORATORIUM
Slug: FARO-vesten

<figure>
  <img src="images/faro2.png">
</figure>

We will be at Ter Vesten cultural center in Beveren the 17th of November presenting FARO, an experimental movie based on the Belousov-Zhabotinsky reaction. 
A work made in collaboration with [Elias Heuninck](http://www.eliasheuninck.be/).

[More info](https://www.tervesten.be/muziek/wik-t)
