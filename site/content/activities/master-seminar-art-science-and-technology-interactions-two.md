Title: Master seminar Art Science and Technology Interactions 2018
Tags: master
Date: 25 January 2018
Author: LABORATORIUM

<figure>
  <img src="images/master18.jpg">
</figure>

In this second edition of the <i>Master Seminar Art, science and technology interactions</i> participants had an introduction about bioart, biodesign and the role of science in our daily culture, together with a wide number of scientific experiments performed in the lab. Living organisms as yeasts, bacteria, algae and protozoa, chemical reactions as B-Z reaction or crystallization processes are part of this seminar that pretends to show and reflect on the possibilities of new materials and methodologies in art and design practice.

The guest speakers were [Zeger Reyers](http://www.zeger.org/), [Bram Crevits](http://www.criticalclouds.be/) and [Kristel Peters](http://shoedesigner.be/). 
