Title: Colour and vision, Natural History Museum, London
Tags: exhibition
Date: 3 November 2016
Author: LABORATORIUM

<figure>
  <img src="images/NHM.jpg">
</figure>

As part of the color research project developed in KASK, we had the opportunity to visit the exhibition “Colour and vision” at the Natural History Museum of London, and also to talk about colors with Dr. Suzanne Williams, a scientific researcher who helped design the exhibition content along with Dr Greg Edgecombe and Ms Fiona Cole-Hamilton.

We are surrounded by colours, and the gate to enjoy them is the eye. For that reason, the first part of the exhibition, after Liz West’s art installation, was focused on the evolution of the eye, that unavoidably goes in parallel to the increase of colors in nature. The evolution from light sensors to much more complex structures supposed an important change not only for the living organisms themselves but also for the environment.

An overview from the Precambrian Eon until now shows the evolution of eyes: first fossils, especially trilobites, and then a wide range of existing animals with different eye structures: arthropods, mollusks, cnidarians, onychophora and finally mammals.

The second part of the exhibition was dedicated to color in nature. What is color, where it can come from and how we perceive and interpret it. Visible and non-visible molecules. Derived from natural pigments, complex physical structures and the combination of both. Color stability and its importance in nature: colors to survive and to thrive.

Pigments are carotenoids, melanin, flavonoids, porphyrins, etc. They are found in vegetables like carrots, in animals such as flamingos and in our daily life products. However, there are some colors that the human eye can not perceive, as the fluoresce red of porphyrins under UV light in some mollusks.

There is a clear idea about the reason why some colors are present in nature, but not in every case. In the exhibition, the presence of colors in some animals is shown as a way of camouflaging, showing status, gender identification, facilitation of reproduction and feeding, light harvesting, etc. The color of an innocent ladybird warns us of the toxicity of its body, and marine iguanas use dark colors to store heat before going into cold water.

At the end of the exhibition, a video invites us to re-think our capability as humans to see, perceive and feel, by listening to the two scientists responsible for the exhibition -Dr. Suzanne Williams and Dr. Greg Edgecombe- and two artists, Liz West and Neil Harbisson, colorblind but able to hear colors through a camera implanted in his head.

[More info and videos.](http://www.nhm.ac.uk/visit/exhibitions/colour-and-vision-exhibition.html)
