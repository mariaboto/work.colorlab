Title: Colour Day Conference 2018
Tags: conference
Date: 21 March 2018
Author: LABORATORIUM

For the Belgian International Colour day in La Cambre, Brussel, we had the opportunity to present our work (The Color Biolab: transdisciplinary research on color) and discuss with other color professionals on topics as color psychology or color biology. 

[More info](http://www.ica-belgium.org/)
