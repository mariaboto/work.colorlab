Title: Traditional dyeing
Tags: workshop
Date: 20 October 2017
Author: LABORATORIUM

<figure>
  <img src="images/dyeing.jpg">
</figure>


In collaboration with the fashion department from KASK, we did an introduction course on natural dyeing by using madder, weld, cochineal, logwood, buckthorn bark, and alkanet. 
A wide range of red, yellow, blue, purple and green colors was obtained as result of different dyeing materials in combination with different mordants.
