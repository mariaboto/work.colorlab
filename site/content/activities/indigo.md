Title: Indigo workshop
Tags: workshop
Date: 28 November 2016
Author: LABORATORIUM

<figure>
  <img src="images/indigo.jpg">
</figure>

Together with the researcher Clara Vankerschaver, from the textile department, a workshop about indigo has been organized. Moving from the chemistry behind indigo color to a practical experience with several fabrics, the participants will create beautiful blue hues.