Title: Best Microalgae Award 2020
Tags: users
Date: 15 December 2020
Author: LABORATORIUM

<figure>
  <img src="images/microaward.png">
</figure>


In collaboration with Larissa Arashiro (UGent) we obtained the Best Microalgae Awards 2020 Wild Card Category Silver Medallist with the project <i>Clean Colour: Sustainable printing by transforming waste streams into colourful microalgae pigments</i>, materializing a circular economy approach turning wastewater streams into low-persistence ink and dyes for short-term applications.

[More info](https://bestmicroalgaeawards.org/uncategorized/best-microalgae-awards-congratulates-winners/)