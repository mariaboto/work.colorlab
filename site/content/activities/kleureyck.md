Title: KLEUREYCK Van Eyck’s Colours in Design
Tags: users
Date: 4 April 2020
Author: LABORATORIUM

<figure>
  <img src="images/kleureyck.jpg">
</figure>

What is the origin of colour? How do you create colour? What is the impact of colour? In honour of the Year of Van Eyck in Ghent, the museum is hosting a large exhibition on the innovative and diverse use of colour. The exhibition starts from the unprecedented hues of Jan van Eyck and illustrates the significance of colour to contemporary designers. An exhibition about the innovative use of colour, linking the past and present with each other.
The Color Biolab is part of one of the experience rooms, presenting the research together with the work of Pepa Ivanova and Tim Theo Deceuninck. Experience rooms let you perceive, taste and hear colour.

{: .credit}
The exhibition is a coproduction with lille3000 as part of Lille Métropole 2020 World Design Capital.Colors, etc. is on display in Tripostal 09.10.2020 — 03.01.2021.Curator: Siegrid Demyttenaere together with Sofie Lachaert [pigment walk].
{: .credit}

[More info](https://www.designmuseumgent.be/en/events/vaneyck)
