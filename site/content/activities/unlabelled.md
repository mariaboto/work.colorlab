Title: Seeing together: a Synopsis
Tags: users
Date: 18 September 2019
Author: LABORATORIUM

<figure>
  <img src="images/mica2.jpg">
</figure>


Often asepticised, secluded and reserved for scientific purposes, a laboratory is a place for scrutiny. Seemingly opposed to the immaculate space of scientific research, is the artist’s studio. Expected to reflect on creativity, the studio tends to be subject to preconceptions. For two types of creativity, two separate shelters. Nonetheless, scientists as well as artists seek « a place providing opportunity for experimentation, observation, or practice in a field of study ».

This exhibition celebrates the research of the Color biolab, offering a transdisciplinary approach to both art and science. The presence of a biolab in an art school, produces bilateral opportunities to thrive and to learn. The symbiotic relationship deriving from this bipolar project, forces one to reconsider and rethink the labels attributed, therefore making the partition between art and science more permeable.

In collaboration with María Boto Ordóñez and Kristel Peters from the LABORATORIUM/Color biolab.
With the participation of Peter Beyls, Tim Theo Deceuninck, Sina Hensel, Elias Heuninck, Jerry Galle, Annemarie Maes, Vanessa Müller, Pepa Ivanova and others to be announced. 
Curated by: Alice Lefebvre and Bert Puype


Opening: 20 September, 2019, from 7pm 
Exhibition: 18 September – 6 October 019 
Mo – Fri: 14:00 – 18:00 Sa – Su: 11:00 – 18:00
{: .online-only} 

Zwarte Zaal – KASK Louis Pasteurlaan 9000 Gent  
