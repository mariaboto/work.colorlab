Title: Future Materials
Tags: exhibition
Date: 27 April 2018
Author: LABORATORIUM

<figure>
  <img src="images/handmade.JPG">
</figure>

A poster printed with algae ink was presented in the exhibition Future Materials organized by Handmade in Bruges. The poster was made by using inks derived from the Color Biolab research project and designed and printed by Open Source Publishing. This exhibition shows the possibilities of using bio-based innovative sustainable materials by craftsmen: experimentation and application. 

[More info](http://www.handmadeinbrugge.be/FUTUREMATERIALS)