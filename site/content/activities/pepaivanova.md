Title: Bright up the chameleon skin to pale
Tags: users
Date: 27 February 2019
Author: LABORATORIUM

<figure>
  <img src="images/Pepa.jpg">
</figure>

Bright up the chameleon skin to pale
Performative experiments of Pepa Ivanova during the Master Seminar.

A discussion of the abiotic condition of light and its manifestation in color appearance, time and transformation. In order to embody that transformation, we will imagine ourselves being in continues mutation where rainbow colors can invade our skin and plastic starts grow out of us into a new set of nature.
While our skin becomes a color laboratory I will tell stories of my artistic evolution into numbers/ time/ patterns/ soundscapes/light intensities collector and lover... and currently a PhD researcher.

Skin is a garment.
Cosmetics comes from Cosmos.

[More info](http://www.pepaivanova.com/)