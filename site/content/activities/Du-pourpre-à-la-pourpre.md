Title: Du pourpre à la pourpre colloquium
Tags: users
Date: 13 October 2018
Author: LABORATORIUM

<figure>
  <img src="images/murex.jpg">
</figure>
On October 13th and 14th in Brussels, a colloquium about the color purple will be carried out by Nuances de Plantes.
Historians and makers will reflect about the meaning of the color and the origin (murex).
Laboratorium will present alternative purples derived from bacteria and algae.

In parallel, there is a collective exhibition of works made in purple obtained from plants and animals pigments.

[More info](https://nuancesdeplantes.wordpress.com/actualites/)
