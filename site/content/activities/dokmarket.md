Title: Nieuw kleur DOK
Tags: users
Date: 2 June 2019
Author: LABORATORIUM

<figure>
  <img src="images/DOKmarket.jpg">
</figure>

Zondag, 2 juni, stellen ze trouwens het atelier open voor publiek en geven ze een demo over hoe je een tweedehands kledingstuk in een nieuw kleur verft met plantenmateriaal!

[More info](https://www.facebook.com/DOKgent)

