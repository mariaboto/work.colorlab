Title: Belgian Society for Microbiology Conference
Tags: conference
Date: 30 October 2017
Author: LABORATORIUM

Belgian Society for Microbiology Conference

We present the results of our collaboration with the Center for Microbial Ecology and Technology (CMET), Ghent, Belgium in the Anual Conference of Belgian Society for Microbiology.
Inspiring photographs, colorful bacterial drawings and original designed lab-tools were obtained as outcome, reflecting on the limit between both disciplines, the aesthetic of science and the use of living organism for artistic proposes. This project aimed to establish a dialogue between art-science, questioning traditional parameters of scientific and artistic production and opening new approaches to academic research to improve, optimize and extend the outreach activities in both areas of knowledge.

[More info](https://belsocmicrobio.be/wp-content/uploads/2017/10/AbstractBook_Current-Highlights-in-MicrobiologyV3.pdf)