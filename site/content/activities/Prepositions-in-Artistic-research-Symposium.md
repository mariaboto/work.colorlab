Title: Prepositions in Artistic research Symposium
Tags: conference
Date: 15 December 2017
Author: LABORATORIUM

For the conference  Prepositions in Artistic research, organized by Arts Platform Brussels and ARTO, Laboratorium presented LABORATORIUM: experimental biolab for Art-Science research.
Art-Science projects are becoming more and more popular. Scientific institutions are inviting artists to participate in their research, offering space, expertise and equipment. But what about performing scientific research within an artistic context as an art academy? What kind of cross-pollination will be the outcome? In this conference, we questioned the methodology followed, the knowledge generated, as well as the language and context to present and communicate the results in transdisciplinary research.

[More info](http://www.kunstenplatformbrussel.be/en/prepositions-in-artistic-research)

