Title: Vlaamse Wetenschapsweek in KASK
Tags: workshop
Date: 9 November 2016
Author: LABORATORIUM

<figure>
  <img src="images/science16.jpg">
</figure>

For the first time, the School of Arts KASK participated in the Vlaamse Wetenschapsweek with a workshop ran by two researchers from the school, Kristel Peters and María Boto. The workshop was split into two parts: the first part was focused on the mycelium, where the students were able to create their own mycelium sculpture, followed by the second part where color was used to explain several scientific concepts by creating ephemeral art pieces. Eight participants from Koninklijk Atheneum Voskenslaan experimented with biology, physics or chemistry, breaking the boundaries between art, design and science.
