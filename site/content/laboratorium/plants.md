Title: Plants
Date: 30/05/2018
Order: 4
<figure>
  <img src="images/_LAB_template_web_1181x1181_cosmos_14.png">
  <img src="images/LAB_template_web_1181x1181_betula_52.png">
  <img src="images/LAB_template_web_1181x1181_azafaran2.png">
  <img src="images/LAB_template_web_1181x1181_camomile_42.png">
  <img src="images/LAB_template_web_1181x1181_cutch_192.png">
  <img src="images/LAB_template_web_1181x1181_equisetum_82.png">
  <img src="images/LAB_template_web_1181x1181_fustic_202.png">
  <img src="images/LAB_template_web_1181x1181_hypericum_perforatum_152.png">
  <img src="images/LAB_template_web_1181x1181_logwood_182.png">
  <img src="images/LAB_template_web_1181x1181_madder_212.png">
</figure>

Plants available in the laboratory are listed here. To find out more about plants, growing mediums and applications go to [colorlab](http://laboratorium.bio/colorlab.html).

<i>Alkanna tinctoria<i> // <i>Reseda luteola<i> //<i>Berberis aristata<i> // <i>Betula pendula<i> // <i>Cladonia<i> genus // <i>Cosmos sulphureus<i> // <i>Equisetum arvense<i> // <i>Evernia prunastri<i> // <i>Frangula alnus<i> // <i>Genista tinctoria<i> // <i>Haematoxylon campechianum<i> // <i>Maclura tinctora<i> // <i>Parastrephia lucida // <i>Persea americana<i> // <i>Rubia tinctorium<i> // <i>Sambucus nigra<i> // <i>Senegalia catechu<i> // <i>Solidago virgaurea<i> // <i>Sophora japonica<i> // <i>Arctostaphylos uva-ursi<i> // <i>Anthemis tinctoria<i> // <i>Hypericum perforatum<i>
