Title: Algae
Date: 29/05/2018
Order: 3

<figure>
  <img src="images/Laboratoriumbio_chlorella_vulgaris_1181x1181.jpg">
  <img src="images/Laboratoriumbio_dunaliella_salina_1181x11812.png">
  <img src="images/Laboratoriumbio_haematococcus_pluralis_1181x11812.png">
  <img src="images/Laboratoriumbio_porphyridium_purpureum_1181x11812.png">
</figure>

Algae available in the laboratory are listed here. To find out more about algae, growing mediums and applications go to [colorlab](http://laboratorium.bio/colorlab.html).

<i>Chlorella vulgaris<i> // <i>Dunaliella salina<i> // <i>Haematococcus pluvialis<i> // <i>Nannochloropsis<i> // <i>Porphyridium purpureumv // <i>Isochrysis galbana<i> // <i>Fucus vesiculosus<i> // <i>Microchaete diplosiphon<i> 



