Title: Laboratory
Date: 30/05/2018
Order: 1

<figure>
    <img src="images/lab_nov17.png" />
</figure>

The laboratory is located in the Media Department (Offerlaan 5) of the School of Arts. 
The space offers the following facilities to be students and collaborators:

microscope // incubator // centrifuge // fridge // frezeer // oven // autoclave // orbital shaker // distillation system // heating plates // safety cabinet