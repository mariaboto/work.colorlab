Title: Others
Date: 29/05/2018
Order: 6

<figure>
  <img src="images/_LAB_template_web_1181x1181_cochineal_23.png">
  <img src="images/evernia_name.jpg">
  <img src="images/pleurotus_name.jpg">
  <img src="images/slime_name.jpg">
</figure>

Others organisms as fungi, lichen or insects are listed here. To find out more about them, growing mediums and applications go to [colorlab](http://laboratorium.bio/colorlab.html).

<i>Chlorociboria aeruginascens<i> // <i>Pleurotus<i> // <i>Dactylopius coccus<i> // <i>Evernia prunastri<i> // <i>Parmelia and Flavoparmelia genus<i> // <i>Usnea genus<i> // <i>Xanthoria parietina<i> // <i>Bolinus brandaris<i> // <i>Physarum polycephalum
