Title: Solidago virgaurea
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_solidago1.jpg">
  <img src="images/laboratoriumbio_flatcolor_solidago2.jpg">
  <img src="images/LAB_template_web_1181x1181_solidago_virgaurea_10.png">
  <img src="images/Laboratoriumbio_plants_1181x1181_solidago10.jpg">

</figure>

It is known as goldenrod and it produces arrays of numerous small yellow flower heads at the top of the stem.The generic name <i>Solidago</i> is derived from the Latin word "solidare", which means "to make" of "merging". Formerly used men's goldenrod as a wound medicine and as a remedy against all kinds of kidney diseases. The addition "virga" means "thin, green branch" and "aurea" (gold)

Type: herbaceous plant
Color: mustard, orange and brown/ gold yellow
Part: stem, leaves, flowers
Applications tested: dyed cotton
Mordants: alum, iron, titane