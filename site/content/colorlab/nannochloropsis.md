Title: Nannochloropsis sp
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_nano.jpg">
  <img src="images/Laboratoriumbio_nanochloropsis_1181x1181.jpg">
</figure>

<i>Nannochloropsis</i> genus is distributed worldwide in marine, fresh, and brackish waters. Apart from a high-fat content, <i>Nannochloropsis</i> has been proposed as a pigment source of chlorophyll, zeaxanthin, canthaxanthin and astaxanthin. 

Type: microalga
Color: grass green
Pigments: chlorophylls
Applications tested: printed cotton
Mordants: --