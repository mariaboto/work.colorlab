Title: Berberis aristata
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_indian1.jpg">
  <img src="images/laboratoriumbio_flatcolor_indian2.jpg">
  <img src="images/LAB_template_web_1181x1181_indianberberry_9.png">
  <img src="images/Laboratoriumbio_plants_1181x1181_indianmadder26.jpg">
</figure>

A spiny bush, ranging between 2 and 3 meters in height. It has a cluster of yellow flowers in spring (mid-March/ April) and red to plum-colored bean-shape berries (May /June). The genus <i>Berberis</i> has an important place in various traditional systems of medicine worldwide for their medicinal properties. 

Type: bush
Color: yellow
Part: roots, stem
Applications tested: dyed cotton
Mordants: alum, iron, titane