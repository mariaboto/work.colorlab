Title: Pleurotus
Date: 13/05/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_oyster_yellow.jpg">
  <img src="images/laboratoriumbio_flatcolor_oyster_grey.jpg">
  <img src="images/laboratoriumbio_flatcolor_oyster_pink.jpg">
  <img src="images/oyster_yellow.png">
  <img src="images/oyster_grey.png">
  <img src="images/oyster_pink.png">
</figure>


Mushroom genus that can grow in different colors, pink (<i>Pleurotus salmoneo</i>), grey (<i>Pleurotus ostreatus</i>) and yellow (<i>Pleurotus citrinopileatus</i>). They are all edible.

Type: Fungi
Color: yellow, grey, pink
Pigment production: 
Growing conditions: mushroom substrate
Applications tested: spores on paper