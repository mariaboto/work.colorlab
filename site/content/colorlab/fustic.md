Title: Maclura tinctora
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_fustic1.jpg">
  <img src="images/laboratoriumbio_flatcolor_fustic2.jpg">
  <img src="images/_LAB_template_web_1181x1181_fustic_20.png">
  <img src="images/Laboratoriumbio_plants_1181x1181_oldfustic21.jpg">
</figure>

Old fustic has been used to produce an intense color at a low cost. During the WW1, fustic was one of the dyes used to obtain khaki for army uniforms. Fustic is a dye which contains the flavonoid morin, a yellow chemical compound. 

Type: tree
Color: yellow, khaki 
Part: wood
Applications tested: dyed cotton
Mordants: alum, iron, titane