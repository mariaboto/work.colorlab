Title: Sophora japonica
Date: 23/01/2019

<figure>
   <img src="images/laboratoriumbio_flatcolor_sophora1.jpg">
   <img src="images/laboratoriumbio_flatcolor_sophora2.jpg">
   <img src="images/_Laboratoriumbio_plants_1181x1181_sophora1.jpg">
   <img src="images/LAB_template_web_1181x1181_sophora_japonica_1.png">
</figure>


A medium to large tree (around 30m high), the spread equals height in a rounded shape. Pale yellow to creamy white pea-like fragrant flowers appear every year in August.
In China and Vietnam, this dye was only used to color silk, embroidery thread and hat tassels, but not for other materials because of the number of flower buds needed to prepare a dye-bath.


Type: tree
Color: yellow / granite-grey
Part: unopened flower buds
Applications tested: dyed cotton
Mordants: alum, iron, titane