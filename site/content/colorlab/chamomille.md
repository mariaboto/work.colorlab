Title: Anthemis tinctoria
Date:23 May 2018

<figure>
  <img src="images/laboratoriumbio_flatcolor_kamille1.jpg">
  <img src="images/laboratoriumbio_flatcolor_kamille2.jpg">
  <img src="images/LAB_template_web_1181x1181_camomile_4.png">
  <img src="images/_Laboratoriumbio_plants_1181x1181_kamille4.jpg">
</figure>


An evergreen biennial or perennial plant to 0.8 m height with daisy-like yellow flowers from July to August. The compounds that give the yellow color are flavonoids and they work better on wool or silk rather than on cotton.

Type: flower
Color: warm yellow till green shades
Part: flowers
Applications tested: dyed cotton
Mordants: alum, iron, titane