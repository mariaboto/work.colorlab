Title: Arctostaphylos uva-ursi
Date: 15/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_uvaursi1.jpg">
  <img src="images/laboratoriumbio_flatcolor_uvaursi2.jpg">
  <img src="images/_LAB_template_web_1181x1181_arctostaphylos_uva_ursi_13.png">
  <img src="images/_Laboratoriumbio_plants_1181x1181_uvaursi13.jpg">
  </figure>

Leaves of bearberry are rich in tannins and have been used in the process of tanning hides/leather. Leaves are used to get a yellow/brown and the fruit to get a grey/brown color.

Type: woody evergreen shrub 
Color: yellow, grey, brown
Part: leaves, fruit
Applications tested: dyed cotton
Mordants: alum, iron, titane
