Title: Rubia tinctorum
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_madder.jpg">
  <img src="images/laboratoriumbio_flatcolor_madder2.jpg">
  <img src="images/_LAB_template_web_1181x1181_madder_choppedroots_21.png">
  <img src="images/Laboratoriumbio_plants_1181x1181_madder22.jpg">
</figure>

Madder root is one of the oldest natural dyes and has been used for many centuries in Turkey, Iran, India, South Asia, Greece, Italy and Egypte. The name comes from the Latin: <i>herba radice rubia</i>, the plant with the red roots 
Madder is an evergreen perennial, growing to 70 till 100 cm height with yellow-green flowers during summer.

Type: tree
Color: brown
Part: inner bark, leaves
Applications tested: dyed cotton
Mordants: alum, iron, titane