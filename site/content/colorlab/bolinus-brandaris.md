Title: Bolinus brandaris
Date: 30/05/2018

<figure>
  <img src="images/murex.jpg">
</figure>

<i>Bolinus brandaris</i> is a marine gastropod mollusk originally known as <i>Murex brandaris</i>. The main characteristic is the production of mucous secretion in the hypobranchial gland that has been extensively used as a long-lasting dyeing substance. The color obtained is known as Tyrian purple.

Used from pre-Roman times, the significant amount of mollusk needed for a little amount of dye made this color a luxury trade, becoming the color for royalty. One of the most important habitats for <i>Bolinus brandaris</i> is the Mediterranean Sea, where it is commonly consumed as food.

The principal responsible for this color is the 6,6’-dibromoindigo. In its shellfish, a transparent substance reacts with the enzyme arylsulfatase. This enzyme generates hydrolyzed compounds that are susceptible to oxidative and photochemical reactions in contact with sunlight. The result is a deep purple.

Type: mollusk
Color: purple
Pigment production: 6,6’-dibromoindigo
Growing conditions: sandy-muddy to detritic environments
Applications tested: paper, fabric