Title: Haematococcus pluvialis
Date: 30/05/2018

<figure>
  <img src="images/haemato.png">
  <img src="images/Laboratoriumbio_haematococcus_pluralis_1181x1181.jpg">
  <img src="images/_Laboratoriumbio_algae_micro_1181x1181_HP.jpg">
  <img src="images/Laboratoriumbio_algae_1181x1181_textile_13_synHP.jpg">
   
</figure>

<i>Haematococcus pluvialis</i> is a freshwater microalga able to produce astaxanthin as a protection element under certain conditions such as the lack of nutrients, intense bright light or high salinity. The red appearance of lakes and ponds is because of this microalga.


Type: microalgae
Color: green/red
Pigment production: astaxanthin
Growing conditions: freshwater medium
Applications tested: paper/ synthetic Haematococcus pluvialis pigment on wool