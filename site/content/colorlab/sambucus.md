Title: Sambucus nigra 
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_sambucus1.jpg">
  <img src="images/laboratoriumbio_flatcolor_sambucus2.jpg">
  <img src="images/LAB_template_web_1181x1181_sambucus_16.png">
  <img src="images/_Laboratoriumbio_plants_1181x1181_sambucus17.jpg">

</figure>

Elderberry is a fast-growing bush or small tree, 8-10 meters’ height. <i>Sambucus nigra</i> is often utilized as valuable fruit and flower crop It is a source of anthocyanin food colorant.

Type: tree
Color: lilac blue and violet, green, black
Part: fruit, leaves, bark
Applications tested: dyed cotton
Mordants: alum, iron, titane