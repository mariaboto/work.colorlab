Title: Betula pendula
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_berken1.jpg">
  <img src="images/laboratoriumbio_flatcolor_berken2.jpg">
  <img src="images/LAB_template_web_1181x1181_betula_5.png">
  <img src="images/_Laboratoriumbio_plants_1181x1181_berkenblad5.jpg">
</figure>

Birch is a fast-growing tree with a white-colored bark. Twenty meters height growing catkin flower cluster (= fruiting body) in early spring.
Birch is often called a 'pioneer' species, one of the first trees to occupy any suitable ground.


Type: tree
Color: brown
Part: inner bark, leaves
Applications tested: dyed cotton
Mordants: alum, iron, titane
