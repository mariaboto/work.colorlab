Title: Chlorociboria aeruginascens
Date: 13/05/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_chlorociboria.jpg">  
  <img src="images/chlorociboria.png">
</figure>

Small green-blue mushroom with a turquoise mycelium. It is able to change the color of the wood that infects, making it a valuable material for decorative woodworking (green oak).

Type: Fungi
Color: blue-green
Pigment production: xylindein
Growing conditions: forest on decaying wood
Applications tested: wood