Title: Haematoxylon campechianum
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_logwood1.jpg">
  <img src="images/laboratoriumbio_flatcolor_logwood2.jpg">
  <img src="images/_LAB_template_web_1181x1181_logwood_18.png">
  <img src="images/Laboratoriumbio_plants_1181x1181_logwood19_2.jpg">
</figure>

Logwood is a small spiny tree, height: 9-15 m, short trunk irregularly fluted and contorted. Discovered by the Spanish and was exported to Europe as an important source for dye: black and purple textile dye from the 16th cent. The generic name of logwood <i>Haematoxylum</i> (often spelled <i>Haematoxylon</i>) means bloodwood, referring to the dark red heartwood. 

Type: tree
Color: purple, red and black
Part: wood
Applications tested: dyed cotton
Mordants: alum, iron, titane
