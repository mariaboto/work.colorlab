Title: Microchaete diplosiphon
Date: 12/05/2018

<figure>
  <img src="images/laboratoriumbio_flatcolor_microchaete_red.jpg">  
  <img src="images/laboratoriumbio_flatcolor_microchaete_green.jpg">
</figure>

<i>Microchaete diplosiphon</i>  is a cyanobacterium known for its chromatic adaptation: it can adjust photosynthetic receptors depending on the light and to present a green or red color.

Type: Bacteria
Color: red, green
Pigment production: chlorophyll
Growing conditions: BG11 medium
Applications tested: --