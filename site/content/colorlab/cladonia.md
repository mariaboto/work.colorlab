Title: Cladonia genus
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_cladonia1.jpg">
  <img src="images/laboratoriumbio_flatcolor_cladonia2.jpg">
  <img src="images/cladonie.jpg">
</figure>

Pale green moss-lichen. They are the primary food source for reindeer and caribou, that’s why the economic importance to reindeer-herders of Russia and Scandinavia.


Type: lichen 
Color: yellow, green
Part: whole
Applications tested: wool
Mordants: none