Title: Spirulina
Date: 30/05/2018

<figure>
  <img src="images/spirulina.png">
  <img src="images/Laboratoriumbio_spirulina_biomass_1181x1181.jpg">
  <img src="images/Laboratoriumbio_spirulina_pigment_1181x1181.jpg">
</figure>

Spirulina is the common name for a food supplement made out of <i>Arthrospira</i>, another genus different to Spirulina with similar morphological structure (the division is from 1989). Spirulina is a cyanobacterium, which means that can photosynthesize. First considered as a green-blue alga, Spirulina is widely distributed in South America, Africa and Asia in lakes with a high pH and carbonate concentration. The most important pigment extracted from spirulina is phycocyanin, responsible for its blue color.

Type: cyanobacteria
Color: blue/green
Pigment production: phycocyanin, chlorophyll
Growing conditions: basic  medium
Applications tested: paper, fabric, wood