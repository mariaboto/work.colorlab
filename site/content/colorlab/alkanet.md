Title: Alkanna tinctoria
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_alkanet1.jpg">
  <img src="images/laboratoriumbio_flatcolor_alkanet2.jpg">
  <img src="images/_LAB_template_web_1181x1181_alkanet_22.png">
  <img src="images/Laboratoriumbio_plants_1181x1181_alkanet23.jpg">
</figure>

<i>Alkanna tinctoria</i> is a small, soft hairy perennial herb growing between 20 till 30 cm height, flowering from June to July with little blue flowers and grey-green leaves. A red color is obtained from the roots. It is used by pharmacists as well as in perfumes and to stain wood or marble. The dye is also used in thermometers and as a litmus to test for acids and alkalines. It can make wood look like rosewood or mahogany. 

Type: herb
Color: purple and lavender
Part: roots
Applications tested: dyed cotton
Mordants: alum, iron, titane