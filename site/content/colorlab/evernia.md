Title: Evernia prunastri
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_evernia1.jpg">
  <img src="images/laboratoriumbio_flatcolor_evernia2.jpg">
  <img src="images/evernia.jpg">
</figure>

Green lichen with a “fork” shape.  The color on top is green and white on the bottom. Evernia is abundant on branches and twigs of trees (oak, fir, pine) and bushes in the countryside. Sensitive to air pollution, it is usually found in clean air environments.

Type: lichen 
Color: orange, green, red-pink
Part: whole
Applications tested: wool
Mordants: none
