Title: Janthinobacterium lividum
Date: 30/05/2018

<figure>
  <img src="images/livi.png">
</figure>

Gram (-) bacteria that produces violacein, a purple compound with anti-bacterial, anti-viral and anti-fungal properties.

Type: bacteria
Color: purple
Pigment production: violacein 
Growing conditions: LB medium +glycerol
Applications tested: paper, fabric

