Title: Parastrephia lucida
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_tola.jpg">
  <img src="images/laboratoriumbio_flatcolor_sipu.jpg">
  <img src="images/LAB_template_web_1181x1181_siputola_11.png">
  <img src="images/_Laboratoriumbio_plants_1181x1181_siputola11.jpg">
</figure>

This plant grows in the pre-cordillera and Altiplano in the North of Chile and Argentina. It is a resinous shrub that reaches a height of up to 2 m.

Type: bush
Color: green, yellow
Part: branch
Applications tested: dyed cotton
Mordants: alum, iron, titane