Title: Frangula alnus or Rhamnus frangula
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_buckthorn.jpg">
  <img src="images/laboratoriumbio_flatcolor_buckthorn2.jpg">
  <img src="images/LAB_template_web_1181x1181_buckthornbark_7.png">
  <img src="images/_Laboratoriumbio_plants_1181x1181_buckthorn7.jpg">
</figure>

Buckthorn is a large, slow-growing bush that reaches up to 5 meters in height with small red berries in the spring that become black when ripe. Traditionally, the wood has been used for making nails, shoe lasts and charcoal to manufacture gunpowder.

Type: bush
Color: mustard yellow / brick or cinnamon red 
Part: bark, leaves, fruit  
Applications tested: dyed cotton
Mordants: alum, iron, titane