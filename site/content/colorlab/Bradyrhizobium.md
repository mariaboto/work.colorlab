Title: Bradyrhizobium sp
Date: 28/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor__Bradyrhizobium.jpg">  
</figure>

Gram(-) soil bacteria which may live in symbiosis in the roots of leguminous plants. It takes atmospheric nitrogen and fixes it into ammonia. The nitrogen not used for the plant enriches the soil.

Type: bacteria
Color: yellow colonies
Pigment production: -- 
Growing conditions: nutrient agar
Applications tested: --