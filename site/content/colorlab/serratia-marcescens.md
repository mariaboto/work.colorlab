Title: Serratia marcescens
Date: 30/05/2018

<figure>
  <img src="images/serra.png">
</figure>

Gram (-) bacteria that produces prodigiosin, a tripyrrole red pigment probably associated with the "blood miracles".

Type: bacteria
Color: pink, red
Pigment production: prodigiosin
Growing conditions: NA
Applications tested: paper, fabric