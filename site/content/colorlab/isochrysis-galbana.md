Title: Isochrysis galbana
Date: 30/05/2018

<figure>
  <img src="images/iso.png">
</figure>

Marine microalgae used in aquaculture to feed mollusks and crustaceans. 

Type: microalgae
Color: brown, orange
Pigment production: fucoxanthin
Growing conditions: marine medium
Applications tested: paper