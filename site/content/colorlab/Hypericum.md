Title: Hypericum perforatum
Date: 10 May 2018

<figure>
  <img src="images/hypericum.png">
  <img src="images/laboratoriumbio_flatcolor_hypericum2.jpg">
  <img src="images/_LAB_template_web_1181x1181_hypericum_perforatum_15.png">
  <img src="images/Laboratoriumbio_plants_1181x1181_hypericum16.jpg">
</figure>

Known as Saint John's wort this plant is worldwide distributed, becoming an invasive weed in some places. Traditionally used as a medicinal herb.

Type: Flowering plant
Color: green, orange, brown
Part: whole plant
Applications tested: Dye cotton
Mordants: Alum, Iron, Titane