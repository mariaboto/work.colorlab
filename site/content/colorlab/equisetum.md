Title: Equisetum arvense
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_equisetum1.jpg">
  <img src="images/laboratoriumbio_flatcolor_equisetum2.jpg">
  <img src="images/LAB_template_web_1181x1181_equisetum_8.png">
  <img src="images/_Laboratoriumbio_plants_1181x1181_equisetum8.jpg">
  
</figure>


A prolific weed/plant with fat clusters in green ‘horse-tail’ shapes. It reproduces itself not by seeds but with spores (these spores are using ‘legs’ to walk and jump). It grows in wet places such as moist woods, ditches or wetlands.

Type: weed
Color: yellow
Part: leaves
Applications tested: dyed cotton
Mordants: alum, iron, titane
