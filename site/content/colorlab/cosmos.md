Title: Cosmos sulphureus
Date: 23/01/2019

<figure>
  <img src="images/laboratoriumbio_flatcolor_cosmos1.jpg">
  <img src="images/laboratoriumbio_flatcolor_cosmos2.jpg">
  <img src="images/_LAB_template_web_1181x1181_cosmos_14.png">
  <img src="images/Laboratoriumbio_plants_1181x1181_cosmos15.jpg">
  </figure>

<i>Cosmos sulphureus</i> is a prolific seed-producing annual herb. It grows to 2 meters tall with yellow flowers from May until November.
The species name sulphureus is a reference to the orange-yellow colors of the plant’s flower.

Type: flower
Color: yellow, orange
Part: flowers
Applications tested: dyed cotton
Mordants: alum, iron, titane