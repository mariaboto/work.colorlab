Title: Experimental Textiles
Tags: users
Date: 11 June 2019
Author: LABORATORIUM

<figure>
  <img src="images/natalia1.jpg">  
  <img src="images/natalia2.jpg">
  <img src="images/natalia4.jpg">
  <img src="images/natalia3.jpg">
</figure>


Trying to create a different type of textiles, I experiment with new materials to develop yarns in a particular method. 

Using Mycelium combined with Rattan and Crystals with wool I created a new type of yarn and then I weave it to make a textil piece.


A project by [Natalia López](https://natalialopezlopez1.wixsite.com/natalia).
{: .credit}