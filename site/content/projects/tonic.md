Title: THE EFFECT OF QUININE
Tags: users
Date: 3 June 2018
Author: LABORATORIUM

<figure>
  <img src="images/tonica.png">
</figure>

Quinine is an alkaloid found in the bark of the centuries as a treatment for malaria. Quinine not only gives tonic water a cinchona tree and has been used for characteristic bitter taste, but this chemical can also be very fluorescent under the right conditions.
Quinine contains a substance called phosphor. When hit with a particular wavelength of the EM spectrum, including UV-light from a black light,these substances will glow. The phosphors have the ability to absorb UV-light, which makes the quinine excited. By releasing or emitting the light, quinine returns to its normal, unexcited state. In this process, it converts the invisible UV-light to a visible, brilliant blue light that we can see, even when only a relatively small amount of quinine is dissolved in the water.

A project by Geertrui de Vijlder.
{: .credit}

[More info](https://geertruidevijlder.wordpress.com/)
