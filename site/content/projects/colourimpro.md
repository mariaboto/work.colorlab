Title: Musical Improvisation inspired by Colour Theory
Tags: users
Date: 23 May 2019
Author: LABORATORIUM

<figure>
  <img src="images/Screen_Shot_2019-05-29_at_09.33.14.png">
</figure>

Inspired by the theories of Goethe and the compositions of Scriabin we developed a relationship between the 12 notes of our Western music chromatic scale and the same amount of colour regions, equally dividing the visible spectrum from Red to Purple. 
We ordered the 12 notes according to the circle of fifths: (F, C, G, D, A, E, B, F#, C#, Ab, Eb, Bb) 
and linked them with the 12 regions of colours: (Red, Red-Orange, Orange, Yellow-Orange, Yellow, Yellow-Green, Green, Blue-Green, Blue, Purple-Blue, Purple, Red-Purple).

Each session was performed in different conditions:
    1. The first session, recorded on the 3rd March was performed using several early 20th century paintings as sources of inspiration. The paintings were Paul Klee’s Scarecrow (1935) and Triumph of a Degenerate, Kandinsky’s Houses in Munich (1908) and Monet’s Twilight in Venice (1908-1912).
    2. The second session, recorded on the 6th of March was performed during one of the Master Seminar’s class. Our fellow students suggested some works to be performed, like a painting of Sol Lowitt, a picture of photographer Dries Segers and a fabric artwork of Olga de Amaral.
    3. The third and last session, recorded on the 3rd of April, was conducted together with the Dance class, led by Paola Bartoletti, and the Improvisation class, led by Bart Maris, of the Drama and Classical music departments of the School of Arts Ghent. The experiment was based on the mutual interaction of three improvisation groups: Dancers, Drawers and Musicians. 
    
The recording of the three sessions, performed between March and April 2019, are available in the SoundCloud account of [Nicholas Cornia](https://soundcloud.com/nicholas-cornia/sets/color-music-session-art-science-seminar-kask-2019) . 

Nicholas Cornia, Dimitrios Tsirogiannis and Ricardo Valero Classical Music students Conservatorium Ghent, Belgium.
{: .credit}
