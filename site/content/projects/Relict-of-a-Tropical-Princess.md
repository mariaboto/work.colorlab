Title: Relict of a Tropical Princess
Tags: users
Date: 3 June 2018
Author: LABORATORIUM

<figure>
  <img src="images/wood.jpeg">
</figure>

The object is a fragile artefact discovered by an extensive expedition to the remote island Lengguru, Neuguinea. The territory is characterised by an extremely high rate of endemic species. Over fifty researchers from Europe and Indonesia were involved in collecting data about the biodiversity when they found the trace of an undiscovered civilisation.

Work made by Vanessa Müller.
{: .credit}

[More info](http://atelierbrieftau.be/)