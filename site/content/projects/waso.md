Title: WASO (Write a Science Opera)
Tags: users
Date: 22 February 2019
Author: LABORATORIUM

<figure>
  <img src="images/waso.jpg">
</figure>

Children from 6 to 9 years old from the school De Buurt prepared a Science Opera on the topic of sustainability and biomaterials, especially mycelium.
During their research, they visited the lab to get inspiration for their libretto.

The final opera "Schimmelschoenen" was presented the February 22th at Wisper Ghent.

Project by De Buurt Gent.
{: .credit}

[More info](https://www.cultuurkuur.be/project/schimmelschoenen-een-wetenschapsopera-gemaakt-door-kinderen)
