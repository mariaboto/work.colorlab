Title: Shi Chen
Tags: users
Date: 29 April 2018
Author: LABORATORIUM

<figure>
  <img src="images/chen.jpg">
</figure>

Although the intention was to make sort of thing like the amber, all these artistic works make me remind of the Chinese luxury jewelry. The blue-burning process (Shao lan) is based on silver, and is made of enamel-fired handicrafts. It is best known for its blue glaze and silver match. It is one of the traditional jewelry crafts in China. 

A project by Shi Chen.
{: .credit}