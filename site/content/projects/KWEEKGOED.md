Title: KWEEKGOED 
Tags: users
Date: 1 June 2016
Author: LABORATORIUM

<figure>
  <img src="images/martha.jpg">
</figure>

For five generations of Hofbouwers in Martha's family a lot of information and knowledge was passed on. This information has been bundled in an archive over time. She is the first person in the family who has not been brought up to take over the company, which makes her aware of the loss of tradition. This work is a reinterpretation of the existing archive and makes this knowledge susceptible to the present and future generations.
Cultivation is a collected work of seeds, alias and technical terminology that forms in an installation with seeding trays, Alaam, Herbarium, Dictionary, Soil samples, etc.

A project by Martha T'Hooft.
{: .credit}