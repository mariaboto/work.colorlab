Title: Red
Tags: users
Date: 13 June 2019
Author: LABORATORIUM


<figure>
  <img src="images/fons.jpg">
</figure>


Blood is something a lot of living entities have in common.
The animation Red is made from an interest in electron microscope images. The unreachable /unwatchable is what grabbed my attention. The primary idea would be to make a Virtual Reality animation of which you would be able to feel like a blood cell.Aiming for a total immersion, but due to the lack of a capable VR headset I was obliged to make it in an animation. The result isn’t a representation from an outer space but an inner space. I used different kinds of electron microscope images as reference and inspiration for the animation. The result is a representation
of the microcosmos we call blood.
My own practice is based in the nuanced zone between art and science, I am an enthusiast of speculative design and 3D modelling is a medium which lend itself to it. It has also an very wide variety of possible outcomes, it could be an animation, game, video or picture.
The main project I am working on at the moment is called Tools For Animals, It is a speculative design project to arm animals against humans. The anthropocene has negative consequences for the fauna and flora on our planet. We are currently undergoing a 6th major extinction wave, one caused for the first time by a species, man. If nature were to recover through the process of natural diversification, it would take another 5 to 7 million years. If we want many species to continue to exist, alternatives must be explored. Is it possible that we can make ecology and technology work together in a harmonious way?

A project by Fons Artois.
{: .credit}

