Title: Microalgae pigments
Tags: users
Date: 31 May 2019
Author: LABORATORIUM

<figure>
  <img src="images/biocolor1.jpg">  
  <img src="images/biocolor2.jpg">  
  <img src="images/biocolor3jpg.jpg">
  <img src="images/biocolor4.jpg">  
</figure>

In collaboration with Elias Heuninck responsible for [FORMLAB](http://www.formlab.schoolofarts.be/) we created colored 3D printer filament from microalgae pigments. Phycocyanin, phycoerythrin, and beta-carotene were mixed with corn-based PLA before extrusion. Filaments and prints had texture from the pigment and resulted in being photosensitive. 

More details of this project can be found in [Tools for things and ideas](https://tools-for-things-and-ideas.github.io/), a research project for the development of 3d technologies and media.
{: .credit}