Title: Algae 3D printing 
Tags: users
Date: 1 May 2017
Author: LABORATORIUM

<figure>
  <img src="images/algen.png">
</figure>

Exploring biomaterials (agar and pigments) derived from algae for 3D-printing. 

A project by Janne Claes.
{: .credit}

[More info](www.janneclaes.com)

