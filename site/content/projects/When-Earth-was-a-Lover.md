Title: When Earth was a Lover
Tags: users
Date: 24 May 2018
Author: LABORATORIUM

<figure>
  <img src="images/Weronika.jpg">
</figure>

When Earth was a Lover is an experiment based on travelling through microscopic elements of our daily life in search of poetry, cosmos, perhaps wisdom, or just a glimpse of a weightless moment, meeting in confrontation with the mind and its daily occupations.
out of many short films created, three of them have been displayed here together with texts that they have triggered. the words seem to be as if thoughts floating to the seen image, though also, inevitably, uncontrolled confessions. 
a mind let loose. a cloud-staring. an exercise of a daydream. an insight to the nature of our surroundings that make us, invisibly. as, to relate has been a matter of education and decision-making... the perception of time, the nature of things, the place of a human, questioned by child`s observations, close to the life, unconstructed, mighty.

[More info](www.weronikazalewska.com)

A project by Weronika Zalewska.
{: .credit}