Title: Interference
Tags: users
Date: 3 May 2019
Author: LABORATORIUM

<figure>
  <img src="images/mica.jpg">  
  <img src="images/mica3.jpg">
  <img src="images/mica2.jpg">
  <img src="images/mica4.jpg">
</figure>

<i>Interference</i> is a proposal that explores the possibility of using the physicochemical properties of some minerals to generate color under certain circumstances in an artistic context: minerals as a source of color and colored minerals as a source of knowledge.

Through the visualization of interference colors observed in a mica sheet before and after the human intervention, this project aims to think about color modifications in geological material in the Anthropocene epoch and its possible application in the arts.

Muscovite is a phyllosilicate mineral conformed by thin elastic laminae. Each lamina looks transparent and colorless under the naked eye, but as anisotropic mineral has birefringence. This property allows for observing interference colors under a petrographic microscope that counts with a polarizer and a polarizer/analyzer. Not only does the color analysis give us an idea about the mineral that has this behavior and thus, valid scientific information, but also generates colorful mosaics that evoke artistic works.

