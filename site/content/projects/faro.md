Title: Faro
Tags: movie
Date: 21-04-2017
Author: Laboratorium

<figure>
  <img src="images/2_c.JPG">
</figure>

Experimental movie based on Belousov-Zhabotinsky reaction. Elias Heuninck (Formlab), María Boto (Laboratorium), 2017, 3'.

Work made in collaborabation between Elias Heuninck and Laboratorium.
{: .credit}