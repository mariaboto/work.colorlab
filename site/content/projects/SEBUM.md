Title: SEBUM
Tags: users
Date: 3 May 2018
Author: LABORATORIUM

<figure>
  <img src="images/brenda.jpg">
</figure>


[SEBUM]( http://espacescalp.com/scalp3/index.html),  Brenda Lou Schaub in collaboration with Moussa Cheniguel, Quentin Lamouroux and Florian Ortega 

Sebum is the fat we can see if we don't wash our hair for a long time, it is natural fat we all do produce.  Sebum is a typography/logo with the word sebum, two videos, a 3D model of the skin with sebum and one text on an iPad fixed on a structure which usually used is for showers.  

 [More info](https://www.brendalouschaub.com/)
 
A project by Brenda Schaub.
{: .credit}