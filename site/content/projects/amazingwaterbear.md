Title: The amazing Tardigrade-Water bear
Tags: users
Date: 3 June 2018
Author: LABORATORIUM

<figure>
  <img src="images/amazing.png">
</figure>

We waited again for 10-15 min and then he slowly (really slow!!) started moving again. First with his legs and then with his head and then the body, but now it was less energetic and it looked like he was tired. His moves we’re really sleepy and he never moved as in the beginning.
I continued putting high frequencies and low ones but they didn’t seem to affect him at all. Then we put Beethoven’s 31st piano sonata on and it seemed like he was dancing to it in heavy ballet movements. Afterwards a pop song from Jax Jones but that one didn’t go well with his slow movements. I think he liked classical music more (haha).

A project by Arta Konjusha.
{: .credit}



