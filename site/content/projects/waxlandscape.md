Title: Wax Landscapes
Tags: users
Date: 30 May 2019
Author: LABORATORIUM

<figure>
  <img src="images/wax.png">
</figure>

The landscape is a recurring element in my practice. It is not always expressed in the same form. They can be existing, imaginary, divided, fragmented, elemental, distorted, moved, copied or self-designed. The landscape that is now present and dominant are the luxuriant greenhouses from the 19th century. When entering a
greenhouse, another world comes your way. A greenhouse is like an encyclopaedia and an escape route from reality.
Because of my fixation on vegetation and my systematic reproduction of it in all kinds of different media, I have come across similarities with other things. While viewing my detail-photos of different flowers, I often ended up with images that have an architectural, dancing or erotic character. Images of known objects often suggest something else. A huge number of stimuli influence our perception. As artists, you look for those stimuli. Where do they come from? Can they be manipulated? How can we put them into words or images?
With these questions, greenhouses as a world on its own, the idea that nature is a part of our image of a landscape in mind, I started to explore how a ‘natural-looking’ landscape can be formed.
It has the same idea as artificial nature nowadays. It’s a mix of a natural element (wax) that is sculpted by an unnatural element (laundry soap). Also by making detailed photos they really look like a landscape. It’s the same effect detailed photo’s of lowers can have.

A project by Hanne Fatah.
{: .credit}