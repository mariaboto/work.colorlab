Title: Mothcinema
Tags: users
Date: 10 May 2019
Author: LABORATORIUM

<figure>
  <img src="images/moth.png">
</figure>


Bestaat er ongerepte sneeuw? Dit werk gaat over het creeëren van een kunst of zuiver en natuurlijke cinema op basis van schaduwspelen. We vertrekken vanuit de idee dat er ‘geen ongerepte sneeuw bestaat’, dat alles wel al eens is gedaan in de kunst. Het plan is om een installatie-film te bouwen waarbij de personages van de film zelf worden gemaakt of beter gezegd, gekweekt. 
Op die manier trachten we toch een vorm van ongerepte sneeuw of zuivere gewaarwording te creëren. we documenteren het kweken en de levensloop van motten. We kunnen in zekere zin veronderstellen dat elk wezen vanaf zijn geboorte ongerepte sneeuw is. Dat elk organisme verschillend is en dat ze nog niet is bepaald door zijn omgeving. Daarom trachten we de personages van onze film zelf te kweken. we hebben gekozen voor de wasmot als personages omdat ze in de eerste instantie een hevige aantrekkingskracht tot licht vertonen, wat een essentiële factor is in cinema en dat wilde we optimaal benutten.
Daarnaast is beweging een vereiste in cinema. Deze proberen we als makers en regisseurs van de film te realiseren door een spel van licht en geluid uit te oefenen op de wasmot. De wasmot of de Galleria mellonella staat er om bekend een zeer breed geluidsspectrum (20 mHz-300 kHz) te kunnen waarnemen. Deze motvlinders gebruiken hun gehoor om roofdieren, vooral vleermuizen te ontwijken en voor paringsroepen. 
Wij beslissen over het leven van de motten doordat we ze in de eerste instantie kweek en ze vervolgens in een bepaalde leefomgeving plaats. De duur van deze filmprojectie zal de levenscyclus van een mot voorstellen, wat ongeveer twee weken zal zijn. Zo creëeren we als ‘regisseur’ of filmmaker een soort scheppende functie over de filmpersonages en maken we een eigen versie van ‘de grot van Plato’.

A project by Lewi Moors & Jacob Schoolmeesters.
{: .credit}

