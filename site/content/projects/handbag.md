Title: Handbag of bacterial land
Tags: users
Date: 1 May 2019
Author: LABORATORIUM

<figure>
  <img src="images/susan.jpg">
</figure>

1- Boil the water(200liter) .
2- Put the tea backs for 30 minutes.
3- Then take the bags ,and add sugar(200g).
4- Add the vinegar (300ml).
5- Add the culture (piece of SCOBY).
6- Then cover up it ,and wait more than two weeks.
7- Then remove solution and squeeze the layer of cellulose in order to be dry .
8- Paint it before it becomes dry.
9- After painting hang it and wait until it will be dried.
10- For adding some textures , I ley the layer of cellulose on the paper(when it was wet) .
11- Then I sewed a handbag.

A project by Susan Moaez (graphic design KASK).
{: .credit}


