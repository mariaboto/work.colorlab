Title: Algae Publication OSP
Tags: users
Date: 3 December 2018
Author: LABORATORIUM

<figure>
  <img src="images/OSP.jpg">
</figure>

This poster/leaflet is under Free Art License Art Libre and has been generated with a Python script. This publication has been plotted with living colors derived from the Color Biolab research project of Maria Boto Ordonez and Kristel Peters within KASK Laboratorium. 
Part of the Color Biolab research, initiated by Laboratorium with OSP (Einar Andersen, Gijs de Heij & Sarah Magnan).
{: .credit} 


[More info](http://osp.kitchen/work/colorlab/)