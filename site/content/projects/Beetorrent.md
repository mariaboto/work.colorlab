Title: Beetorrent
Tags: users
Date: 20 May 2019
Author: LABORATORIUM

<figure>
  <img src="images/bees1.jpg">  
  <img src="images/bees2.jpg">
  <img src="images/bees3.jpg">
  <img src="images/bees5.jpg">
</figure>

Research in the potential of propolis as a mummification material for the project "Beetorrent" by Jerry Galle.

<i>"As the Arctic stronghold of world’s seeds was flooded after permafrost melted, the Global Seed Vault is possibly in danger. No seeds were lost yet, but the ability of the rock vault to provide failsafe protection against all disasters is now threatened by climate change. Taking care of preserving as much ‘informed matter’ as we can ourselves, beeTorrent could be of some help."<i>

This project is part of ["Tools for things and Ideas"](https://tools-for-things-and-ideas.github.io/)
{: .credit} 

A project by Jerry Galle.
{: .credit}

[More info](https://tools-for-things-and-ideas.github.io/beetorrent)
