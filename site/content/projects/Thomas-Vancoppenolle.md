Title: Thomas Vancoppenolle
Tags: Users
Date: 2 May 2018
Author: LABORATORIUM

<figure>
  <img src="images/thomas.jpg">
</figure>

Once photographed however, the depth of this physical object is reduced to a singular surface again.
But when looking at these photos once they have been scanned and edited, a sense of imperfection rises. Apart from the fact that we cannot see a physical depth, a kind of doubt is raised when confronted with this image. Despite seeing ‘one’ image in front of us, the viewer is urged to think about the aspect of time that might have been needed to realise an image of this kind. 

[More info](https://www.instagram.com/thomasvancoppenolle/)

A project by Thomas Vancoppenolle.
{: .credit}
