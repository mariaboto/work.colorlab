Title: Ressurected Object
Tags: users
Date: 3 February 2019
Author: LABORATORIUM

<figure>
  <img src="images/jerry.jpg">  
</figure>

In this work of the artist [Jerry Galle](https://jerrygalle.com), mycelium is growing inside of a PLA sculture printed by [FORMLAB]( http://www.formlab.schoolofarts.be). The deformation is captured by camera attached to a robot arm and recognize it by a AI program.

[More info](https://jerrygalle.com/projects/ressobject/)

A project by Jerry Galle.
{: .credit}