Title: Tierra de Diatomeas
Date: 21-04-2017
Author: LABORATORIUM

<figure>
  <img src="images/musac_lab987_tierradediatomeas_1_xl.jpg" />
  <img src="images/TD1.jpg">  
  <img src="images/TD2.jpg">
  <img src="images/TD3.jpg">
</figure>

<i>Land of Diatoms</i>, a project by Susana, Inés Cámara Leret and María Boto Ordóñez to investigate the relationship between human beings and Castilla and León geographic areas. This research is part of a collaboration with the Laboratory of Diatomology of the University of León, in which diatoms are used as a metaphor to create a dialogue through which to discover historical events, trace existing narratives and discuss desirable futures for the land, its waters and its people.

Due to the high sensitivity of diatoms to chemical changes in the water, these microorganisms are used to analyse ecological parameters such as the quality of the water. Thanks to the fact that diatom cells are covered by a silica wall, they are fossilizable, and through their study, it is possible to analyse agricultural cycles marked by the arrival of the Romans, the Industrial Revolution or livestock farming, for example. As a consequence of their easy dispersion they can be found in any environment, not only in places where there is or there was water. At the same time, diatoms also help to outline a map of invasions through the study of the migration, dispersal and propagation of species as a result of human activities.

<i>Land of Diatoms</i> has been accompanied by researcher Dr. Saúl Blanco Lanza (Diatomology Lab from the University of León) and botanist Dr. Estrella Alfaro. It is commissioned by LAB987 at MUSAC, supported by Universidad de León, represented by the Area de Actividades Culturales and Departamento de Diatomología from Facultad de Ciencias Biológicas and The University College Ghent, School of Arts KASK.
{: .credit}

