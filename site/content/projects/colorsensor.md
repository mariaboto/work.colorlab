Title: Color sensor
Tags: users
Date: 27 August 2019
Author: LABORATORIUM

<figure>
  <img src="images/colorsensor1.jpg">  
  <img src="images/colorsensor2.png">
  <img src="images/colorsensor3.jpg">
</figure>

Color sensor by using  ColorPAL Colour Sensor Module (Parallax Inc 28380). 
The sensor is able to measure and emit any color. Connected to a raspberry pie+screen can show the reading on the screen.
This project is developed as an extension for [neochromologism](neochromologism.io) database to measure and name colors.

Project developed in collaboration with Daniel Gonzalez and Juan Luis Font.
{: .credit}

Send us an [email](info@laboratorium.bio) for the arduino code and documentation.