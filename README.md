#Colorlab

Colorlab is research based project in collaboration and initiated by Maria Boto Ordonez. Maria is a scientist, see [http://laboratorium.bio](http://laboratorium.bio)

In research between art and science, we experiment on a playground drawn by living color and a plotter. We use a set of color based on living organism like algues produced by Maria in her laboratorium at Kask, Gent.