
outer_diameter = 11.5;
outer_radius = outer_diameter / 2;
handle_radius = (16.5 / 2);
handle_thickness_outer = 1.3;
handle_thickness_inner = 1.9;
handle_position = 0;
facets = 128;

module nut ()
{
    scale ([.98,.98,1]) import("montana-adapter-thread.stl");
}


// Base tube
intersection(){
    cylinder(d=11.5, h=10, $fn=128);
    nut ();
}

difference(){
    // Handle
    union(){
        // base
        translate([0, 0, handle_position])
        cylinder(h=handle_thickness_outer, r=handle_radius, $fn=facets);
        
        // angled top
        translate([0, 0, handle_position + handle_thickness_outer])
        cylinder(h=handle_thickness_inner - handle_thickness_outer, r1=handle_radius, r2=outer_radius, $fn=facets);
    }
    cylinder(d=11., h=handle_thickness_inner, $fn=facets);
}


// Angled middle
translate([0,0,10])
difference(){
    union(){
        cylinder(d=outer_diameter, h=5, $fn=facets);
        translate([0,0,5]) cylinder(d1=outer_diameter,d2=6.5, h=2, $fn=facets);
    }
    
    cylinder(d1=6.5, d2=4.5, h=7, $fn=facets);
    cylinder(d=10, h=1.5, $fn=facets);
    translate([0, 0, 1.5]) cylinder(d1=10, d2=5, h=3, $fn=facets); // h was 2.5
}


// Straight tip
module guide()
{
    translate ([0,2.25,3.5])
    rotate ([90,0,0])
    intersection(){
        translate ([0,30,0])
        rotate ([90,0,0])
        linear_extrude($fn=20, height = 60) polygon(  points=[[-.4,0],[-.4,.7],[.4,.7],[.4,0]] );

        rotate ([0,0,90])
        translate ([0,10,0])
        rotate ([90,0,0])
        linear_extrude($fn=20, height = 20) polygon(points=[[-2.5,0],[-1.5,1],[1.5,1],[2.5,0]] );
    }
}

translate([0,0,17]){
    difference(){
        cylinder(d1=6.5,d2=6.3, h=7, $fn=128);
        cylinder(d1=4.5, d2=4.2, h=7, $fn=128);
    }
    for (angle=[0:45:315]) rotate ([0,0,angle]) guide();
}

//intersection(){
//    for (angle=[0:45:315]) rotate ([0,0,angle]) translate ([5.75+0.5,0,0]) cube([10,0.4,1.5]);
//    cylinder (r1=5.75+0.5+5+5,r2=5.75+0.5+5+5-2, h=1.5, $fn=128);
//}

//difference(){
//    cylinder (r1=5.75+0.5+5+8,r2=5.75+0.5, h=0.7, $fn=128);
//    cylinder (r=5.75+1, h=1, $fn=128);
//}