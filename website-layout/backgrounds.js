(function () {
  var selector = '.patterned, .patterned-border',
      background_selector = '.patterned-background';

  function initBackgrounds() {
    document.querySelectorAll(selector).forEach(function (el) {
      el.dataset.rotation = parseInt(Math.random() * 90);
      el.dataset.speed = (Math.random() * 3) - 1.5;
    });

    var lastScrollY = null;

    function updateBackgrounds () {
      if (window.scrollY !== lastScrollY) {
        document.querySelectorAll(selector).forEach(function (el) {
          var rotation = Math.round((window.scrollY * parseFloat(el.dataset.speed) + parseInt(el.dataset.rotation)) % 360);
          el.style.setProperty('--background-rotation', rotation.toString() + 'deg')
        });
        lastScrollY = window.scrollY;
      }

      window.requestAnimationFrame(updateBackgrounds);
    }

    updateBackgrounds();
  }

  window.initBackgrounds = initBackgrounds;
})();