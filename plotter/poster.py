#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from chiplotle.hpgl import commands
from chiplotle import instantiate_plotters
from chiplotle.tools.plottertools import instantiate_virtual_plotter
from plotterTools import configureMemory, PlotterMargins, PlotterOffset
from plotterTools.units import mm
from plottertools.font import Font
from plottertools.textbox import Textbox
from chiplotle.geometry.core.coordinate import Coordinate
import math

"""
  Return amount of rectangles that fit and space in between them
"""


def getCount(length, unit):
    return int(length) / int(unit)


def getMargin(length, unit, count):
    return (length % unit) / (count - 1)


def getCountAndMargin(length, unit):
    count = getCount(length, unit)
    return (count, getMargin(length, unit, count))


def makeCombinations(limit, tail=[]):
    if limit > 0:
        return makeCombinations(limit - 1, tail + [[limit]] + [[limit] + r for r in tail])

    return tail


VIRTUAL = 'virtual'
HARDWARE = 'hardware'

mode = VIRTUAL

plotter = instantiate_virtual_plotter(left_bottom=(-8400,-12360), right_top=(8400, 11400),
    type='HP7576A') if mode == VIRTUAL else instantiate_plotters()[0]
configureMemory(plotter, 9678, 2048, 0, 0, 0, 1024)

plotter.rotate(90)

pagemargins = PlotterMargins(
    plotter.margins.hard.all_coordinates, PlotterOffset(mm(10), mm(4), mm(10), mm(4)))

print pagemargins.left, pagemargins.right

shapesize = mm(70)

drawingsize = [pagemargins.right - pagemargins.left,
               pagemargins.top - pagemargins.bottom]


rectcounts, shapemargins = zip(*[getCountAndMargin(l, shapesize)
                                 for l in drawingsize])

def sortCombinations(a,b):
    la = len(a)
    lb = len(b)

    if la <> lb:
        return 1 if (la > lb) else -1
    else:
        for i in range(la):
            ia = a[i]
            ib = b[i]

            if ia <> ib:
                return 1 if (ia > ib) else -1

        return 0

pencount = 5
pens = range(1, pencount + 1)
angles = [15 + pen * 3  for pen in pens] # [pen * 15 for pen in pens]
layers = [[] for pen in pens]
combinations = sorted(makeCombinations(pencount), cmp=sortCombinations)

rows = math.ceil(len(combinations) / float(rectcounts[0]))
verticalmargins = int((drawingsize[1] - (rows * shapesize)) / (rows-1))

penlabels = ['Arthrospira', 'Arthrospira', 'Synth. astaxanthin', 'D.salina', 'P.cruentum']

for r, y in enumerate(range(pagemargins.top - shapesize, pagemargins.bottom-10, -(shapesize + verticalmargins))):
    for c, x in enumerate(range(pagemargins.left, pagemargins.right, shapesize + shapemargins[0])):
        if (combinations):
            combination = combinations.pop(0)
            for l, pen in enumerate(combination):
                layers[pen - 1].extend([
                    commands.PA([(x, y)]),
                    commands.RA((x + shapesize, y + shapesize)),
                    commands.PA([(x + l * mm(8), y - mm(2))]),
                    commands.LO(3),
                    commands.SI(.25,.45),
                    commands.LB('{0} {1}'.format(pen, penlabels[pen-1]) if r == 0 else '{0}'.format(pen))
                ])




def biolabText (position, text, font, fontoffset):
    offsets = [Coordinate(0,0),Coordinate(fontoffset, 0), Coordinate(0, -fontoffset), Coordinate(-fontoffset, 0), Coordinate(0, fontoffset)]
    hpgl = []

    for offset in offsets:
        textbox = Textbox(
            font=font,
            width=mm(420),
            position=position + offset,
            align=Textbox.alignLeft,
            lineHeight=1.40,
            letterSpacing=0
        )

        
        textbox.insertText(text)


        hpgl.append('{};'.format(textbox.hpgl()))


    return hpgl

font = Font(path='fonts/converted/futural.fnt', resolution=1, scale=45)
typography_layers1 = biolabText(Coordinate(pagemargins.left + shapesize * 1.4, pagemargins.bottom), 'LIVING', font, mm(1.5))
typography_layers2 = biolabText(Coordinate(pagemargins.left + shapesize * 3.2, pagemargins.bottom), 'COLORS', font, mm(1.5))

for pen, angle, hpgl in zip(pens, angles, layers):
    plotter.write([commands.SP(pen), commands.VS(
        8), commands.FS(18), commands.FT(3, mm(2.5), angle)])
    plotter.write(hpgl)
    plotter.write(typography_layers1.pop(0))
    plotter.write(typography_layers2.pop(0))
    con = raw_input('continue ?')

# for i, commands in enumerate(layers):
#     pen = pens


if mode == VIRTUAL:
    from chiplotle import io
    io.view(plotter)
