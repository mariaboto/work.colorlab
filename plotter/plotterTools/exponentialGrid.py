#!/usr/bin/env python
# -*- coding: utf-8 -*-

# delta y = f (x) { n² }

from chiplotle.geometry.core.group import Group
from chiplotle.geometry.shapes.line import line

import math

def exponentialGrid (plotter, margins, cell_width, cell_height, base=2, start_n=0):
    
    g = Group()
    i = 0
    
    for x in range(margins.left, margins.right, cell_width):
        # vertical
        if i % 2:
            l = line((x, margins.top), (x, margins.bottom))
        else:
            l = line((x, margins.bottom), (x, margins.top))
        g.append(l)
        i += 1
    #y = margins.bottom

    #n = start_n
    #x = margins.left

    #while x < margins.right:
        #x += (pow(base * 1.1, abs(n * .95)) * cell_height)
        
        ##print max(abs(math.cos(n)) * math.pi, 1)
        
        ##y += max(abs(math.cos(n)), .5) * cell_height
        
        #l = line((x, margins.top), (x, margins.bottom))
        #g.append(l)
        #n += 1

    y = margins.bottom
    n = start_n
    i = 0

    while y < margins.top:
        y += (pow(base, abs(n)) * cell_height)
        
        #print max(abs(math.cos(n)) * math.pi, 1)
        
        #y += max(abs(math.cos(n)), .5) * cell_height
        
        if i % 2:
            l = line((margins.left, y), (margins.right, y))
        else:
            l = line((margins.right, y), (margins.left, y))
        g.append(l)
        n += 1
        i += 1
        
    return g