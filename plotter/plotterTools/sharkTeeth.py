#!/usr/bin/env python
# -*- coding: utf-8 -*-

from chiplotle.geometry.core.path import Path
from chiplotle.geometry.core.group import Group
from chiplotle.geometry.core.coordinatearray import CoordinateArray

def sharkTeeth (plotter, margins, width, height, step):
    g = Group()
        
    for y in range(margins.bottom, margins.top - height, step):
        points = CoordinateArray([(margins.left, y)])
        
        for x in range(margins.left, margins.right, width):
            points.extend(CoordinateArray([
                (x + width * .5, y + height),
                (x + width, y)
            ]))
            
        g.append(Path(points))

    return g