from units import mm


class CurvesConfig:
    def __init__(self, size=(mm(10), mm(5)), interval=mm(1), interpolation_count=50, pen=1, speed=1, force=1):
        self.size = size
        self.interval = interval
        self.interpolation_count = interpolation_count
        self.pen = pen
        self.speed = speed
        self.force = force
