from units import mm


class TeethConfig:
    def __init__(self, size=(mm(10), mm(10)), interval=mm(5), pen=1, speed=1, force=1, offset=(0, 0)):
        self.size = size
        self.interval = interval
        self.pen = pen
        self.speed = speed
        self.force = force
        self.offset = offset
