from chiplotle.geometry.core.path import Path
from chiplotle.geometry.core.group import Group
from chiplotle.geometry.shapes.line import line

def efficientGrid(margins, cell_width, cell_height):
   '''Rectangular grid. 

   - `width` : ``int`` or ``float``, width of the rectangle.
   - `height` : ``int`` or ``float``, height of the rectangle.
   - `width_divisions` : ``int``, number of horizontal equidistant partitions.
   - `height_divisions` : ``int``, number of vertical equidistant partitions.
   
   '''

   ul_x = margins.left
   bl_x = ul_x
   ur_x = margins.right

   ul_y = margins.top
   ur_y = ul_y
   bl_y = margins.bottom
   
   width_divisions = margins.width / cell_width
   height_divisions = margins.height / cell_height

   g = Group()
   
   ## add horizontal lines
   for i in range(0, height_divisions + 1, 2):
      step_y = cell_height * i
      l = line((ul_x, ul_y - step_y), (ur_x, ur_y - step_y))
      g.append(l)
      step_y = cell_height * (i + 1)
      l = line((ur_x, ur_y - step_y), (ul_x, ul_y - step_y))
      g.append(l)

   ## add vertical lines

   for i in range(0, width_divisions + 1, 2):
      step_x = cell_width * i
      l = line((ul_x + step_x, ul_y), (bl_x + step_x, bl_y))
      g.append(l)
      step_x = cell_width * (i + 1)
      l = line((bl_x + step_x, bl_y), (ul_x + step_x, ul_y))
      g.append(l)

   return g

## RUN DEMO CODE

if __name__ == '__main__':
   from chiplotle.tools import io
   gr = grid(1000, 2000, 10, 20)
   assert isinstance(gr, Group)
   print gr.format
   io.view(gr)
