#!/usr/bin/env python
# -*- coding: utf-8 -*-

from chiplotle.geometry.core.path import Path
from chiplotle.geometry.core.group import Group
from chiplotle.geometry.core.coordinatearray import CoordinateArray


def sharkTeethSimple(margins, width, height, interval):
    g = Group()

    for y in range(margins.bottom, margins.top - height, interval):
        points = [(x, y if (k % 2) else y + height)
                  for k, x in enumerate(range(margins.left, margins.right, width / 2))]

        g.append(Path(CoordinateArray(points)))

    return g
