from chiplotle.geometry.core.path import Path
from chiplotle.geometry.core.group import Group
from chiplotle.geometry.shapes.line import line
from units import mm
from random import randint


def deviationGrid(margins, cell=(mm(10), mm(10)), deviation=(mm(-2), mm(2))):
    grid = [
        [(
            x + randint(deviation[0], deviation[1]),
            y + randint(deviation[0], deviation[1])
        ) for x in range(margins.left, margins.right, cell[0])]
        for y in range(margins.bottom, margins.top, cell[1])]

    horizontal = Group([Path(reversed(row) if key % 2 else row)
                        for key, row in enumerate(grid)])
    # Unzip rows into columns
    vertical = Group([Path(reversed(col) if key % 2 else col)
                      for key, col in enumerate(zip(*grid))])

    return Group([horizontal, vertical])
