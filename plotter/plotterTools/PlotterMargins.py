from chiplotle.geometry.core.coordinate import Coordinate
from PlotterOffset import PlotterOffset

class PlotterMargins:
    def __init__ (self, coordinates, offset=None):
        self.all_coordinates = coordinates
        self.offset = offset if offset else PlotterOffset()
    ## PROPERTIES ##


    @property
    def left(self):
        return self.all_coordinates[0] + self.offset.left

    @property
    def bottom(self):
        return self.all_coordinates[1] + self.offset.bottom

    @property
    def right(self):
        return self.all_coordinates[2] - self.offset.right

    @property
    def top(self):
        return self.all_coordinates[3] - self.offset.top

    @property
    def width(self):
        return self.right - self.left
    @property
    def height(self):
        return self.top - self.bottom

    @property
    def center(self):
        #return (self.right + self.left) / 2., (self.top + self.bottom) / 2.
        x = (self.right + self.left) / 2.
        y = (self.top + self.bottom) / 2.
        return Coordinate(x, y)

    @property
    def bottom_left(self):
        coords = self.all_coordinates
        return Coordinate(self.bottom, self.left)

    @property
    def bottom_right(self):
        coords = self.all_coordinates
        return Coordinate(self.bottom, self.right)

    @property
    def top_right(self):
        coords = self.all_coordinates
        return Coordinate(self.top, self.right)

    @property
    def top_left(self):
        return Coordinate(self.top, self.left)

    # @property
    # def all_coordinates(self):
    #     self._plotter._serial_port.flushInput( )
    #     self._plotter._write_string_to_port(self._queryCommand.format)
    #     m = self._plotter._read_port( ).rstrip('\r').split(',')
    #     return tuple([eval(n) for n in m])