from chiplotle.geometry.shapes.bezier_path import bezier_path
from chiplotle.geometry.core.group import Group
from units import mm
from random import randint


def curves(margins, size=(mm(10), mm(10)), interval=mm(1), interpolation_count=50):
    lines = []

    for y in range(margins.bottom, margins.top, interval):
        line = []

        for x in range(margins.left, margins.right, size[0]):
            line.extend([
                (x, y),
                (x + size[0] * .25, y + size[1]),
                (x + size[0] * .5, y),
                (x + size[0] * .75, y - size[1])
            ])

        lines.append(line)

    return Group([bezier_path((reversed(line) if key % 2 else line), 1, interpolation_count)
                  for key, line in enumerate(lines)])
