# Exports a Python iterable to JSON. Currently supports list, tuple and dict
class Writer (object):
    buff = ''
    newLine = '\n'
    tab = '\t'
    
    _stringBuff = ''
    
    __tabs = 0
    _terminator = ',{0}'.format (newLine)
    _key = '"{0}": '
    _list = ('[', ']')
    _dict = ('{', '}')
    
    def __init__ (self, value = False):
        if (value <> False):
            self.buff = value
    
    def write (self, path):
        self.build ()
            
        try:
            with open (path, 'w') as self.f:
                self.f.write (self._stringBuff)
                self.f.close ()
        except IOError:
            return False
            
        return True
    
    def build (self):
        self._stringBuff = self._writeValue (self.buff)
            
        return self._stringBuff
    
    def _tabs (self):
        return self.__tabs * self.tab
    
    def _increaseTabs (self, amount = 1):
        self.__tabs += amount
    
    def _decreaseTabs (self, amount = 1):
        self.__tabs += -1 * amount
            
    def _writeKey (self, key):
        return self._key.format (key)
    
    def _writeValue (self, value):
        if (type (value) == int):
            return self._writeInt (value)
                    
        if (type (value) == float):
            return self._writeFloat (value)
                    
        if (type (value) == str or type (value) == unicode):
            return self._writeStr (value)
        
        if (type (value) == list):
            return self._writeList (value)
                
        if (type (value) == dict):
            return self._writeDict (value)
                    
        if (type (value) == tuple):
            return self._writeTuple (value)
    
    def _writeInt (self, value):
        return '{0:d}'.format (value)
    
    def _writeFloat (self, value):
        return '{0:n}'.format (value)
    
    def _writeStr (self, value):
        return '"{0}"'.format (value)
    
    def _writeList (self, _list):
        buffList = []
        
        self._increaseTabs ()
        buff = self._list[0] + self.newLine
        
        for value in _list:
            buffList.append (self._tabs() + self._writeValue (value))
        
        buff += self._terminator.join (buffList)
        buff += self.newLine
        self._decreaseTabs ()
        buff += self._tabs () + self._list[1]
        
        return buff
    
    def _writeTuple (self, _tuple):
        return self._writeList (_tuple)
    
    def _writeDict (self, _dict):
        buffList = []
        
        self._increaseTabs ()
        buff = self._dict[0] + self.newLine

        for key in _dict:
            buffList.append (self._tabs() + self._writeKey(key) + self._writeValue (_dict[key]))
        
        buff += self._terminator.join (buffList)
        buff += self.newLine
        self._decreaseTabs ()
        buff += self._tabs () + self._dict[1]
        
        return buff
                            