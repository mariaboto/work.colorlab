#!/usr/bin/env python
# -*- coding: utf-8 -*-

class PlotterOffset (object):
  def __init__ (self, top=0, right=0, bottom=0, left=0):
    self.top = top
    self.bottom = bottom
    self.left = left
    self.right = right