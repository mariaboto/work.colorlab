from units import mm


class DeviationGridConfig:
    def __init__(self, cell=(mm(10), mm(10)), deviation=(mm(-2), mm(2)), pen=1, speed=1, force=1):
        self.cell = cell
        self.deviation = deviation
        self.pen = pen
        self.speed = speed
        self.force = force
