#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
    Script generating the colortest poster.
"""

from chiplotle.hpgl import commands
from chiplotle import instantiate_plotters
from chiplotle.tools.plottertools import instantiate_virtual_plotter
from chiplotle.geometry.core.coordinate import Coordinate
import math

"""
  Return amount of rectangles that fit and space in between them
"""

VIRTUAL = 'virtual'
HARDWARE = 'hardware'

mode = VIRTUAL

plotter = instantiate_virtual_plotter(left_bottom=(-8400,-12360), right_top=(8400, 11400),
    type='HP7576A') if mode == VIRTUAL else instantiate_plotters()[0]

text = "LIVING COLORS"
charwidth = 3
charheight = 4.5
slant = 60 # angle
colors = 3
offset = 100


for i in range(1, colors):
  slant = (i+1)*5
  distance = charheight * math.tan(math.radians(slant))

  plotter.write([
    commands.SP(i),
    commands.PA([(-distance*200, 0)]),
    commands.SI(charwidth, charheight),
    commands.SL(math.tan(math.radians(slant))),
    commands.LB(text)
  ])

  plotter.write([
    commands.SP(i),
    commands.PA([(distance * 200, 0)]),
    commands.SI(charwidth, charheight),
    commands.SL(-math.tan(math.radians(slant))),
    commands.LB(text)
  ])

if mode == VIRTUAL:
    from chiplotle import io
    io.view(plotter)