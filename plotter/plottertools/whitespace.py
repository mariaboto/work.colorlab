#!/usr/bin/env python
#! -*- coding: utf-8 -*-

# Whitespace buffer. Behaves like a character object
class Whitespace (object):
    def __init__ (self, width = False):
            self.width = width if width <> False else 0
                
            return 'PR0,{0:.1f}'.format (self.width)