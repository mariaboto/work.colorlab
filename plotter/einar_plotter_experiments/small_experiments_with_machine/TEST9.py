from chiplotle import *
import random

plotter = instantiate_plotters( )[0]

plotter.select_pen(1)

coords = [(x, random.randint(0, 1000)) for x in range(0, 1000, 10)]
plotter.write(hpgl.PU([(8079,5520)]))
plotter.write(hpgl.PD(coords))

plotter.select_pen(0)
