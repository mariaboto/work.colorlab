from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)


coords = []
cond = True
x = 0
max_y = 10000
min_y = 8000
for i in range(1,25):
    if cond == True:
        x += 500
        if i % 2 == 0:
            y = max_y
        else:
            y = min_y
    else:
        x -= 500
        if i % 2 == 0:
            y = min_y
        else:
            y = max_y

    coords.append((x,y))
    if x == 5000:
        cond = False
        coords.append((x,min_y))
    elif x== 0:
        cond = True
        coords.append((x,max_y))

path = shapes.catmull_path(coords,10)
plotter.write(path)


plotter.select_pen(0)
