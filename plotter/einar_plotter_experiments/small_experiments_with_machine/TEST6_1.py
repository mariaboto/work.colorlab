x = 1
y = 1

x_increment = 570
y_increment = 830

y_limit = 5520
x_limit = 4039

x_turns = 0

counter = 0

while x_turns < 5:
	if x >= x_limit:
		x_increment = -(x_increment)
		x_turns += 1
	elif x <= 0:
		x_increment = -(x_increment)
		x_turns += 1
	if y >= y_limit:
		y_increment = -(y_increment)
	elif y <= 0:
		y_increment = -(y_increment)

	# if counter % 2 == 0:
	# 	print("FT3,20,45;")
	# else:
	# 	print("FT3,20,0;")
	print("PU%s,%s;" % (x,y))
	print("CI100;")
	x += x_increment
	y += y_increment

	# counter += 1
