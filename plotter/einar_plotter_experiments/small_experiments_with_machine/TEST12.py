from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)
x = 0
y = 7000

for i in range(1,11):
    plotter.write("PA%s,%s" % (x,y))
    fill_dens = (i * 10)
    plotter.write("FT3,%s,135" % (fill_dens))
    rect = hpgl.RR((1000,1000))
    plotter.write(rect)
    x += 1000

plotter.select_pen(0)
