import math
# print("PU0,0")
# x = 5000
# y = 5000
# #prints circles in a ellipse
# for i in range(45):
# 	x += int(math.sin(i)*600)
# 	y += int(math.cos(i)*800)
# 	print("PA%s,%s" % (x,y))
# 	print("CI100")
# #prints a line back and forth of circles
# for i in range(45):
# 	x += int(math.sin(i)*600)
# 	y += int(math.sin(i)*600)
# 	print("PA%s,%s" % (x,y))
# 	print("CI50")
# #prints a circle of squares
# for i in range(45):
# 	x += int(math.sin(i)*600)
# 	y += int(math.cos(i)*600)
# 	print("PA%s,%s" % (x,y))
# 	print("ER500,500")

#Draws a pingpong like line around the paper with squares
x = 0
y = 0

x_increment = 570
y_increment = 830

y_limit = 10000
x_limit = 15000

x_turns = 0

counter = 0

while x_turns < 2:
	x += x_increment
	y += y_increment
	if x >= x_limit:
		x_increment = -(x_increment)
		x_turns += 1
	elif x <= 0:
		x_increment = -(x_increment)
		x_turns += 1
	if y >= y_limit:
		y_increment = -(y_increment)
	elif y <= 0:
		y_increment = -(y_increment)

	if counter % 2 == 0:
		print("FT3,50,45;")
	else:
		print("FT3,50,0;")
	print("PU%s,%s;" % (x,y))
	print("RR500,500;")
	counter += 1
