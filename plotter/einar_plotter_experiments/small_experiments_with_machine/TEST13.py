from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)

y = 5000
coords = [(x, y - (math.cos(x)*1000)) for x in range(0, 5000,500)]
path = shapes.bezier_path(coords,1)
plotter.write(path)
transforms.rotate(path, 3.14 / 4)
transforms.center_at(path,(2249.8282566037733, 4989.920526892924))
plotter.write(path)




plotter.select_pen(0)
