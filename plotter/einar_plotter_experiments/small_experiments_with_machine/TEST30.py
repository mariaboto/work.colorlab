#Base for Nannochloropsis

from chiplotle import *
import math
import random

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

cell_shape = shapes.catmull_path([(0,40),(0,100),(40,140),(100,140),(140,100),(60,100),(60,40),(140,40),(100,0),(40,0),(0,40),(0,100),(40,140)])

x = 100
y = 100
deg = [0,45,90,135,180,225,270,315]
for i in range(1, 401):
    # transforms.rotate(cell_shape, math.radians(random.choice(deg)))
    transforms.rotate(cell_shape, math.radians(random.randint(0,359)))
    transforms.center_at(cell_shape,(x,y))
    plotter.write(cell_shape)
    x += cell_shape.width *1.2
    if i % 20 == 0:
        x = 100
        y += cell_shape.height *1.2


io.view(plotter)
