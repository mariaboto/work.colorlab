from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)

# for i in range(4,6):
#     height = i * 1000
coords = []
amplitude = 1000
frequency = 0.25
center_of_wave = 3000
for i in range (50):
    x = i*100
    y = amplitude * math.sin(frequency * x) + center_of_wave
    coords.append((x,y))
path = shapes.catmull_path(coords,5)

plotter.write(path)


plotter.select_pen(0)
