#creates a wave inside a wave

from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)


amplitude = 500
frequency = 0.5
center_of_wave_1 = 3000
center_of_wave_2 = 3500

coords = []
coords_2 = []

for i in range (50):
    x1 = i*100
    y1 = amplitude * math.sin(frequency * x1) + center_of_wave_1
    x2 = i*100
    y2 = amplitude * math.sin(frequency * x2) + center_of_wave_2
    if i % 4 == 0:
        coords.append((x1,y1))
        coords_2.append((x2,y2))
    elif (i+2) % 4 == 0:
        coords_2.append((x1,y1))
        coords.append((x2,y2))
wave_coords = coords + list(reversed(coords_2))

path = shapes.catmull_path(wave_coords,10)
plotter.write(path)

plotter.select_pen(0)
