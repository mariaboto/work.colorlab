#Base for P.cruentum

from chiplotle import *
import math
import random

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

def random_nums(total, n):
    numbers = []
    sum = 0
    for number in range(n-1):
        num = random.randint(170,230)
        numbers.append(num)
        sum += num
    numbers.append(total-sum)
    return numbers



y_pos = [100,100,100,100,100,100,100,100,100,100]
y = random_nums(2000, 10)

# print(coords)
for r in range(len(y_pos)):
    x = random_nums(2000, 10)
    random.shuffle(x)
    future_y = random_nums(2000, 10)
    random.shuffle(future_y)
    x_pos = 100
    for c in range(len(x)):
        cell = shapes.group([])

        outer_shape = shapes.ellipse(x[c],y[c])
        inner_shape = shapes.ellipse(x[c]*0.7,y[c]*0.7)
        cell.append(outer_shape)
        cell.append(inner_shape)

        transforms.center_at(cell, (x_pos , y_pos[c]))
        plotter.write(cell)
        y_pos[c] += y[c]/2 + future_y[c]/2
        if c != len(x)-1:
            x_pos += x[c]/2 + (x[c+1]/2)
    y = future_y




io.view(plotter)
