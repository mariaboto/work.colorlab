#creates a wave inside a wave

from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)


coords = []
coords_2 = []

for i in range (50):
    x1 = i*100
    y1 = 300 * math.sin(x1) + 2000
    x2 = i*100
    y2 = 300 * math.sin(x2) + 3000
    if i % 4 == 0:
        coords.append((x1,y1))
        coords_2.append((x2,y2))
    elif (i+2) % 4 == 0:
        coords_2.append((x1,y1))
        coords.append((x2,y2))


path = shapes.catmull_path(coords,10)
path_2 = shapes.catmull_path(coords_2,10)
plotter.write(path)

plotter.write(path_2)

plotter.select_pen(0)
