from chiplotle import *
import math
import random

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

def random_nums(total, n):
    numbers = []
    sum = 0
    for number in range(n-1):
        num = random.randint(180,220)
        numbers.append(num)
        sum += num
    numbers.append(total-sum)
    return numbers

x = random_nums(2000, 10)
y = random_nums(2000, 10)
x2 = random_nums(2000, 10)
y2 = random_nums(2000, 10)
x3 = random_nums(2000, 10)
y3 = random_nums(2000, 10)
x4 = random_nums(2000, 10)
y4 = random_nums(2000, 10)

for e in range(len(x)):
    print (x[e],y[e])


x_pos = 100
y_pos = 100
# print(coords)
for c in range(len(x)):
    cell = shapes.group([])

    outer_shape = shapes.ellipse(x[c],y[c])
    inner_shape = shapes.ellipse(x[c]*0.7,y[c]*0.7)
    cell.append(outer_shape)
    cell.append(inner_shape)

    transforms.center_at(cell, (x_pos , y_pos))
    plotter.write(cell)
    if c != len(x)-1:
        x_pos += x[c]/2 + (x[c+1]/2)
x_pos = 100
for c in range(len(x2)):
    cell = shapes.group([])

    outer_shape = shapes.ellipse(x2[c],y2[c])
    inner_shape = shapes.ellipse(x2[c]*0.7,y2[c]*0.7)
    cell.append(outer_shape)
    cell.append(inner_shape)

    transforms.center_at(cell, (x_pos , y_pos+y[c]/2+y2[c]/2))
    plotter.write(cell)
    if c != len(x)-1:
        x_pos += x2[c]/2 + (x2[c+1]/2)
x_pos = 100
for c in range(len(x3)):
    cell = shapes.group([])

    outer_shape = shapes.ellipse(x3[c],y3[c])
    inner_shape = shapes.ellipse(x3[c]*0.7,y3[c]*0.7)
    cell.append(outer_shape)
    cell.append(inner_shape)

    transforms.center_at(cell, (x_pos , y_pos+y[c]/2+y2[c]+y3[c]/2))
    plotter.write(cell)
    if c != len(x)-1:
        x_pos += x3[c]/2 + (x3[c+1]/2)
x_pos = 100
for c in range(len(x4)):
    cell = shapes.group([])

    outer_shape = shapes.ellipse(x4[c],y4[c])
    inner_shape = shapes.ellipse(x4[c]*0.7,y4[c]*0.7)
    cell.append(outer_shape)
    cell.append(inner_shape)

    transforms.center_at(cell, (x_pos , y_pos+y[c]/2+ y2[c] + y3[c] + y4[c]/2))
    plotter.write(cell)
    if c != len(x)-1:
        x_pos += x4[c]/2 + (x4[c+1]/2)


io.view(plotter)
