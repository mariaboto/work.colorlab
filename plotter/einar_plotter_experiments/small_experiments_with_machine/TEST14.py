from chiplotle import *
import random
import math

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)

g = shapes.group([])
for radius in range(100, 1000, 100):
   a = shapes.arc_circle(radius, 1.0, math.pi)
   g.append(a)

transforms.center_at(g,(3000,3000))
transforms.noise(g, (50,50))
plotter.write(g)

plotter.select_pen(0)
