from chiplotle import *
import random
import math

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
#plotter.select_pen(1)
radius = 200

cell_shape = shapes.group([])

outer_shape = shapes.circle(radius)
inner_shape = shapes.circle(radius*0.7)

cell_shape.append(outer_shape)
cell_shape.append(inner_shape)

pos_x = radius
pos_y = radius

for i in range(1, 80):
    transforms.center_at(cell_shape,(pos_x, pos_y))
    plotter.write(cell_shape)
    pos_x += radius * 2
    if i % 20 == 0:
        pos_y += (radius * 2) - radius * 0.3
        pos_x = radius * 2

# plotter.select_pen(0)
io.view(plotter)
