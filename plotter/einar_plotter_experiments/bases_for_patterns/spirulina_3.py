from chiplotle import *
import random
import math

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)


def spirulina_cell(frequency, amplitude, width):
    #frequency = 6
    #amplitude = 0.3
    #size = 300
    width = width / 2
    angle_stepsize = math.radians(360.000/frequency/24) ### MIGHT NEED SOME OPTIMIZATION!!!
    spirulina_path = shapes.group([])
    coordinates = []
    reverse_coordinates = []
    for i in range (1,3):
        angle = 0.0
        while angle < 2 * math.pi:
                if i == 1:
                    radius = (math.sin(angle * frequency) * amplitude + 1) * width
                else:
                    radius = (math.sin(-angle * frequency) * amplitude + 1) * width
                x = radius * math.cos(angle)
                y = radius * math.sin(angle)
                x_pos = x
                y_pos = y
                if i == 1:
                    coordinates.append((x_pos,y_pos))
                else:
                    reverse_coordinates.append((x_pos,y_pos))
                # circle = shapes.circle(10, segments=18)
                # transforms.center_at(circle,(x_pos,y_pos))
                # wave_circle.append(circle)
                # plotter.write(circle)
                angle += angle_stepsize

    coordinates.append(coordinates[0])
    reverse_coordinates.append(coordinates[0])

    path = shapes.path(coordinates)
    reverse_path = shapes.path(reverse_coordinates)
    spirulina_path.append(path)
    spirulina_path.append(reverse_path)
    return spirulina_path
x_unit = 400 * 0.657 #cm
y_unit = 400 * 0.6187 #cm

posx = 2*x_unit
posy = 3500
for i in range(8):
    hcell = spirulina_cell(random.randint(4,16),random.uniform(0.1,0.4),random.randint(600,750))

    transforms.center_at(hcell,(posx,posy))

    print(hcell.width)
    plotter.write(hcell)
    posx += 4*x_unit

plotter.select_pen(0)
io.view(plotter)
