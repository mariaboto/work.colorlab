import json

f = open("content.md", "r")
content = f.read()

algaes_dicts = []

for algae in content.split("----"):
    algdict = {}
    for line in algae.split("\n"):
        if line.startswith("# "):
            Title = line.replace("# ", "")
            algdict["title"] = Title
            
        if line.startswith("## B"):
            bio_header = line.replace("## Biomass: ", "")
            algdict["b_header"] = bio_header
            
        if line.startswith("## P"):
            pig_header = line.replace("## Pigment: ", "")
            algdict["p_header"] = pig_header
            
        if line.startswith("### B"):
            bio_body = line.replace("### Biomass: ", "")
            algdict["b_body"] = bio_body
            
        if line.startswith("### P"):
            pig_body = line.replace("### Pigment: ", "")
            algdict["p_body"] = pig_body
            
        if line.startswith("#### "):
            main = line.replace("#### ", "")
            algdict["main"] = main

        if line.startswith("##### "):
            list_info = line.replace("##### ", "")
            algdict["list_info"] = list_info
            
    algaes_dicts.append(algdict)

json = json.dumps(algaes_dicts, sort_keys=True, indent=4)
f = open("formatted_content.json", "w")
f.write(json)
f.close()
