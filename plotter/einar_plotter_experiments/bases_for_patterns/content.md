# Spirulina (Arthrospira)
## Biomass: Dry spirulina 
### Biomass: All the pigments included. Because of the different degradation speed, the color changes from green to  blue,  from blue to a yellowish shadow.
## Pigment: Phycocyanin
### Pigment: A blue pigment (different shadows depending of the extraction)
#### Is well-know as the food supplement spirulina, and its main pigment is the blue from the phycocyanin. The only one used in this book that is a cyanobacteria, although it was considered and microalgae until 1962. The application of the whole cell allows seeing a green color due to the chlorophyll. Exposed to the light, the green color (chlorophyll ) will fade becoming firstly turquoise, and then blue (phycocyanin) for eventually disappear leaving a yellowish shadow behind. The blue pigment produces fluorescence.
##### Name: Arthrospira genus (most common species A. Platensis and A. Maxima) newlineType: Cyanobacteria newlineColor: Blue/Green newlinePigment production: Phycocyanin, chlorophylls, carotenes newlineGrowing condition: Marine medium enriched with NaHCO3 to generate an alkali environment. Temperature 25-30°C. 
----
# Chlorella
## Biomass: Dry chlorella
### Biomass: All the pigments included. Because of the different degradation speeds, the color changes from green to blue to yellow. Greener than the Spirulina, the green it will take longer!
## Pigment: Phycocyanin
### Pigment: A Blue pigment (different shadows depending of the extraction)   

#### A round shaped freshwater green microalga with a high chlorophyll content. Very resistant. it has similar  pigments to Arthrospira, but the amount of phycocyanin is lower, so visually it seems a darker green. It was  first described by the Dutch microbiologist and botanist Martinus Willem Beijerinck in 1890.

##### Name: Chlorella vulgaris newlineType: Microalga newlineColor: Blue/Green newlinePigment production: Chlorophylls, phycocyanin newlineGrowing condition: Freshwater medium. Temperature 30°C
----
# Dunaliella Salina
## Biomass: 
### Biomass: 
## Pigment: Carotenes
### Pigment: An orange pigment (different shadows depending of the extraction)
#### The principal pigment of this microalga, carotene, is the same pigment that we can find in other vegetable and animal products. For that reason, the smell is more vegetable than the rest of the microalgae. The carotene production occurs when the microalgae are under stress (light, salinity, lack of nutrients). Together with chlorophyll, this is the most unstable pigment, and can fade in hours if it is directly exposed to sunlight. Discovered at  the beginning of the XIX century, Dunaliella has become a convenient model organism for the study of salt adaptation in algae and organic osmotic balance.

##### Name: Dunaliella salina newlineType: Microalga newlineColor: Green/Orange newlinePigment production: Carotenes, Chlorophylls newlineGrowing condition: Marine medium enriched with NaCl for carotene production. Temperature 25-30°C
----
# Porphyridium purpureum
## Biomass: 
### Biomass: 
## Pigment: Phycoerythrin
### Pigment: A pink  pigment
#### It has a reddish color while growing, but the main pigment is a fluorescent pink color that appears once the cells are broken and get in contact with water. It is easy to cultivate by using sea water. This is a ubiquitous species present in moist terrestrial areas as well as brackish, and marine environments. Describe for the first time in 1965 by Drew, K.M. & Ross, R.

##### Name: Porphyridium purpureum newlineType: Microalga newlineColor: Pink newlinePigment production: Phycoerythrin newlineGrowing condition: Marine medium. Temperature 25-30°C

----
# Nannochloropsis
## Biomass: Dry nannochloropsis
### Biomass: A green color because of the chlorophyll, but it has some carotenes too. This pigment is very unstable.
## Pigment: 
### Pigment: 
#### This microalga has a grass green color derived from chlorophyll, making the pigment very unstable. Exposing the pigment to direct sunlight may suppose its disappearance in hours.

##### Name: Nannochloropsis  sp newlinetype: Microalga newlineColor: Green newlinePigment production: Chlorophllys newlineGrowing condition: Marine medium. Temperature 20°C
----
# Haematococcus pluvialis
## Biomass: 
### Biomass: 
## Pigment: Astaxanthin
### Pigment: A red pigment. This pigment is very unstable.
#### It easy to see this alga in nature (the red color in puddles), but to grow it in the lab for extracting the pigment is not as easy. The easiest way is to grow them green, then remove the nutrients and stress them with light. After a few weeks, the population will change from green to red/orange. Haematococcus pluvialis is able to produce a strong antioxidant, astaxanthin, to protect itself from light, salinity or lack of nutrients.

##### Name: Haematococcus pluvialis newlineType: Microalga newlineColor: Green/Red newlinePigment production: Asthaxantin newlineGrowing condition: Freshwater medium. Temperature 25-30°C (higher range to asta production). To produce the astaxanthin, remove the nitrogen from the growing medium, and expose the cells to light.