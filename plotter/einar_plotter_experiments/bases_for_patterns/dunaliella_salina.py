#Base for Dunaliella Salina

from chiplotle import *
import random
import math

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
plotter.select_pen(1)
triangle_group = shapes.group([])

curved_triangle= shapes.bezier_path([(50,86.6),(100,0),(0,0),(50,86.6)],1)

width = 100
x_1 = 0
y_1 = 0



for t in range(1,8):
    x_2 = x_1 + width/2
    x_3 = x_1 + width
    y_2 = math.sqrt((width**2)-((width/2)**2))
    curved_triangle= shapes.bezier_path([(x_2,y_2),(x_3,y_1),(x_1,y_1),(x_2,y_2)],1)
    transforms.center_at(curved_triangle, (100,100))
    triangle_group.append(curved_triangle)
    # plotter.write(curved_triangle)
    width += 200

# plotter.write(triangle_group)
# transforms.rotate(triangle_group, 90 * (math.pi/180))
rotation = 60
radian = rotation * (math.pi/180)
move_dist = (triangle_group.width)
height_dist = triangle_group.height
x = move_dist
y = height_dist / 2
for d in range(1,60):
    transforms.rotate(triangle_group,radian)
    transforms.center_at(triangle_group,(x,y))
    plotter.write(triangle_group)
    x += move_dist
    if d % 10 == 0:
        y += height_dist
        x = move_dist

# plotter.select_pen(0)
io.view(plotter)
