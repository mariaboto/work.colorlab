#Draws the spirulina pattern

from chiplotle import *
import random
import math

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)
x = 0
y = 250

for c in range(1,401):
    cell = shapes.group([])
    outer_circle_radius = random.randint(150,250)
    outer_circle = shapes.circle(outer_circle_radius)
    transforms.noise(outer_circle, 5)

    # inner_circle_width = random.randint(120,150)
    # inner_circle_height = random.randint(120,150)
    # inner_circle = shapes.ellipse(inner_circle_width, inner_circle_height)
    inner_circle_radius = random.randint(int(outer_circle_radius * 0.3),int(outer_circle_radius * 0.6))
    inner_circle = shapes.circle(inner_circle_radius)
    transforms.noise(inner_circle, 2)

    # transforms_x = random.randint((-int(outer_circle_radius*0.8) + inner_circle_width/2),(int(outer_circle_radius*0.8) - inner_circle_width/2))
    # transforms_y = random.randint((-int(outer_circle_radius*0.8) + inner_circle_height/2),(int(outer_circle_radius*0.8) - inner_circle_height/2))
    transforms_x = random.randint((-int(outer_circle_radius*0.8) + inner_circle_radius),(int(outer_circle_radius*0.8) - inner_circle_radius))
    transforms_y = random.randint((-int(outer_circle_radius*0.8) + inner_circle_radius),(int(outer_circle_radius*0.75) - inner_circle_radius))
    transforms.center_at(inner_circle,(transforms_x, transforms_y))

    cell.append(outer_circle)
    cell.append(inner_circle)
    transforms.center_at(cell,(x + outer_circle_radius, y))
    x += 500
    plotter.write(cell)
    if c % 20 == 0:
        y += 500
        x = 250
    if c % 40 == 0:
        x = 0




plotter.select_pen(0)

io.view(plotter)
