#Draws the spirulina pattern

from chiplotle import *
import random
import math

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)
base_circle = random.randint(150,250) #Generates the center of the cell
prev_rad = base_circle
cell = shapes.group([])
cell.append(shapes.circle(base_circle)) #Adds the center to the shape
for i in range(1,5):
    circle_rad = random.randint(80,200) #Defines the size of the circles surrounding previous layer
    middle_circle = prev_rad + circle_rad #Finds where the midpoint of new circles is located
    c_middle_circle = 2 * math.pi * middle_circle #Calculates the circumference of the middle circle
    amount_of_circles = int(c_middle_circle / (circle_rad * 2)) #Calculates the amount of circles on this layer
    print(amount_of_circles)
    angle = 0.0
    angle_stepsize = math.radians(360.0/amount_of_circles) #Finds the angle between each circle
    circle = shapes.circle(circle_rad) #Defines the circle as a shape
    while angle < 2 * math.pi:
            x = middle_circle * math.cos(angle)
            y = middle_circle * math.sin(angle)
            x_pos = x
            y_pos = y
            circle = shapes.circle(circle_rad, segments=18)
            transforms.center_at(circle,(x_pos,y_pos))
            transforms.noise(circle, 15)
            cell.append(circle)
            # plotter.write(circle)
            angle += angle_stepsize

    # transforms.center_at(circle,(prev_rad + circle_rad,0))
    # plotter.write(circle)
    circle_2 = shapes.circle(prev_rad + circle_rad)
    # plotter.write(circle_2)
    prev_rad += circle_rad*2


transforms.center_at(cell,(5000,5000))
plotter.write(cell)
plotter.select_pen(0)

io.view(plotter)
