from chiplotle import *
import random
import math

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7550A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)
#
# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

def gen_spirulina(amplitude, frequency, width_of_wave, center_of_wave_1,pos_x,pos_y):
    spirulina = shapes.group([])

    # length = 250.0
    # amplitude = 500
    # frequency = 0.5
    # width_of_wave = 500
    # center_of_wave_1 = 3000

    center_of_wave_2 = center_of_wave_1 + width_of_wave
    length = width_of_wave / 2

    angle = math.radians(-90)
    angle_stepsize = (math.radians(18))

    coords = []
    coords_2 = []

    ### Create horizontal wave lines
    # for w in range(11):
    #     # print(math.degrees(angle))
    #     b_coords =  []
    #     for i in range (50):
    #         y = (center_of_wave_1 + length) + length * math.sin(angle)
    #         x1 = i*100
    #         y1 = amplitude * math.sin(frequency * x1) + y
    #         b_coords.append((x1,y1))
    #     basepath = shapes.catmull_path(b_coords, 10)
    #     # transforms.rotate(path,math.radians(rotation))
    #     # print(y)
    #     # plotter.write(basepath)
    #     spirulina.append(basepath)
    #     angle += angle_stepsize


    ### Create inner spiral
    for i in range (50):
        x1 = i*100
        y1 = amplitude * math.sin(frequency * x1) + center_of_wave_1
        x2 = i*100
        y2 = amplitude * math.sin(frequency * x2) + center_of_wave_2
        if i % 4 == 0:
            coords.append((x1,y1))
            coords_2.append((x2,y2))
        elif (i+2) % 4 == 0:
            coords_2.append((x1,y1))
            coords.append((x2,y2))

    wave_coords = coords + list(reversed(coords_2))
    path = shapes.catmull_path(wave_coords,10)
    spirulina.append(path)
    transforms.center_at(spirulina,(pos_x,pos_y))
    return spirulina


for i in range(1,2):
    plotter.write(gen_spirulina(250,1,500,0, 4000, i*500*1.2))

plotter.select_pen(0)
io.view(plotter)
