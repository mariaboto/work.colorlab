[
  {
    "biomass": "DRY ARTHROSPIRA\r\nAll the pigments included. Due to the different degradation speed of the pigments, the color changes from green to blue, and from blue to a light yellow when is applied.",
    "description": "The *Arthrospira* genus is well-known as the food supplement spirulina, and its main pigment is the blue from sits phycocyanin content. *Arthrospira* is the only microorganism used in this book that is a cyanobacterium, although it was considered a microalga until 1962. The application of the whole biomass results in a green color due to chlorophyll. When the alga is exposed to light, the green color (chlorophyll) will fade, becoming firstly turquoise and then blue (phycocyanin) before eventually disappearing, leaving a yellowish shadow behind. The phycocyanin produces fluorescence.",
    "pigment": "PHYCOCYANIN\r\nBlue pigment (different hues depending on the extraction)",
    "properties": "NAME: Arthrospira genus (most common species *A. platensis* and *A. maxima*)\r\nTYPE: Cyanobacteria\r\nCOLOR: Blue/Green\r\nPIGMENT PRODUCTION: Phycocyanin, chlorophylls, carotenes\r\nGROWING CONDITIONS: Marine medium enriched with NaHCO3 to generate an alkaline environment.\r\nTEMPERATURE: 25-30°C",
    "title": "Arthrospira"
  },
  {
    "biomass": "DRY *CHLORELLA*\r\nAll the pigments included. Due to the different degradation speeds of the pigments, the color changes from green to blue, and from blue to a light yellow when is applied.",
    "description": "Round-shaped freshwater microalga with a high chlorophyll content. Its rapid growth, resistance, and flexibility against external factors make it one of the most used by the industry (biodiesel production, food industry, aquaculture, etc.). It has a similar pigment profile to *Arthrospira*, but the amount of phycocyanin is lower, making *Chlorella* looking greener. It was first described by the Dutch microbiologist and botanist Martinus Willem Beijerinck in 1890.",
    "pigment": "PHYCOCYANIN\r\nBlue pigment (different hues depending on the extraction)",
    "properties": "NAME: *Chlorella vulgaris*\r\nTYPE: Microalga\r\nCOLOR: Blue/Green\r\nPIGMENT PRODUCTION: Chlorophylls, phycocyanin\r\nGROWING CONDITIONS: Freshwater medium.\r\nTEMPERATURE: 30°C",
    "title": "Chlorella"
  },
  {
    "description": "The principal pigments of this microalga, carotenes, are the same pigments found in plants. For this reason, this alga recalls the smell of a vegetable more than other microalgae. The carotenes’ production occurs when the microalgae are under stress (light, salinity, lack of nutrients). Together with chlorophyll, carotene is the most unstable pigment and can fade in hours if it is directly exposed to sunlight. Discovered at the beginning of the 19th century, *Dunaliella* has become a model organism for the study of salt adaptation and cellular osmotic balance.",
    "pigment": "CAROTENE\r\nOrange pigment (different hues depending on the extraction). This pigment is very unstable when exposed to light.",
    "properties": "NAME: *Dunaliella salina*\r\nTYPE: Microalga\r\nCOLOR: Green/Orange\r\nPIGMENT PRODUCTION: Carotenes, chlorophylls\r\nGROWING CONDITIONS: Marine medium enriched with NaCl for carotenes production.\r\nTEMPERATURE: 25-30°C",
    "title": "Dunaliella salina"
  },
  {
    "description": "*Porphyridium purpureum* is a red unicellular microalga, previously known as *Porphyridium cruentum*. It is a ubiquitous species found in moist terrestrial and brackish waters. It has a reddish color while growing, but the main pigment is a bright pink (phycoerythrin) that appears once the cells are broken and get in contact with water. Phycoerythrin produces fluorescence.\r\n*Porphyridium purpureum* generates extracellular polysaccharides that cover the surface of the container where the algae are cultured. These polysaccharides are used in industry as thickeners, emulsifiers, or stabilizers because of their gelling properties.",
    "pigment": "PHYCOERYTHRIN\r\nPink pigment",
    "properties": "NAME: *Porphyridium purpureum*\r\nTYPE: Microalga\r\nCOLOR: Pink\r\nPIGMENT PRODUCTION: Phycoerythrin\r\nGROWING CONDITIONS: Marine medium.\r\nTEMPERATURE: 25–30°C",
    "title": "Porphyridium purpureum"
  },
  {
    "biomass": "DRY *NANNOCHLOROPSIS*\r\nThe chlorophylls are responsible for the green color of this alga, but it also contains carotenes and astaxanthin. This pigment is very unstable.",
    "description": "*Nannochloropsis* is mainly a marine microalga, but it also grows in brackish and fresh water. This microalga has a grass-green color derived from chlorophyll, although it has been suggested as a source for other pigments, such as zeaxanthin, canthaxanthin, and astaxanthin. Exposing the pigment to direct sunlight may result  in its disappearance in hours. It is easy to culture this alga in vitro, and this, together with its oil content, makes *Nannochloropsis* an interesting microorganism for biofuel, aquaculture, and pigment production.",
    "properties": "NAME: *Nannochloropsis genus*\r\nTYPE: Microalga\r\nCOLOR: Green\r\nPIGMENT PRODUCTION: Chlorophylls, carotenes\r\nGROWING CONDITIONS: Marine medium. \r\nTEMPERATURE: 20°C",
    "title": "Nannochloropsis"
  },
  {
    "biomass": "DRY *HAEMATOCOCCUS PLUVIALIS*\r\nGreen and red powder rich in chlorophyll and astaxanthin, respectively.",
    "description": "*Haematococcus pluvialis* is able to produce a strong antioxidant, astaxanthin, to protect itself from light, salinity, or lack of nutrients. For this reason, it is easy to find this alga in nature (it is responsible for the red color in puddles), but how to grow it efficiently for use in the industry is still under research. To obtain the astaxanthin, you have to culture these algae in vitro while they are green and then stress the biomass by removing the nutrients and direct light exposure. After a few weeks the population will change from green to red/orange.\r\nFor this publication, synthetic astaxanthin has been used due to the small amount of Haematococcus pluvialis biomass available.",
    "pigment": "ASTAXANTHIN\r\nRed pigment. Very unstable.",
    "properties": "NAME: *Haematococcus pluvialis*\r\nTYPE: Microalga\r\nCOLOR: Green/Red\r\nPIGMENT PRODUCTION: Astaxanthin\r\nGROWING CONDITIONS: Freshwater medium.\r\nTEMPERATURE: 25-30°C (higher range for astaxanthin production). To produce the astaxanthin, remove the nitrogen from the growing medium and expose the cells to light.",
    "title": "HAEMATOCOCCUS PLUVIALIS"
  }
]