# -*- coding: utf-8 -*-
import random
import math
import json
from chiplotle import *
from chiplotle.hpgl import commands
import re
from configureMemory import configureMemory
from instantiate_plotter_on_port import instantiate_plotter_on_port

############################################
## IMPORT THE FUNCTIONS TO DRAW EACH CELL ##
############################################

from shapes.chlorella import chlorella_cell
from shapes.dunaliella_salina import duna_cell
from shapes.nannochloropsis import nanno_cell
from shapes.p_cruentum import p_cell
from shapes.spirulina import spirulina_cell
from shapes.haematococcus import haema_cell

#######################################
## VARIOUS FUNCTIONS TO PROCESS TEXT ##
#######################################

def mm(amount):
    return amount * 40

def cm(amount):
    return amount * 400

def units_to_cm(amount):
    return amount / 400.0

# Doubling effect of the header
def title(text, char_width, char_height, linespace):
    text = text.upper().replace(' ', '\r\n')
    title = shapes.group([])
    for i in range(4):
        title_text = shapes.label(text, charwidth=units_to_cm(char_width), charheight=units_to_cm(char_height), charspace=0, linespace=linespace)
        transforms.center_at(title_text, (i * mm(.8), 0))
        title.append(title_text)
    return title
# Calculate linelength
def line_length(txt, char_width):
    return len(txt) * char_width * 1.3

# Make a plotter label instrucion for the given text line
def make_label (text, char_width, char_height, offset, charspace=None, linespace=None):
    line = shapes.label(text, units_to_cm(char_width), units_to_cm(char_height), charspace, linespace)
    transforms.offset(line, (0, -offset))
    return line

# Transform text lines into plotter labels
def make_labels (lines, charwidth, charheight, charspace=None, linespace=None, offset=None):
    text = '{}{}'.format(chr(10), chr(13)).join(lines)
    label = shapes.label(text, charwidth=units_to_cm(charwidth), charheight=units_to_cm(charheight), charspace=charspace, linespace=linespace)
    if offset:
        transforms.offset(label, offset)
    return label

# # enter word, come back to start + half, insert HT, reinsert word
# # insert 2 spaces, HT, continue
# def add_style_instructions (word, bold=False):
#     if bold:
#         print('BOLD:::{}:::'.format(word))
#         return '{word}{backfeed}{ht}{word}{ht}'.format(word=word, backfeed=(len(word)-1)*chr(8), ht=chr(9))    

#     return word

# Returns a tuple with the word, and a boolean whether it's bold
# if it's bold also return tokens before and after bold sign
# *word*, → ('word,', True)
def add_style_annotation (word):
    m = re.match(r'^(\W?)\*(.+)\*(\W?)$', word)

    if m:
        return ('{}{}{}'.format(m.group(1), m.group(2), m.group(3)), True)
    else:
        return (word, False)

# Changes boldness across multiple word to bold per word.
# *one two three* *one* *two* *three*
def bold_per_word (m):
    return re.sub(r'(\s+)', '*\\1*', m.group(0))

def tokenize_text (text):
    text = re.sub(r'\*[^\*]+\*', bold_per_word, text.replace('\r\n', '\n').replace('\n', ' __newline__ '))
    return [add_style_annotation(w) for w in re.split(r'\s+', text)]

def bufferLength (annotated_words, separator=" ", prefix="", char_width=1):
    if annotated_words:
        separator_length = line_length(separator, char_width)
        length = line_length(prefix, char_width)

        # Loop through words, if they're bold count an extra char
        for word, bold in annotated_words:
            length += line_length(word, char_width)
            if bold:
                length += line_length(word[-1], char_width)

        # add spaces
        length += (len(annotated_words) - 1) * separator_length

        return length
    else:
        return 0

def makeLines (text, width, char_width, prefix = ""):
    lines = []
    buff = []
    separator = " "
    # print(text, tokenize_text(text))
    for token in tokenize_text(text):
        # print(token)
        length = bufferLength(buff, separator, prefix, char_width)
        word_length = bufferLength([token], separator, separator, char_width)

        if (width and length + word_length > width) or token[0] == '__newline__':
            lines.append(buff)
            buff = [token] if token[0] <> '__newline__' else []
        else:
            if '°' in token[0]:
                buff.append((token[0].replace("°", chr(14) + chr(122) + chr(15)), token[1]))
            else:
                buff.append(token)

    if buff:
        lines.append(buff)

    normal = [prefix + separator.join([word for word, _ in line]) for line in lines]
    bold = [prefix + separator.join([word if bold else len(word) * ' ' for word, bold in line]) for line in lines]

    return (normal, bold)

def body(input_text, width, char_width, char_height, line_height, charspace=-.1, linespace=None):
    linesRegular, linesBold = makeLines(input_text, width, char_width)

    return Group ([
        make_labels(linesRegular, charwidth=char_width, charheight=char_height, charspace=charspace, linespace=linespace),
        make_labels(linesBold, charwidth=char_width, charheight=char_height, charspace=charspace, linespace=linespace, offset=(mm(.5), 0))
    ]   )


#######################
## DEFINING THE GRID ##
#######################
def draw_grid (plotter):
    from chiplotle.geometry.shapes.line import line

    plotter.set_origin_bottom_left()
    width = plotter.margins.hard.width
    height = plotter.margins.hard.height

    left = 0
    right = width
    top = height
    bottom = 0

    x_unit = width / 72.0
    y_unit = height / 48.0

    def x(input):
        return input * x_unit

    def y(input):
        return input * y_unit


    g = Group()
    
    ## add horizontal lines
    for i in range(48):
        g.append(line((left, y(i)), (right, y(i))))
    ## add vertical lines

    for i in range(72):
        g.append(line((x(i), top), (x(i), bottom)))

    plotter.select_pen(2)
    plotter.write(g)
    plotter.select_pen(0)

def draw (plotter, pen=1, speed=15, force=3):
    plotter.set_origin_bottom_left()
    width = plotter.margins.hard.width
    height = plotter.margins.hard.height

    print(width, height)

    plotter.select_pen(pen)
    plotter.write(commands.FS(force))
    plotter.write(commands.VS(speed))

    left = 0
    right = width
    top = height
    bottom = 0

    x_unit = width / 72.0
    y_unit = height / 48.0

    def x(input):
        return input * x_unit

    def y(input):
        return input * y_unit

    def rx(input):
        # A relative X value. Made for positioning items on a page(section of the whole poster). The base_x is defined at the beginning of each page.
        return (base_x + input) * x_unit

    def ry(input):
        # A relative y value. Made for positioning items on a page(section of the whole poster). The base_y is defined at the beginning of each page.
        return (base_y + input) * y_unit


    plotter.select_pen(1)

    #########################
    ## DEFINING TEXT SIZES ##
    #########################

    title_char_width = x(0.8) 
    title_char_height = y(2)
    title_line_height = ((y(3))/(y(4)))-1

    big_char_width = x(0.25)
    big_char_height = y(0.42) 
    big_char_line_height = y(2.0 / 3.0) 

    body_char_width = x(0.15)
    body_char_height = y(0.25 )
    body_char_line_height = y(0.5)

    cell_size = int(x(1.8))

    #########################################
    ## IMPORTING THE TEXT FROM A JSON FILE ##
    #########################################

    with open("formatted_content.json", "r") as read_file:
        content = json.load(read_file)

    #######################
    ## DRAWING THE PAGES ##
    #######################

    plotter.write(commands.CA(5)) #Enables alternative characterset so it is possible to draw the degree sign
    plotter.select_pen(1)


    # ############
    # ## PAGE 1 ##
    # ############
    # Arthrospira
    # ############
    base_x = 0
    base_y = 0

    TEXT_POS_VERTICAL = 10
    PIGMENT_X = 9

    title_input = content[0]["title"].encode("utf-8")
    biomass_input = content[0]["biomass"].encode("utf-8")
    pigment_input = content[0]["pigment"].encode("utf-8")
    description_input = content[0]["description"].encode("utf-8")
    properties_input = content[0]["properties"].encode("utf-8")

    title_position = (rx(4), ry(21) - ry(2))
    biomass_position = (rx(1), ry(19) - ry(2))
    pigment_position = (rx(PIGMENT_X), ry(15) - ry(2))
    description_position = (rx(1), ry(TEXT_POS_VERTICAL) - ry(2))
    properties_position = (rx(14), ry(TEXT_POS_VERTICAL) - ry(2))
    shape_position = (rx(22), ry(2) - ry(1))

    title_plot = title(title_input, title_char_width, title_char_height, title_line_height)
    transforms.offset(title_plot, title_position)

    biomass_plot = body(biomass_input, x(18), big_char_width, big_char_height, big_char_line_height)
    transforms.offset(biomass_plot, biomass_position)

    pigment_plot = body(pigment_input, x(10), big_char_width, big_char_height, big_char_line_height)
    transforms.offset(pigment_plot, pigment_position)

    description_plot = body(description_input, x(11), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(description_plot, description_position)

    properties_plot = body(properties_input, x(8), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(properties_plot, properties_position)

    small_shape = spirulina_cell(random.randint(3,6), random.uniform(0.1, 0.3), random.randint(int(x(1.5)), int(x(2))))
    transforms.center_at(small_shape, shape_position)

    plotter.write([title_plot, biomass_plot, pigment_plot, description_plot, properties_plot, small_shape])


    # ############
    # ## PAGE 2 ##
    # ############
    # Chlorella
    # ############

    base_x = 24
    base_y = 0

    title_input = content[1]["title"].encode("utf-8")
    biomass_input = content[1]["biomass"].encode("utf-8")
    pigment_input = content[1]["pigment"].encode("utf-8")
    description_input = content[1]["description"].encode("utf-8")
    properties_input = content[1]["properties"].encode("utf-8")

    title_position = (rx(4), ry(21) - ry(2))
    biomass_position = (rx(1), ry(19) - ry(2))
    pigment_position = (rx(PIGMENT_X), ry(15) - ry(2))
    description_position = (rx(1), ry(TEXT_POS_VERTICAL) - ry(2))
    properties_position = (rx(14), ry(TEXT_POS_VERTICAL) - ry(2))
    shape_position = (rx(22), ry(2) - ry(1))

    title_plot = title(title_input, title_char_width, title_char_height, title_line_height)
    transforms.offset(title_plot, title_position)

    biomass_plot = body(biomass_input, x(18), big_char_width, big_char_height, big_char_line_height)
    transforms.offset(biomass_plot, biomass_position)

    pigment_plot = body(pigment_input, x(10), big_char_width, big_char_height, big_char_line_height)
    transforms.offset(pigment_plot, pigment_position)

    description_plot = body(description_input, x(11), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(description_plot, description_position)

    properties_plot = body(properties_input, x(7), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(properties_plot, properties_position)

    small_shape = chlorella_cell(cell_size, 3)
    transforms.center_at(small_shape, shape_position)

    plotter.write([title_plot, biomass_plot, pigment_plot, description_plot, properties_plot, small_shape])

    # ############
    # ## PAGE 3 ##
    # ############
    # Dunaliella salina
    # ##########

    base_x = 48
    base_y = 0

    title_input = content[2]["title"].encode("utf-8")
    pigment_input = content[2]["pigment"].encode("utf-8")
    description_input = content[2]["description"].encode("utf-8")
    properties_input = content[2]["properties"].encode("utf-8")

    title_position = (rx(4), ry(21) - ry(2))
    pigment_position = (rx(PIGMENT_X), ry(16) - ry(2))
    description_position = (rx(1), ry(TEXT_POS_VERTICAL) - ry(2))
    properties_position = (rx(14), ry(TEXT_POS_VERTICAL) - ry(2))
    shape_position = (rx(22), ry(2) - ry(1))

    title_plot = title(title_input, title_char_width, title_char_height, title_line_height)
    transforms.offset(title_plot, title_position)

    pigment_plot = body(pigment_input, x(10), big_char_width, big_char_height, big_char_line_height)
    transforms.offset(pigment_plot, pigment_position)

    description_plot = body(description_input, x(11), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(description_plot, description_position)

    properties_plot = body(properties_input, x(8), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(properties_plot, properties_position)

    small_shape = duna_cell(cell_size, 4)
    transforms.center_at(small_shape, shape_position)

    plotter.write([title_plot, pigment_plot, description_plot, properties_plot, small_shape])

    # ############
    # ## PAGE 4 ##
    # ############
    # Porphyridium purpureum
    # ############

    base_x = 0
    base_y = 24

    title_input = content[3]["title"].encode("utf-8")
    pigment_input = content[3]["pigment"].encode("utf-8")
    description_input = content[3]["description"].encode("utf-8")
    properties_input = content[3]["properties"].encode("utf-8")

    title_position = (rx(4), ry(21))
    pigment_position = (rx(PIGMENT_X), ry(16))
    description_position = (rx(1), ry(TEXT_POS_VERTICAL))
    properties_position = (rx(14), ry(TEXT_POS_VERTICAL))
    shape_position = (rx(22), ry(2))

    title_plot = title(title_input, title_char_width, title_char_height, title_line_height)
    transforms.offset(title_plot, title_position)

    pigment_plot = body(pigment_input, x(10), big_char_width, big_char_height, big_char_line_height)
    transforms.offset(pigment_plot, pigment_position)

    description_plot = body(description_input, x(11), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(description_plot, description_position)

    properties_plot = body(properties_input, x(10), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(properties_plot, properties_position)

    small_shape = p_cell(int(cell_size * 0.75), cell_size)
    transforms.center_at(small_shape, shape_position)

    plotter.write([title_plot, pigment_plot, description_plot, properties_plot, small_shape])

    # ############
    # ## PAGE 5 ##
    # ############
    # Nannochloropsis
    # ##########

    base_x = 24
    base_y = 24

    title_input = content[4]["title"].encode("utf-8")
    biomass_input = content[4]["biomass"].encode("utf-8")
    description_input = content[4]["description"].encode("utf-8")
    properties_input = content[4]["properties"].encode("utf-8")

    title_position = (rx(4), ry(21))
    biomass_position = (rx(1), ry(19))
    description_position = (rx(1), ry(TEXT_POS_VERTICAL))
    properties_position = (rx(14), ry(TEXT_POS_VERTICAL))
    shape_position = (rx(22), ry(2))

    title_plot = title(title_input, title_char_width, title_char_height, title_line_height)
    transforms.offset(title_plot, title_position)

    biomass_plot = body(biomass_input, x(19), big_char_width, big_char_height, big_char_line_height)
    transforms.offset(biomass_plot, biomass_position)

    description_plot = body(description_input, x(11), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(description_plot, description_position)

    properties_plot = body(properties_input, x(7), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(properties_plot, properties_position)

    small_shape = nanno_cell(cell_size, 4)
    transforms.center_at(small_shape, shape_position)

    plotter.write([title_plot, biomass_plot, description_plot, properties_plot, small_shape])


    # ############
    # ## PAGE 6 ##
    # ############
    # HAEMATOCOCCUS PLUVIALIS
    # ############

    base_x = 48
    base_y = 24

    title_input = content[5]["title"].encode("utf-8")
    biomass_input = content[5]["biomass"].encode("utf-8")
    pigment_input = content[5]["pigment"].encode("utf-8")
    description_input = content[5]["description"].encode("utf-8")
    properties_input = content[5]["properties"].encode("utf-8")

    title_position = (rx(4), ry(21))
    biomass_position = (rx(1), ry(16))
    pigment_position = (rx(PIGMENT_X), ry(13))
    description_position = (rx(1), ry(TEXT_POS_VERTICAL))
    properties_position = (rx(14), ry(TEXT_POS_VERTICAL))
    shape_position = (rx(22), ry(2))

    title_plot = title(title_input, title_char_width, title_char_height, title_line_height)
    transforms.offset(title_plot, title_position)

    biomass_plot = body(biomass_input, x(20), big_char_width, big_char_height, big_char_line_height)
    transforms.offset(biomass_plot, biomass_position)

    pigment_plot = body(pigment_input, x(10), big_char_width, big_char_height, big_char_line_height)
    transforms.offset(pigment_plot, pigment_position)

    description_plot = body(description_input, x(11), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(description_plot, description_position)

    properties_plot = body(properties_input, x(7.5), body_char_width, body_char_height, body_char_line_height)
    transforms.offset(properties_plot, properties_position)

    small_shape = haema_cell(cell_size)
    transforms.center_at(small_shape, shape_position)

    plotter.write([title_plot, biomass_plot, pigment_plot, description_plot, properties_plot, small_shape])

    plotter.select_pen(0)

if __name__ == '__main__':

    ############
    ## FINISH ##
    ############
    DRAW_GRID = False

    #################################
    ## VIRTUAL OR HARDWARE PLOTTER ##
    #################################

    VIRTUAL = False 

    if VIRTUAL:
        from chiplotle.tools.plottertools import instantiate_virtual_plotter
        plotter = instantiate_virtual_plotter(left_bottom = Coordinate(-17300,-11880), right_top = Coordinate(16340,11880), type="DPX-3300")
        plotter.margins.hard.draw_outline()
        plotter.select_pen(1)
        draw(plotter)

    else:
        # plotter = instantiate_plotters()[0]
        plotter = instantiate_plotter_on_port('/dev/ttyUSB0')
        configureMemory(plotter, 9678, 2048, 0, 0, 0, 1024)
        plotter.rotate(0)

        while(True):
            draw(plotter)
            next = str(raw_input('Plot next?'))

    if DRAW_GRID:
        draw_grid(plotter)

    if VIRTUAL:
        io.view(plotter)