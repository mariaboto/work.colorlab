# -*- coding: utf-8 -*-
from chiplotle import *
from chiplotle.hpgl import commands
import random
import math
from configureMemory import configureMemory
from utils import flatten, split_to_pens
from instantiate_plotter_on_port import instantiate_plotter_on_port


"""
    TODO: switches for wich page to print
"""

############################################
## IMPORT THE FUNCTIONS TO DRAW EACH CELL ##
############################################

from shapes.chlorella import chlorella_cell
from shapes.dunaliella_salina import duna_cell
from shapes.nannochloropsis import nanno_cell
from shapes.p_cruentum import p_cell
from shapes.spirulina import spirulina_cell
from shapes.haematococcus import haema_cell

#################################
## VIRTUAL OR HARDWARE PLOTTER ##
#################################


def draw(plotter, pens=[]):

    # plotter._serial_port.write (chr(27) + '.T14337;3072;0;0;0;1024:')
    # print plotter._send_query (chr(27) + '.L')
    # plotter._serial_port.write (chr(27) + '.@14337:')
    # print 'Buffer: ' + str (plotter._buffer_space)
    # plotter.buffer_size = int (plotter._buffer_space / 2)

    #######################
    ## DEFINING THE GRID ##
    #######################

    plotter.set_origin_bottom_left()
    width = plotter.margins.hard.width
    height = plotter.margins.hard.height

    buff = []

    x_unit = 33146 / 72.0
    y_unit = 22378 / 48.0


    def x(input):
        return 327 + (input * x_unit)

    def y(input):
        return 1091 + (input * y_unit)

    def rx(input):
        # A relative X value. Made for positioning items on a page(section of the whole poster). The base_x is defined at the beginning of each page.
        return  327 + ((base_x + input) * x_unit)

    def ry(input):
        # A relative y value. Made for positioning items on a page(section of the whole poster). The base_y is defined at the beginning of each page.
        return 1091 + ((base_y + input) * y_unit)



    #Sizes for the different shapes
    small_size = x(2)
    medium_size = x(4)
    big_size = x(8)

    def size(input):
        return random.randint(int(input*0.75), int(input))

    #######################
    ## DRAWING THE PAGES ##
    #######################


    ############
    ## PAGE 1 ##
    ############

    base_x = 0 
    base_y = 0

    def frequency():
        return random.randint(3,12)
    def amplitude(): 
        return random.uniform(0.1,0.3)

    spirulina_page = [commands.SP(3)]

    title_shape = spirulina_cell(frequency(), amplitude(), size(small_size))
    transforms.center_at(title_shape, (rx(2), ry(22) - ry(0)))

    small_shape_1 = spirulina_cell(frequency(), amplitude(), size(small_size))
    transforms.center_at(small_shape_1, (rx(7), ry(15) - ry(0)))
    small_shape_2 = spirulina_cell(frequency(), amplitude(), size(small_size))
    transforms.center_at(small_shape_2, (rx(15), ry(15) - ry(0)))
    small_shape_3 = spirulina_cell(frequency(), amplitude(), size(small_size))
    transforms.center_at(small_shape_3, (rx(15), ry(7) - ry(0)))

    medium_shape_1 = spirulina_cell(frequency(), amplitude(), size(medium_size))
    transforms.center_at(medium_shape_1, (rx(3), ry(12) - ry(0)))
    medium_shape_2 = spirulina_cell(frequency(), amplitude(), size(medium_size))
    transforms.center_at(medium_shape_2, (rx(11), ry(11) - ry(0)))
    medium_shape_3 = spirulina_cell(frequency(), amplitude(), size(medium_size))
    transforms.center_at(medium_shape_3, (rx(12), ry(3) - ry(0)))

    big_shape = spirulina_cell(frequency(), amplitude(), size(big_size))
    transforms.center_at(big_shape, (rx(5), ry(5) - ry(1)))

    spirulina_page.extend([title_shape, small_shape_1, small_shape_2, small_shape_3, medium_shape_1, medium_shape_2, medium_shape_3, big_shape])
    buff.append(spirulina_page)


    # ############
    # ## PAGE 2 ##
    # ############

    base_x = 24
    base_y = 0


    chlorella_page = [] # shapes.group([])
    chlorella_page.append(commands.SP(6))

    title_shape = chlorella_cell(size(small_size), 3)
    transforms.center_at(title_shape, (rx(2), ry(22) - ry(0)))

    small_shape_1 = chlorella_cell(size(small_size), 3)
    transforms.center_at(small_shape_1, (rx(4), ry(4) - ry(0)))
    small_shape_2 = chlorella_cell(size(small_size), 3)
    transforms.center_at(small_shape_2, (rx(8), ry(11) - ry(0)))
    small_shape_3 = chlorella_cell(size(small_size), 3)
    transforms.center_at(small_shape_3, (rx(23), ry(16) - ry(0)))

    medium_shape_1 = chlorella_cell(size(medium_size), 4)
    transforms.center_at(medium_shape_1, (rx(3), ry(13) - ry(0)))
    medium_shape_2 = chlorella_cell(size(medium_size), 4)
    transforms.center_at(medium_shape_2, (rx(11), ry(3)))
    medium_shape_3 = chlorella_cell(size(medium_size), 4)
    transforms.center_at(medium_shape_3, (rx(14), ry(14) - ry(0)))

    big_shape = chlorella_cell(size(big_size), 5)
    transforms.center_at(big_shape, (rx(18), ry(6) - ry(0)))

    chlorella_page.extend([title_shape, small_shape_1, small_shape_2, small_shape_3, medium_shape_1, medium_shape_2, medium_shape_3, big_shape])

    buff.append(chlorella_page)

    # ############
    # ## PAGE 3 ##
    # ############

    base_x = 48
    base_y = 0

    dunaliella_page = []
    dunaliella_page.append(commands.SP(4))

    title_shape = duna_cell(size(small_size), 4)
    transforms.center_at(title_shape, (rx(2), ry(22) - ry(0)))

    small_shape_1 = duna_cell(size(small_size), 4)
    transforms.center_at(small_shape_1, (rx(7), ry(13) - ry(0)))
    small_shape_2 = duna_cell(size(small_size), 4)
    transforms.center_at(small_shape_2, (rx(13), ry(12) - ry(0)))
    small_shape_3 = duna_cell(size(small_size), 4)
    transforms.center_at(small_shape_3, (rx(17), ry(6) - ry(0)))
    
    medium_shape_1 = duna_cell(size(medium_size), 6)
    transforms.center_at(medium_shape_1, (rx(6), ry(16) - ry(0)))
    medium_shape_2 = duna_cell(size(medium_size), 6)
    transforms.center_at(medium_shape_2, (rx(10), ry(11) - ry(0)))
    medium_shape_3 = duna_cell(size(medium_size), 6)
    transforms.center_at(medium_shape_3, (rx(18), ry(9) - ry(0)))

    big_shape = duna_cell(size(big_size), 8)
    transforms.center_at(big_shape, (rx(12), ry(5) - ry(0)))

    dunaliella_page.extend([title_shape, small_shape_1, small_shape_2, small_shape_3, medium_shape_1, medium_shape_2, medium_shape_3, big_shape])

    buff.append(dunaliella_page)
    buff.append(dunaliella_page)


    # ############
    # ## PAGE 4 ##
    # ############

    base_x = 0
    base_y = 24


    porphyridium_page = []

    porphyridium_page.append(commands.SP(5))

    title_shape = p_cell(int(1.5*x_unit), int(2*x_unit))
    transforms.center_at(title_shape, (rx(2),ry(22)))

    small_shape_1 = p_cell(int(1.5*x_unit), int(2*x_unit))
    transforms.center_at(small_shape_1, (rx(6),ry(4)))
    small_shape_2 = p_cell(int(1.5*x_unit), int(2*x_unit))
    transforms.center_at(small_shape_2, (rx(17),ry(15)))
    small_shape_3 = p_cell(int(1.5*x_unit), int(2*x_unit))
    transforms.center_at(small_shape_3, (rx(21),ry(6)))

    medium_shape_1 = p_cell(int(3*x_unit), int(4*x_unit))
    transforms.center_at(medium_shape_1, (rx(3),ry(13)))
    medium_shape_2 = p_cell(int(3*x_unit), int(4*x_unit))
    transforms.center_at(medium_shape_2, (rx(15),ry(3)))
    medium_shape_3 = p_cell(int(3*x_unit), int(4*x_unit))
    transforms.center_at(medium_shape_3, (rx(20),ry(11)))

    big_shape = p_cell(int(7*x_unit), int(8*x_unit))
    transforms.center_at(big_shape, (rx(12),ry(12)))

    porphyridium_page.extend([title_shape, small_shape_1, small_shape_2, small_shape_3, medium_shape_1, medium_shape_2, medium_shape_3, big_shape])

    buff.append(porphyridium_page)
    buff.append(porphyridium_page)
    buff.append(porphyridium_page)

    # plotter.write(porphyridium_page)


    # ############
    # ## PAGE 5 ##
    # ############

    base_x = 24
    base_y = 24

    nannochloropsis_page = []
    nannochloropsis_page.append(commands.SP(7))

    title_shape = nanno_cell(size(small_size), 4)
    transforms.center_at(title_shape, (rx(2), ry(22)))

    small_shape_1 = nanno_cell(size(small_size), 4)
    transforms.center_at(small_shape_1, (rx(4),ry(8)))
    small_shape_2 = nanno_cell(size(small_size), 4)
    transforms.center_at(small_shape_2, (rx(8),ry(12)))
    small_shape_3 = nanno_cell(size(small_size), 4)
    transforms.center_at(small_shape_3, (rx(12),ry(6)))

    medium_shape_1 = nanno_cell(size(medium_size), 6)
    transforms.center_at(medium_shape_1, (rx(4),ry(14)))
    medium_shape_2 = nanno_cell(size(medium_size), 6)
    transforms.center_at(medium_shape_2, (rx(8),ry(3)))
    medium_shape_3 = nanno_cell(size(medium_size), 6)
    transforms.center_at(medium_shape_3, (rx(12),ry(16)))

    big_shape = nanno_cell(size(big_size), 8)
    transforms.center_at(big_shape, (rx(19),ry(12)))

    nannochloropsis_page.extend([title_shape, small_shape_1, small_shape_2, small_shape_3, medium_shape_1, medium_shape_2, medium_shape_3, big_shape])

    buff.append(nannochloropsis_page)

    # ############
    # ## PAGE 6 ##
    # ############

    base_x = 48
    base_y = 24

    # red → Haematocuccus pluvialis, pen 2
    haematococcus_page = []
    haematococcus_page.append(commands.SP(2))

    title_shape = haema_cell(size(small_size))
    transforms.center_at(title_shape, (rx(2), ry(22)))

    small_shape_1 = haema_cell(size(small_size))
    transforms.center_at(small_shape_1, (rx(11), ry(2)))
    small_shape_2 = haema_cell(size(small_size))
    transforms.center_at(small_shape_2, (rx(12), ry(14)))
    small_shape_3 = haema_cell(size(small_size))
    transforms.center_at(small_shape_3, (rx(20), ry(11)))

    medium_shape_1 = haema_cell(size(medium_size))
    transforms.center_at(medium_shape_1, (rx(10), ry(5)))
    medium_shape_2 = haema_cell(size(medium_size))
    transforms.center_at(medium_shape_2, (rx(15), ry(14)))
    medium_shape_3 = haema_cell(size(medium_size))
    transforms.center_at(medium_shape_3, (rx(18), ry(9)))

    big_shape = haema_cell(size(big_size))
    transforms.center_at(big_shape, (rx(5), ry(12)))

    haematococcus_page.extend([title_shape, small_shape_1, small_shape_2, small_shape_3, medium_shape_1, medium_shape_2, medium_shape_3, big_shape])

    buff.append(haematococcus_page)

    for pen, data in enumerate(split_to_pens(flatten(buff))):
        if pen < len(pens):
            plotter.write([
                commands.SP(pen),
                commands.FS(pens[pen]['force']),
                commands.VS(pens[pen]['speed']),
                data
            ])
        else:
            plotter.write([
                commands.SP(pen),
                data
            ])

    ############
    ## FINISH ##
    ############
    plotter.select_pen(0)


# 1 - Arthrospira, spirulina
# 2 - Chlorella, chlorella_cell
# 3 - Dunaliella Salina, duna_cell
# 4 - Porphyridium Purpureum, p_cell
# 5 - Nannochloropsis, nanno_call
# 6 - Haematococcus Pluvialis, heama_cell

if __name__ == '__main__':
    VIRTUAL = False

    if VIRTUAL:
        from chiplotle.tools.plottertools import instantiate_virtual_plotter
        plotter =  instantiate_virtual_plotter(left_bottom = Coordinate(-17300,-11880), right_top = Coordinate(16340,11880), type="DPX-3300")
        plotter.margins.hard.draw_outline()
        plotter.select_pen(1)
        draw(plotter, [
            { 'pen': 0, 'speed': 0, 'force': 0 }, 
            { 'pen': 1, 'speed': 10, 'force': 3 }, # Black
            { 'pen': 2, 'speed': 10, 'force': 22 }, # red → Haematocuccus pluvialis
            { 'pen': 3, 'speed': 7, 'force': 23 }, # blue → Arthrospira
            { 'pen': 4, 'speed': 4, 'force': 27 }, # orange → Daniella Salina 
            { 'pen': 5, 'speed': 5, 'force': 23 }, # purple → Purpureum
            { 'pen': 6, 'speed': 9, 'force': 27 }, # Chlorella
            { 'pen': 7, 'speed': 6, 'force': 22 }, # Nannnochloropsis
            { 'pen': 8, 'speed': 10, 'force': 18 }
        ])

    else:
        # plotter = instantiate_plotters( )[0]
        plotter = instantiate_plotter_on_port('/dev/ttyUSB2')
        configureMemory(plotter, 9678, 2048, 0, 0, 0, 1024)
        # plotter.write(commands.FS(22))
        # plotter.write(commands.VS(7))
        # plotter.select_pen(1)
        while (True):
            draw(plotter, [
                { 'pen': 0, 'speed': 0, 'force': 0 }, 
                { 'pen': 1, 'speed': 10, 'force': 3 }, # Black
                { 'pen': 2, 'speed': 10, 'force': 22 }, # red → Haematocuccus pluvialis
                { 'pen': 3, 'speed': 9, 'force': 21 }, # blue → Arthrospira
                { 'pen': 4, 'speed': 5, 'force': 23 }, # orange → Daniella Salina 
                { 'pen': 5, 'speed': 5, 'force': 23 }, # purple → Purpureum
                { 'pen': 6, 'speed': 9, 'force': 25 }, # Chlorella
                { 'pen': 7, 'speed': 4, 'force': 22 }, # Nannnochloropsis
                { 'pen': 8, 'speed': 10, 'force': 18 }
            ])
            next = str(raw_input('Plot next?'))

    # draw(plotter, [
    #     { 'pen': 0, 'speed': 0, 'force': 0 }, 
    #     { 'pen': 1, 'speed': 10, 'force': 3 }, # Black
    #     { 'pen': 2, 'speed': 10, 'force': 22 }, # red → Haematocuccus pluvialis
    #     { 'pen': 3, 'speed': 7, 'force': 23 }, # blue → Arthrospira
    #     { 'pen': 4, 'speed': 4, 'force': 27 }, # orange → Daniella Salina 
    #     { 'pen': 5, 'speed': 5, 'force': 23 }, # purple → Purpureum
    #     { 'pen': 6, 'speed': 9, 'force': 27 }, # Chlorella
    #     { 'pen': 7, 'speed': 6, 'force': 22 }, # Nannnochloropsis
    #     { 'pen': 8, 'speed': 10, 'force': 18 }
    # ])

    # draw(plotter, [
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3},
    #     { 'pen': 1, 'speed': 25, 'force': 3}
    # ])


    if VIRTUAL:
        io.view(plotter)