#Base for Nannochloropsis

from chiplotle import *
import math
import random

# from chiplotle.tools.plottertools import instantiate_virtual_plotter
# plotter =  instantiate_virtual_plotter(type="DXY1300")
# plotter.margins.hard.draw_outline()
# plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

def nanno_cell(base, layers):
    cell = shapes.group([])

    x = [base, 0, 0, base]
    y = [0, 0, base, base]
    
    scale = 1
    scale_add = 1.0 / (layers + 1)
    rot = 0 
    rot_add = random.randint(-(72/(layers + 1)), 72/(layers + 1))
    for i in range(layers):
        coordinates = [(x[0],y[0]), (x[1],y[1]), (x[2],y[2]), (x[3],y[3])]
        # cell_shape = shapes.bezier_path(coordinates, 1)
        cell_shape = shapes.path(coordinates)

        transforms.scale(cell_shape, scale)
        transforms.center_at(cell_shape,(0,0))
        transforms.rotate(cell_shape, math.radians(rot))

        cell.append(cell_shape)
        scale -= scale_add
        rot += rot_add
  

    transforms.rotate(cell, math.radians(random.randrange(0,360,90)))

    return(cell)
    
# x_unit = 400 * 0.657 #cm
# y_unit = 400 * 0.6187 #cm

# posx = 2*x_unit
# posy = 3500
# for i in range(1,10):
#     hcell = nanno_cell(750, i)
#     # transforms.rotate(hcell,math.radians(random.randint(0,360)))
#     transforms.center_at(hcell,(posx,posy))
    
#     print(hcell.width)
#     plotter.write(hcell)
#     posx += 4*x_unit

# plotter.select_pen(0)

# io.view(plotter)
