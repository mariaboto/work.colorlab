#Base for Dunaliella Salina

from chiplotle import *
import random
import math

# from chiplotle.tools.plottertools import instantiate_virtual_plotter
# plotter =  instantiate_virtual_plotter(type="HP7550A")
# plotter.margins.hard.draw_outline()
# plotter.select_pen(1)

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)


def duna_cell(total_width , layers):
    triangle_group = shapes.group([])
    # total_width = total_width * 3
    width = total_width / layers
    # print(width)
    # init_width = random.randint(int(width * 0.8), int(width * 1.2))
    init_width = width
    x_1 = 0
    y_1 = 0
    rot = random.randint(-35, 35)
    rotation = math.radians(rot / layers)
    y_dist = random.randint(-int(total_width * 0.05),int(total_width * 0.05))
    for t in range(1,layers + 1):
        x_2 = x_1 + init_width/2
        x_3 = x_1 + init_width
        # print(x_3)
        y_2 = math.sqrt((init_width**2)-((init_width/2)**2))
        curved_triangle= shapes.bezier_path([(x_2,y_2),(x_3,y_1),(x_1,y_1),(x_2,y_2)],1, interpolation_count=12)
        # transforms.rotate(curved_triangle, rotation)
        transforms.center_at(curved_triangle, (100,t*y_dist))
        triangle_group.append(curved_triangle)
        init_width += width
        rotation += math.radians(rot / layers)
    transforms.rotate(triangle_group, math.radians(random.randint(0,360)))
    return triangle_group

# x_unit = 400 * 0.657 #cm
# y_unit = 400 * 0.6187 #cm

# posx = 2*x_unit
# posy = 3500
# for i in range(8):
#     hcell = duna_cell(random.randint(500,750),random.randint(3,5))

#     transforms.center_at(hcell,(posx,posy))

#     print(hcell.width)
#     plotter.write(hcell)
#     posx += 4*x_unit

# plotter.select_pen(0)
# io.view(plotter)
