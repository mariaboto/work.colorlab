#Base for P.cruentum

from chiplotle import *
import math
import random

# from chiplotle.tools.plottertools import instantiate_virtual_plotter
# plotter =  instantiate_virtual_plotter(type="HP7550A")
# plotter.margins.hard.draw_outline()
# plotter.select_pen(1)

def p_cell(min_size, max_size):
    cell = shapes.group([])

    width = random.randint(min_size, max_size)
    height = random.randint(min_size, max_size)

    spiral_radius = (min(width,height) * 0.6) / 2
    turns = random.uniform(2.0,3.5)
    # print(turns)
    outer_shape = shapes.ellipse(width, height)
    inner_shape = shapes.spiral_archimedean(spiral_radius, num_turns=turns, wrapping_constant=1, direction='cw', segments=25)

    cell.append(outer_shape)
    cell.append(inner_shape)

    return cell

# x_unit = 400 * 0.657 #cm
# y_unit = 400 * 0.6187 #cm

# posx = 2*x_unit
# posy = 3500

# for i in range(8):
#     hcell = p_cell(600,750)

#     transforms.center_at(hcell,(posx,posy))

#     print(hcell.width)
#     plotter.write(hcell)
#     posx += 4*x_unit

# io.view(plotter)
