#Draws the spirulina pattern

from chiplotle import *
import random
import math

# from chiplotle.tools.plottertools import instantiate_virtual_plotter
# plotter =  instantiate_virtual_plotter(type="HP7550A")
# plotter.margins.hard.draw_outline()
# plotter.select_pen(1)

# # plotter = instantiate_plotters( )[0]
# # plotter.select_pen(1)

def random_nums(total, n, variant, base_circle):
    numbers = []
    min = (total / n) * (1 - variant)
    max = (total / n) * (1 + variant)
    sum = 0

    sum += base_circle

    for number in range(n-2):
        num = random.randint(int(min),int(max))
        numbers.append(num)
        sum += num
    numbers.append(total-sum)
    random.shuffle(numbers)

    return numbers


def chlorella_cell(size, layers):
    size = size / 2
    base_circle = random.randint((size / (layers + 2)), (size / (layers))) #Generates the center of the cell
    circles = random_nums(size, layers, 0.25, base_circle)
    # print(circles)
    prev_rad = base_circle
    cell = shapes.group([])
    # cell.append(shapes.circle(base_circle)) #Adds the center to the shape

    for i in range(len(circles)):
        circle_rad = circles[i] / 2#Defines the size of the circles surrounding previous layer
        middle_circle = prev_rad + circle_rad #Finds where the midpoint of new circles is located
        c_middle_circle = 2 * math.pi * middle_circle #Calculates the circumference of the middle circle
        amount_of_circles = int(c_middle_circle / (circle_rad * 2)) #Calculates the amount of circles on this layer
        angle = 0.0
        angle_stepsize = math.radians(360.0/amount_of_circles) #Finds the angle between each circle
        circle = shapes.circle(circle_rad) #Defines the circle as a shape
        layer = shapes.group([])
        counter = 0
        while angle <= 2 * math.pi:
                counter += 1
                if counter <= amount_of_circles:
                    x = middle_circle * math.cos(angle)
                    y = middle_circle * math.sin(angle)
                    x_pos = x
                    y_pos = y
                    circle = shapes.circle(circle_rad, segments=8)
                    transforms.center_at(circle,(x_pos,y_pos))
                    layer.append(circle)
                angle += angle_stepsize

        circle_2 = shapes.circle(prev_rad + circle_rad)
        prev_rad += circle_rad*2
        if i > 0:
            cell.append(layer)

    return cell

# x_unit = 400 * 0.657 #cm
# y_unit = 400 * 0.6187 #cm

# posx = 2*x_unit
# posy = 3500
# for i in range(8):
#     hcell = chlorella_cell(750, random.randint(2,4))

#     transforms.center_at(hcell,(posx,posy))

#     print(hcell.width)
#     plotter.write(hcell)
#     posx += 4*x_unit

# plotter.select_pen(0)

# io.view(plotter)
