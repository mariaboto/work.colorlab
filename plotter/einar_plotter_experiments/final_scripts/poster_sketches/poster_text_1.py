import random
import math
import json
from chiplotle import *
from chiplotle.hpgl import commands

##################
##    VIRTUAL   ##
##################

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="DPX330")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

##################
##   HARDWARE   ##
##################

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

################
def mm(amount):
    return amount * 40

def cm(amount):
    return amount * 400

def units_to_cm(amount):
    return amount / 400.0

def title(input_text):
    input_text = input_text.upper()
    if " " in input_text:
        formatted_text = input_text.replace(" ", "\r\n")
        input_text = formatted_text
    title = shapes.group([])
    movement = 0
    for i in range(4):
        title_text = shapes.label(input_text, units_to_cm(0.8 * x_unit), units_to_cm(2 * y_unit),0.2,-0.3)
        transforms.center_at(title_text, (movement,0))
        title.append(title_text)
        movement += mm(0.8)
    return title

def header(input_text):
    input_text = input_text.upper()
    header_text = shapes.label(input_text, units_to_cm(0.7 * x_unit), units_to_cm(0.6 * y_unit), -0.1, 0.4)

    return header_text

def body(input_text, text_box, char_width, char_height):
    text = input_text
    fixed_text = ""
    width_counter = 0

    for word in text.split(" "):
        word = word + " "
        if width_counter + len(word) * (char_width + char_width * 0.25) < text_box:
            fixed_text += word
            width_counter  += len(word) * (char_width + char_width * 0.25)
        else:
            fixed_text += "\r\n" + word
            width_counter = 0
            width_counter += len(word) * (char_width + char_width * 0.25)
    body_text = shapes.label(fixed_text, units_to_cm(char_width), units_to_cm(char_height), -0.1, -0.1)

    return body_text

def list_body(input_text, text_box, char_width, char_height):
    text = input_text
    indent = " "
    fixed_text = ""
    for line in text.split('"newline"'): 
        width_counter = 0
        sentence = line.split(": ")
        identifier = sentence[0].upper() 
        content = indent + sentence[1]
        if len(content) * (char_width + char_width * 0.25) > text_box:
            fixed_content = ""
            for word in content.split(" "):
                word = word + " "
                if width_counter + len(word) * (char_width + char_width * 0.25) < text_box:
                    fixed_content += word
                    width_counter  += len(word) * (char_width + char_width * 0.25)
                else:
                    fixed_content += "\r\n" + indent + word
                    width_counter = 0
                    width_counter += len(word) * (char_width + char_width * 0.25)
            content = fixed_content
        fixed_text += identifier + "\r\n"
        fixed_text += content + "\r\n"
    list_body_text = shapes.label(fixed_text, units_to_cm(char_width), units_to_cm(char_height), -0.1, -0.1)

    return list_body_text

def page_n(input_number):
    page_number = shapes.group([])
    movement = 0
    for i in range(4):
        number_text = shapes.label(input_number, units_to_cm(x_unit), units_to_cm(y_unit),0.2,-0.2)
        transforms.center_at(number_text, (movement,0))
        page_number.append(number_text)
        movement += mm(0.8)
    return page_number

def a4_page(title_input, b_header, p_header, b_body, p_body, text, list_text, page_num):
    page = shapes.group([])

    title_plot = title(title_input)
    transforms.offset(title_plot, (5*x_unit,30*y_unit))

    bio_header = header(b_header)
    pig_header = header(p_header)

    transforms.offset(bio_header, (x_unit, 25*y_unit))
    transforms.offset(pig_header, (8 * x_unit, 17*y_unit))

    bio_body = body(b_body, 20*x_unit, 0.4 * x_unit, 0.45 * y_unit)
    pig_body = body(p_body, 20*x_unit, 0.4 * x_unit, 0.45 * y_unit)

    transforms.offset(bio_body, (x_unit, 24*y_unit))
    transforms.offset(pig_body, (8 * x_unit, 16*y_unit))

    main_text = body(text, 17*x_unit, 0.3 * x_unit, 0.35 * y_unit)
    transforms.offset(main_text, ( 12 *x_unit, 12 * y_unit))

    list_txt = list_body(list_text, 10 * x_unit, 0.3 * x_unit, 0.35 * y_unit)
    transforms.offset(list_txt, ( x_unit, 12 * y_unit))

    page_number = page_n(page_num)
    transforms.offset(page_number, (28 * x_unit, 1 * y_unit))

    page.append(title_plot)
    page.append(bio_header)
    page.append(pig_header)
    page.append(bio_body)
    page.append(pig_body)
    page.append(main_text)
    page.append(list_txt)
    page.append(page_number)
    return page


with open("formatted_content.json", "r") as read_file:
    content = json.load(read_file)

plotter.set_origin_bottom_left()
width = plotter.margins.hard.width
height = plotter.margins.hard.height


print(width)
print(height)
x_unit = ((width/4)/32)
y_unit = ((height/6)/16)

print(x_unit)
print(y_unit)




x_movement = 2 * (32*x_unit)
y_movement = 0


num = 1

for i in content:
    t_title = i["title"].encode('latin-1')
    t_b_header = i["b_header"].encode('latin-1')
    t_p_header = i["p_header"].encode('latin-1')
    t_b_body = i["b_body"].encode('latin-1')
    t_p_body = i["p_body"].encode('latin-1')
    t_main = i["main"].encode('latin-1')
    t_num = str(num)
    list_info = i["list_info"].encode('latin-1')

    page = a4_page(t_title, t_b_header, t_p_header, t_b_body, t_p_body, t_main, list_info, t_num)
    if num < 3:
        transforms.offset(page,(x_movement,y_movement))
        x_movement += (32 * x_unit)

    else:
        y_movement = (64 + 32) * y_unit
        transforms.rotate(page, math.radians(180), pivot=(0,0))    
        transforms.offset(page,(x_movement, y_movement))
        x_movement -= (32 * x_unit)
    num += 1
    plotter.write(page)

plotter.select_pen(0)


io.view(plotter)

