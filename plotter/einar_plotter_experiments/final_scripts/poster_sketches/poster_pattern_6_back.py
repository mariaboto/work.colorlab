from chiplotle import *
import random
import math

from shapes.chlorella import chlorella_cell
from shapes.dunaliella_salina import duna_cell
from shapes.nannochloropsis import nanno_cell
from shapes.p_cruentum import p_cell
from shapes.spirulina import spirulina_cell
from shapes.haematococcus import haema_cell

##################
##    VIRTUAL   ##
##################

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(left_bottom = Coordinate(-17300,-11880), right_top = Coordinate(16340,11880), type="DPX-3300")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

##################
##   HARDWARE   ##
##################

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

################
def random_nums(total, n, variant, base_circle):
    numbers = []
    min = (total / n) * (1 - variant)
    max = (total / n) * (1 + variant)
    sum = 0

    sum += base_circle

    for number in range(n-2):
        num = random.randint(int(min),int(max))
        numbers.append(num)
        sum += num
    numbers.append(total-sum)
    random.shuffle(numbers)

    return numbers

def wrapped_chlorella():
    plotter.select_pen(2)
    return chlorella_cell(random.randint(int(4.5 * y_unit), int(7 * y_unit)), random.randint(3,4))

def wrapped_duna():
    plotter.select_pen(3)
    return duna_cell(random.randint(int(4.5 * y_unit), int(7 * y_unit)), random.randint(3,6))

def wrapped_haema():
    plotter.select_pen(4)
    return haema_cell(random.randint(int(4.5 * y_unit), int(7 * y_unit)))

def wrapped_nanno():
    plotter.select_pen(5)
    return nanno_cell(random.randint(int(4.5 * y_unit), int(7 * y_unit)), 5)

def wrapped_p():
    plotter.select_pen(6)
    return p_cell(int(4.5 * y_unit), int(7 * y_unit))

def wrapped_spirulina():
    plotter.select_pen(7)
    return spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(4.5 * y_unit), int(7 * y_unit)))

def make_combinations(options=[], length=1):
    if length > 1:
        combinations = []
        
        for i in range(len(options) - 1):
            for combination in make_combinations(options[i+1:], length - 1):
                combinations.append([options[i]] + combination)

        return combinations
    else:
        return [[option] for option in options]

def kask():
    logo = shapes.group([])
    half = shapes.group([])
    lower_half = shapes.group([])

    height = 1 * x_unit
    width = 0.8 * y_unit

    vertical_indent = height / 2.0

    k_1 = shapes.label("K", units_to_cm(width), units_to_cm(height),0,0)
    a = shapes.label("A", units_to_cm(width), units_to_cm(height),0,0)
    transforms.offset(a, (1 * y_unit, vertical_indent))
    s = shapes.label("S", units_to_cm(width), units_to_cm(height),0,0)
    transforms.offset(s, (2 * y_unit, vertical_indent))
    k_2 = shapes.label("K", units_to_cm(width), units_to_cm(height),0,0)
    transforms.offset(k_2, (3 * y_unit, 0))

    half.append(k_1)
    half.append(a)
    half.append(s)
    half.append(k_2)
    
    k_3 = shapes.label("K", units_to_cm(width), units_to_cm(height),0,0)
    a_1 = shapes.label("A", units_to_cm(width), units_to_cm(height),0,0)
    transforms.offset(a_1, (1 * y_unit, vertical_indent))
    s_1 = shapes.label("S", units_to_cm(width), units_to_cm(height),0,0)
    transforms.offset(s_1, (2 * y_unit, vertical_indent))
    k_4 = shapes.label("K", units_to_cm(width), units_to_cm(height),0,0)
    transforms.offset(k_4, (3 * y_unit, 0))

    lower_half.append(k_3)
    lower_half.append(a_1)
    lower_half.append(s_1)
    lower_half.append(k_4)
    
    logo.append(half)
    transforms.rotate(lower_half, math.radians(180))
    transforms.offset(lower_half, (3.8 * y_unit,- 0.2 * y_unit))
    logo.append(lower_half)

    border = shapes.ellipse(5.5 * y_unit, 4 * x_unit)
    transforms.offset(border, ((3.8 * y_unit)/2, (-0.2 * y_unit)/2))
    logo.append(border)

    transforms.rotate(logo, math.radians(90))
    transforms.center_at(logo, (0,0))
    return logo

def mm(amount):
    return amount * 40

def cm(amount):
    return amount * 400

def units_to_cm(amount):
    return amount / 400.0

def title(input_text, char_widht, char_height):
    input_text = input_text.upper()
    if " " in input_text:
        formatted_text = input_text.replace(" ", "\r\n")
        input_text = formatted_text
    title = shapes.group([])
    movement = 0
    for i in range(8):
        title_text = shapes.label(input_text, units_to_cm(char_widht), units_to_cm(char_height),0,0)
        transforms.center_at(title_text, (movement,0))
        title.append(title_text)
        movement += (y_unit)/7
    return title

def line_length(txt, char_width):
    return len(txt) * char_width * 1.3

def make_label (text, char_width, char_height, offset):
    line = shapes.label(text, units_to_cm(char_width), units_to_cm(char_height), -.1, 1)
    transforms.offset(line, (0, -offset))
    return line

def make_labels (lines, char_width, char_height, line_height):
    return Group([make_label(line, char_width, char_height, i * line_height) for i, line in enumerate(lines)])


def makeLines (text, width, char_width, prefix = ""):
    lines = []
    buff = prefix
    separator = " "
    
    for word in text.split(" "):
        word += separator 
        
        if line_length(buff + word, char_width) < width:
            buff += word
        else:
            lines.append(buff)
            buff = prefix + word

    if buff:
        lines.append(buff)

    return lines

def body(input_text, width, char_width, char_height, line_height):
    return make_labels(makeLines(input_text, width, char_width), char_width, char_height, line_height)

############################
############################

plotter.set_origin_bottom_left()
width = plotter.margins.hard.width
height = plotter.margins.hard.height

x_unit = width / 10.0 / 8.0
y_unit = height / 8.0 / 8.0 

#############################
### DRAWING THE PATTERNS  ###
#############################


posx = 12 * x_unit
posy = 12 * y_unit

counter = 0

wraps = [wrapped_chlorella, wrapped_duna, wrapped_haema, wrapped_nanno, wrapped_p, wrapped_spirulina]
random.shuffle(wraps)

def get_pos():
    ordered_xpos = [4*x_unit, 12*x_unit, 20*x_unit, 28*x_unit, 36*x_unit, 44*x_unit, 52*x_unit, 60*x_unit] 
    ordered_ypos = [4*y_unit, 12*y_unit, 20*y_unit, 28*y_unit, 36*y_unit, 44*y_unit, 52*y_unit, 60*y_unit]
    
    xpos = random.choice(ordered_xpos)
    ypos = random.choice(ordered_ypos)
    position = (xpos, ypos)

    while position == (60*x_unit, 60*y_unit):
        xpos = random.choice(ordered_xpos)
        ypos = random.choice(ordered_ypos)
        position = (xpos, ypos)
    return position
def generate_positions(number_of_positions):
    
    positions = []
    for i in range(number_of_positions):
        position = get_pos()
        while position in positions:
            position = get_pos()
        else:
            positions.append(position)
    return(positions)



for pattern in wraps:
    items = 16
    positions = generate_positions(items)
    for p in positions:
        shape = pattern()
        transforms.center_at(shape, p) 
        plotter.write(shape)
    
# #############################
# ##### DRAWING THE TEXT ######
# #############################
plotter.select_pen(1)
poster_title = shapes.group([])


def title_splitter(text):
    gapped_title = shapes.group([])
    pos_y = 0 * y_unit
    change = 8 * y_unit 
    for letter in text:
        l = title(letter, 5 * y_unit, 6 * x_unit)
        transforms.rotate(l, math.radians(90))
        transforms.offset(l, (0, pos_y))
        gapped_title.append(l)
        pos_y += change

    return gapped_title


living = title_splitter("Living")
transforms.offset(living, (71 * x_unit, 1 * y_unit))

colors = title_splitter("Colors")
transforms.offset(colors, (79 * x_unit, 1 * y_unit))

# living = title("Living", 3 * y_unit, 6 * x_unit)
# transforms.rotate(living, math.radians(90))
# transforms.offset(living, (71 * x_unit, 1 * y_unit))

# colors = title("Colors", 3 * y_unit, 6 * x_unit)
# transforms.rotate(colors, math.radians(90))
# transforms.offset(colors, (79 * x_unit, 1 * y_unit))

header_title_text = "Colorlab research,"
header_text = "Colorlab research, initiated by Laboratorium with OSP (Einar Andersen, Gijs de Heij & Sarah Magnan)."
body_text = "This poster, leaflet is under Free licence Art Libre and has been generated with a python script. All the files are available on www.osp.kitchen/work/colorlab/. This publication has been plotted with living colors produced by Maria Boto Ordonez within Kask Laboratorium: www.laboratorium.bio."

# h_t_t = body(header_title_text, 23 * y_unit, 0.9 * y_unit, 1 * x_unit, x_unit)
# transforms.rotate(h_t_t, math.radians(90))
# transforms.offset(h_t_t, (66 * x_unit, 33 * y_unit))


h_t = body(header_text, 14 * y_unit, 0.2 * y_unit, 0.4 * x_unit, 1 * x_unit)
transforms.rotate(h_t, math.radians(90))
transforms.offset(h_t, (70 * x_unit, 48 * y_unit))


b_t = body(body_text, 14 * y_unit, 0.2 * y_unit, 0.4 * x_unit, 1 * x_unit)
transforms.rotate(b_t, math.radians(90))
transforms.offset(b_t, (73 * x_unit, 48 * y_unit))



kask_logo = kask()
transforms.center_at(kask_logo, (60 * x_unit, 60 * y_unit))


poster_title.append(living)
poster_title.append(colors)
# poster_title.append(h_t_t)
poster_title.append(h_t)

poster_title.append(b_t)

plotter.write(poster_title)
plotter.write(kask_logo)
io.view(plotter)