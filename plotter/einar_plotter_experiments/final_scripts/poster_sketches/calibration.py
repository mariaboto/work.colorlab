import random
import math
import json
from chiplotle import *
from chiplotle.hpgl import commands

plotter = instantiate_plotters( )[0]
plotter.select_pen(1)

plotter.set_origin_bottom_left()
width = plotter.margins.hard.width
height = plotter.margins.hard.height

cir = shapes.circle(100)
transforms.center_at(cir,(0,0))
plotter.write(cir)
transforms.center_at(cir,(width,0))
plotter.write(cir)
transforms.center_at(cir,(width,height))
plotter.write(cir)
transforms.center_at(cir,(0,height))
plotter.write(cir)

plotter.select_pen(0)
