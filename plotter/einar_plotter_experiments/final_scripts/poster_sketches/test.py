import random
import math
import json
from chiplotle import *
from chiplotle.hpgl import commands

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(type="HP7576A")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

# # text = shapes.label("hello humans", 2, 2) 
# textgroup = shapes.group([])
# text = hpgl.LB("Hello")
# textgroup.append(text)

# plotter.write(hpgl.SL(math.tan(20)))
# plotter.write(textgroup)

# # plotter.write(shapes.label("hello", 2,2))
# # plotter.write(text)
# for i in range(1, 11):
char_height = 2
des = 2.73

line_height = ((des)/(char_height*2.0))-1.0
print line_height
text = shapes.label("HELLO\n\rHELLO\n\rHELLO\n\rHELLO\n\rHELLO\n\rHELLO\n\r", 1, char_height, 0, line_height)
# transforms.center_at(text, (i * 5000, 2000))

plotter.write(text)

# for i in range(11):
#     text = shapes.label("HELLO\n\rHELLO\n\rHELLO\n\rHELLO\n\rHELLO\n\rHELLO\n\r", 1, 2.5, 0, i * 0.1)
#     transforms.center_at(text, (i * 5000, 2000))
#     plotter.write(text)
io.view(plotter)