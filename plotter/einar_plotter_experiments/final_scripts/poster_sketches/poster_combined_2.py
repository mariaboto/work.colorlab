# -*- coding: utf-8 -*-
import random
import math
import json
from chiplotle import *
from chiplotle.hpgl import commands

from shapes.chlorella import chlorella_cell
from shapes.dunaliella_salina import duna_cell
from shapes.nannochloropsis import nanno_cell
from shapes.p_cruentum import p_cell
from shapes.spirulina import spirulina_cell
from shapes.haematococcus import haema_cell

DRAW_GRID = False

##################
##    VIRTUAL   ##
#################

from chiplotle.tools.plottertools import instantiate_virtual_plotter
plotter =  instantiate_virtual_plotter(left_bottom = Coordinate(-17300,-11880), right_top = Coordinate(16340,11880), type="DPX-3300")
plotter.margins.hard.draw_outline()
plotter.select_pen(1)

##################
##   HARDWARE   ##
##################

# plotter = instantiate_plotters( )[0]
# plotter.select_pen(1)

################
def mm(amount):
    return amount * 40

def cm(amount):
    return amount * 400

def units_to_cm(amount):
    return amount / 400.0

def title(input_text):
    input_text = input_text.upper()
    line_height = ((3*y_unit)/(2*y_unit*2))-1
    if " " in input_text:
        formatted_text = input_text.replace(" ", "\r\n")
        input_text = formatted_text
    title = shapes.group([])
    movement = 0
    for i in range(4):
        title_text = shapes.label(input_text, units_to_cm(0.8 * x_unit), units_to_cm(2 * y_unit),0,line_height)
        transforms.center_at(title_text, (movement,0))
        title.append(title_text)
        movement += mm(0.8)
    return title

def header(input_text):
    input_text = input_text.upper()
    header_text = shapes.label(input_text, units_to_cm(0.5 * x_unit), units_to_cm(0.3 * y_unit), -0.1, 0.4)

    return header_text

def line_length(txt, char_width):
    return len(txt) * char_width * 1.3

def make_label (text, char_width, char_height, offset):
    line = shapes.label(text, units_to_cm(char_width), units_to_cm(char_height), -.1, 1)
    transforms.offset(line, (0, -offset))
    return line

def make_labels (lines, char_width, char_height, line_height):
    return Group([make_label(line, char_width, char_height, i * line_height) for i, line in enumerate(lines)])


def makeLines (text, width, char_width, prefix = ""):
    lines = []
    buff = prefix
    separator = " "
    
    for word in text.split(" "):
        word += separator 
        
        if line_length(buff + word, char_width) < width:
            buff += word
        else:
            lines.append(buff)
            buff = prefix + word

    if buff:
        lines.append(buff)

    return lines

def body(input_text, width, char_width, char_height, line_height):
    return make_labels(makeLines(input_text, width, char_width), char_width, char_height, line_height)

def list_body(input_text, text_box, char_width, char_height, line_height):
    lines = []
    text = input_text
    indent = "  "
    degree_symbol = chr(14) + chr(122) + chr(15)
    
    line_spacing = (line_height - (2 * char_height))/float(2 * char_height)

    for line in text.split('"newline"'):
        if "°" in line:
            line = line.replace( "°", degree_symbol)
        sentence = line.split(": ")
        identifier = sentence[0].upper() 
        content = indent + sentence[1]
        
        if line_length(identifier + content, char_width) > text_box:
            tmp = makeLines(identifier + content, text_box, char_width)
            lines.append(tmp[0])
            lines += makeLines("".join(tmp[1:]), text_box, char_width, indent)
        else:
            lines.append(identifier + ":" + content)

    return make_labels(lines, char_width, char_height, line_height)

#############

with open("formatted_content.json", "r") as read_file:
    content = json.load(read_file)


plotter.set_origin_bottom_left()
plotter.write(commands.CA(5))

width = plotter.margins.hard.width
height = plotter.margins.hard.height

left = 0
right = width
top = height
bottom = 0

print(width)
print(height)
x_unit = width / 72.0
y_unit = height / 48.0

print(x_unit)
print(y_unit)



def x (c):
    return c * x_unit

def y (c):
    return c * y_unit

if DRAW_GRID:
    from chiplotle.geometry.shapes.line import line

    g = Group()
    
    ## add horizontal lines
    for i in range(48):
        g.append(line((left, y(i)), (right, y(i))))
    ## add vertical lines

    for i in range(72):
        g.append(line((x(i), top), (x(i), bottom)))

    plotter.select_pen(2)
    plotter.write(g)
    plotter.select_pen(1)


x_movement = 2 * (32*x_unit)
y_movement = 0
base_x = 0
base_y = 0

pigment_char_width = 0.25 * x_unit
pigment_char_height = 0.42 * y_unit
pigment_line_height = 1 * y_unit

main_char_width = 0.15 * x_unit
main_char_height = 0.25 * y_unit
main_char_line_height = 0.5* y_unit

list_char_width = main_char_width
list_char_height = main_char_height
list_char_line_height = 0.5 * y_unit



### PAGE 1 ###
#### TEXT ####
plotter.select_pen(1)


title_input = content[0]["title"].encode("utf-8")
b_header = content[0]["b_header"].encode("utf-8")
p_header = content[0]["p_header"].encode("utf-8")
b_body = content[0]["b_body"].encode("utf-8")
p_body = content[0]["p_body"].encode("utf-8")
text = content[0]["main"].encode("utf-8")
list_text = content[0]["list_info"].encode("utf-8")


spirulina_page_text = shapes.group([])

title_plot = title(title_input)
transforms.offset(title_plot, ((base_x + 4) * x_unit, (base_y + 21) * y_unit))

bio_header = header(b_header)
pig_header = header(p_header)
transforms.offset(bio_header, ((base_x + 1) * x_unit, (base_y + 20) * y_unit))
transforms.offset(pig_header, ((base_x + 13) * x_unit, (base_y + 16) * y_unit))

bio_body = body(b_body, 22 * x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
pig_body = body(p_body, 10 * x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
transforms.offset(bio_body, ((base_x + 1) * x_unit, (base_y + 19) * y_unit))
transforms.offset(pig_body, ((base_x + 13) * x_unit, (base_y + 15) * y_unit))

main_text = body(text, 10 * x_unit, main_char_width, main_char_height, main_char_line_height)
transforms.offset(main_text, ((base_x + 1) * x_unit, (base_y + 13) * y_unit))

list_txt = list_body(list_text, 9 * x_unit, main_char_width, main_char_height, list_char_line_height)
transforms.offset(list_txt, ((base_x + 13) * x_unit, (base_y + 8) * y_unit))

small_shape = spirulina_cell(random.randint(3,6), random.uniform(0.1,0.3), random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape, ((base_x + 22 ) * x_unit,(base_y + 2) * y_unit))

spirulina_page_text.append(title_plot)
spirulina_page_text.append(bio_header)
spirulina_page_text.append(pig_header)
spirulina_page_text.append(bio_body)
spirulina_page_text.append(pig_body)
spirulina_page_text.append(main_text)
spirulina_page_text.append(list_txt)
spirulina_page_text.append(small_shape)

plotter.write(spirulina_page_text)



##############
##############
##############

base_x = 24
base_y = 0

### PAGE 2 ###
#### TEXT ####

plotter.select_pen(1)

title_input = content[1]["title"].encode("utf-8")
b_header = content[1]["b_header"].encode("utf-8")
p_header = content[1]["p_header"].encode("utf-8")
b_body = content[1]["b_body"].encode("utf-8")
p_body = content[1]["p_body"].encode("utf-8")
text = content[1]["main"].encode("utf-8")
list_text = content[1]["list_info"].encode("utf-8")


nannochloropsis_page_text = shapes.group([])

title_plot = title(title_input)
transforms.offset(title_plot, ((base_x + 4) * x_unit, (base_y + 21) * y_unit))

bio_header = header(b_header)
pig_header = header(p_header)
transforms.offset(bio_header, ((base_x + 1) * x_unit, (base_y + 20) * y_unit))
transforms.offset(pig_header, ((base_x + 13) * x_unit, (base_y + 18) * y_unit))

bio_body = body(b_body, 22*x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
pig_body = body(p_body, 10*x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
transforms.offset(bio_body, ((base_x + 1) * x_unit, (base_y + 19) * y_unit))
transforms.offset(pig_body, ((base_x + 13) * x_unit, (base_y + 17) * y_unit))

main_text = body(text, 11*x_unit, main_char_width, main_char_height, main_char_line_height)
transforms.offset(main_text, ((base_x + 1) * x_unit, (base_y + 10) * y_unit))

list_txt = list_body(list_text, 10 * x_unit, main_char_width, main_char_height, list_char_line_height)
transforms.offset(list_txt, ((base_x + 13) * x_unit, (base_y + 7) * y_unit))

small_shape = nanno_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape, ((base_x + 22 ) * x_unit,(base_y + 2) * y_unit))


nannochloropsis_page_text.append(title_plot)
nannochloropsis_page_text.append(bio_header)
nannochloropsis_page_text.append(pig_header)
nannochloropsis_page_text.append(bio_body)
nannochloropsis_page_text.append(pig_body)
nannochloropsis_page_text.append(main_text)
nannochloropsis_page_text.append(list_txt)
nannochloropsis_page_text.append(small_shape)

plotter.write(nannochloropsis_page_text)


##############
##############
##############

base_x = 48
base_y = 0

### PAGE 3 ###
#### TEXT ####
plotter.select_pen(1)

title_input = content[2]["title"].encode("utf-8")
b_header = content[2]["b_header"].encode("utf-8")
p_header = content[2]["p_header"].encode("utf-8")
b_body = content[2]["b_body"].encode("utf-8")
p_body = content[2]["p_body"].encode("utf-8")
text = content[2]["main"].encode("utf-8")
list_text = content[2]["list_info"].encode("utf-8")


dunaliella_page_text = shapes.group([])

title_plot = title(title_input)
transforms.offset(title_plot, ((base_x + 4) * x_unit, (base_y + 21) * y_unit))

bio_header = header(b_header)
pig_header = header(p_header)
transforms.offset(bio_header, ((base_x + 1) * x_unit, (base_y + 20) * y_unit))
transforms.offset(pig_header, ((base_x + 13) * x_unit, (base_y + 18) * y_unit))

bio_body = body(b_body, 22*x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
pig_body = body(p_body, 10*x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
transforms.offset(bio_body, ((base_x + 1) * x_unit, (base_y + 19) * y_unit))
transforms.offset(pig_body, ((base_x + 13) * x_unit, (base_y + 17) * y_unit))

main_text = body(text, 11*x_unit, main_char_width, main_char_height, main_char_line_height)
transforms.offset(main_text, ((base_x + 1) * x_unit, (base_y + 12) * y_unit))

list_txt = list_body(list_text, 10 * x_unit, main_char_width, main_char_height, list_char_line_height)
transforms.offset(list_txt, ((base_x + 13) * x_unit, (base_y + 12) * y_unit))

small_shape = duna_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape, ((base_x + 22 ) * x_unit,(base_y + 2) * y_unit))

dunaliella_page_text.append(title_plot)
dunaliella_page_text.append(bio_header)
dunaliella_page_text.append(pig_header)
dunaliella_page_text.append(bio_body)
dunaliella_page_text.append(pig_body)
dunaliella_page_text.append(main_text)
dunaliella_page_text.append(list_txt)
dunaliella_page_text.append(small_shape)

plotter.write(dunaliella_page_text)



##############
##############
##############

base_x = 0
base_y = 24

### PAGE 4 ###
#### TEXT ####
plotter.select_pen(1)

title_input = content[3]["title"].encode("utf-8")
b_header = content[3]["b_header"].encode("utf-8")
p_header = content[3]["p_header"].encode("utf-8")
b_body = content[3]["b_body"].encode("utf-8")
p_body = content[3]["p_body"].encode("utf-8")
text = content[3]["main"].encode("utf-8")
list_text = content[3]["list_info"].encode("utf-8")


porphyridium_page_text = shapes.group([])

title_plot = title(title_input)
transforms.offset(title_plot, ((base_x + 4) * x_unit, (base_y + 21) * y_unit))

bio_header = header(b_header)
pig_header = header(p_header)
transforms.offset(bio_header, ((base_x + 1) * x_unit, (base_y + 20) * y_unit))
transforms.offset(pig_header, ((base_x + 13) * x_unit, (base_y + 17) * y_unit))

bio_body = body(b_body, 22*x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
pig_body = body(p_body, 10*x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
transforms.offset(bio_body, ((base_x + 1) * x_unit, (base_y + 19) * y_unit))
transforms.offset(pig_body, ((base_x + 13) * x_unit, (base_y + 16) * y_unit))

main_text = body(text, 11*x_unit, main_char_width, main_char_height, main_char_line_height)
transforms.offset(main_text, ((base_x + 1) * x_unit, (base_y + 10) * y_unit))

list_txt = list_body(list_text, 10 * x_unit, main_char_width, main_char_height, list_char_line_height)
transforms.offset(list_txt, ((base_x + 13) * x_unit, (base_y + 8) * y_unit))

small_shape = p_cell(int(1.5*x_unit), int(2*x_unit))
transforms.center_at(small_shape, ((base_x + 22 ) * x_unit,(base_y + 2) * y_unit))

porphyridium_page_text.append(title_plot)
porphyridium_page_text.append(bio_header)
porphyridium_page_text.append(pig_header)
porphyridium_page_text.append(bio_body)
porphyridium_page_text.append(pig_body)
porphyridium_page_text.append(main_text)
porphyridium_page_text.append(list_txt)
porphyridium_page_text.append(small_shape)

plotter.write(porphyridium_page_text)

##############
##############
##############

base_x = 24
base_y = 24

### PAGE 5 ###
#### TEXT ####
plotter.select_pen(1)

title_input = content[4]["title"].encode("utf-8")
b_header = content[4]["b_header"].encode("utf-8")
p_header = content[4]["p_header"].encode("utf-8")
b_body = content[4]["b_body"].encode("utf-8")
p_body = content[4]["p_body"].encode("utf-8")
text = content[4]["main"].encode("utf-8")
list_text = content[4]["list_info"].encode("utf-8")


chlorella_page_text = shapes.group([])

title_plot = title(title_input)
transforms.offset(title_plot, ((base_x + 4) * x_unit, (base_y + 21) * y_unit))

bio_header = header(b_header)
pig_header = header(p_header)
transforms.offset(bio_header, ((base_x + 1) * x_unit, (base_y + 20) * y_unit))
transforms.offset(pig_header, ((base_x + 13) * x_unit, (base_y + 15) * y_unit))

bio_body = body(b_body, 22*x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
pig_body = body(p_body, 10*x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
transforms.offset(bio_body, ((base_x + 1) * x_unit, (base_y + 19) * y_unit))
transforms.offset(pig_body, ((base_x + 13) * x_unit, (base_y + 14) * y_unit))

main_text = body(text, 11*x_unit, main_char_width, main_char_height, main_char_line_height)
transforms.offset(main_text, ((base_x + 1) * x_unit, (base_y + 10) * y_unit))

list_txt = list_body(list_text, 10 * x_unit, main_char_width, main_char_height, list_char_line_height)
transforms.offset(list_txt, ((base_x + 13) * x_unit, (base_y + 10) * y_unit))

small_shape = chlorella_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 3)
transforms.center_at(small_shape, ((base_x + 22 ) * x_unit,(base_y + 2) * y_unit))


chlorella_page_text.append(title_plot)
chlorella_page_text.append(bio_header)
chlorella_page_text.append(pig_header)
chlorella_page_text.append(bio_body)
chlorella_page_text.append(pig_body)
chlorella_page_text.append(main_text)
chlorella_page_text.append(list_txt)
chlorella_page_text.append(small_shape)

plotter.write(chlorella_page_text)



##############
##############
##############

base_x = 48
base_y = 24

### PAGE 6 ###
#### TEXT ####
plotter.select_pen(1)

title_input = content[5]["title"].encode("utf-8")
b_header = content[5]["b_header"].encode("utf-8")
p_header = content[5]["p_header"].encode("utf-8")
b_body = content[5]["b_body"].encode("utf-8")
p_body = content[5]["p_body"].encode("utf-8")
text = content[5]["main"].encode("utf-8")
list_text = content[5]["list_info"].encode("utf-8")


haematococcus_page_text = shapes.group([])

title_plot = title(title_input)
transforms.offset(title_plot, ((base_x + 4) * x_unit, (base_y + 21) * y_unit))

bio_header = header(b_header)
pig_header = header(p_header)
transforms.offset(bio_header, ((base_x + 1) * x_unit, (base_y + 17) * y_unit))
transforms.offset(pig_header, ((base_x + 13) * x_unit, (base_y + 13) * y_unit))

bio_body = body(b_body, 20 * x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
pig_body = body(p_body, 10 * x_unit, pigment_char_width, pigment_char_height, pigment_line_height)
transforms.offset(bio_body, ((base_x + 1) * x_unit, (base_y + 16) * y_unit))
transforms.offset(pig_body, ((base_x + 13) * x_unit, (base_y + 12) * y_unit))

main_text = body(text, 9 * x_unit, main_char_width, main_char_height, main_char_line_height)
transforms.offset(main_text, ((base_x + 1) * x_unit, (base_y + 10) * y_unit))

list_txt = list_body(list_text, 10 * x_unit, main_char_width, main_char_height, list_char_line_height)
transforms.offset(list_txt, ((base_x + 13) * x_unit, (base_y + 7) * y_unit))

small_shape = haema_cell(random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape, ((base_x + 22 ) * x_unit,(base_y + 2) * y_unit))


haematococcus_page_text.append(title_plot)
haematococcus_page_text.append(bio_header)
haematococcus_page_text.append(pig_header)
haematococcus_page_text.append(bio_body)
haematococcus_page_text.append(pig_body)
haematococcus_page_text.append(main_text)
haematococcus_page_text.append(list_txt)
haematococcus_page_text.append(small_shape)

plotter.write(haematococcus_page_text)



##############
##############
##############

base_x = 0
base_y = 0
## PAGE 1 ##
## PATTERNS ##
plotter.select_pen(2)

spirulina_page_pattern = shapes.group([])

title_shape = spirulina_cell(random.randint(3,6), random.uniform(0.1,0.3), random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_1, ((base_x + 7 ) * x_unit,(base_y + 15) * y_unit))
small_shape_2 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_2, ((base_x + 15 ) * x_unit,(base_y + 15) * y_unit))
small_shape_3 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_3, ((base_x + 15 ) * x_unit,(base_y + 7) * y_unit))

medium_shape_1 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_1, ((base_x + 3 ) * x_unit,(base_y + 12) * y_unit))
medium_shape_2 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_2, ((base_x + 11 ) * x_unit,(base_y + 11) * y_unit))
medium_shape_3 = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_3, ((base_x + 12 ) * x_unit,(base_y + 3) * y_unit))

big_shape = spirulina_cell(random.randint(3,12), random.uniform(0.1,0.3), random.randint(int(7*x_unit), int(8*x_unit)))
transforms.center_at(big_shape, ((base_x + 5 ) * x_unit,(base_y + 5) * y_unit))

spirulina_page_pattern.append(title_shape)
spirulina_page_pattern.append(small_shape_1)
spirulina_page_pattern.append(small_shape_2)
spirulina_page_pattern.append(small_shape_3)
spirulina_page_pattern.append(medium_shape_1)
spirulina_page_pattern.append(medium_shape_2)
spirulina_page_pattern.append(medium_shape_3)
spirulina_page_pattern.append(big_shape)

plotter.write(spirulina_page_pattern)

base_x = 24
base_y = 0
## PAGE 2 ##
## PATTERNS ##

plotter.select_pen(3)
nannochloropsis_page_pattern = shapes.group([])

title_shape = nanno_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = nanno_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_1, ((base_x + 4 ) * x_unit,(base_y + 8) * y_unit))
small_shape_2 = nanno_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_2, ((base_x + 8 ) * x_unit,(base_y + 12) * y_unit))
small_shape_3 = nanno_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_3, ((base_x + 12 ) * x_unit,(base_y + 6) * y_unit))

medium_shape_1 = nanno_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_1, ((base_x + 4 ) * x_unit,(base_y + 14) * y_unit))
medium_shape_2 = nanno_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_2, ((base_x + 8 ) * x_unit,(base_y + 3) * y_unit))
medium_shape_3 = nanno_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_3, ((base_x + 12 ) * x_unit,(base_y + 16) * y_unit))

big_shape = nanno_cell(random.randint(int(7*x_unit), int(8*x_unit)), 8)
transforms.center_at(big_shape, ((base_x + 19 ) * x_unit,(base_y + 12) * y_unit))

nannochloropsis_page_pattern.append(title_shape)
nannochloropsis_page_pattern.append(small_shape_1)
nannochloropsis_page_pattern.append(small_shape_2)
nannochloropsis_page_pattern.append(small_shape_3)
nannochloropsis_page_pattern.append(medium_shape_1)
nannochloropsis_page_pattern.append(medium_shape_2)
nannochloropsis_page_pattern.append(medium_shape_3)
nannochloropsis_page_pattern.append(big_shape)

plotter.write(nannochloropsis_page_pattern)

base_x = 48
base_y = 0
## PAGE 3 ##
## PATTERNS ##

plotter.select_pen(4)
dunaliella_page_pattern = shapes.group([])

title_shape = duna_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = duna_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_1, ((base_x + 7 ) * x_unit,(base_y + 13) * y_unit))
small_shape_2 = duna_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_2, ((base_x + 13 ) * x_unit,(base_y + 12) * y_unit))
small_shape_3 = duna_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 4)
transforms.center_at(small_shape_3, ((base_x + 17 ) * x_unit,(base_y + 6) * y_unit))

medium_shape_1 = duna_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_1, ((base_x + 6 ) * x_unit,(base_y + 16) * y_unit))
medium_shape_2 = duna_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_2, ((base_x + 10 ) * x_unit,(base_y + 11) * y_unit))
medium_shape_3 = duna_cell(random.randint(int(3*x_unit), int(4*x_unit)), 6)
transforms.center_at(medium_shape_3, ((base_x + 18 ) * x_unit,(base_y + 9) * y_unit))

big_shape = duna_cell(random.randint(int(7*x_unit), int(8*x_unit)), 8)
transforms.center_at(big_shape, ((base_x + 12 ) * x_unit,(base_y + 5) * y_unit))

dunaliella_page_pattern.append(title_shape)
dunaliella_page_pattern.append(small_shape_1)
dunaliella_page_pattern.append(small_shape_2)
dunaliella_page_pattern.append(small_shape_3)
dunaliella_page_pattern.append(medium_shape_1)
dunaliella_page_pattern.append(medium_shape_2)
dunaliella_page_pattern.append(medium_shape_3)
dunaliella_page_pattern.append(big_shape)

plotter.write(dunaliella_page_pattern)

base_x = 0
base_y = 24
## PAGE 4 ##
## PATTERNS ##

plotter.select_pen(5)
porphyridium_page_pattern = shapes.group([])

title_shape = p_cell(int(1.5*x_unit), int(2*x_unit))
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = p_cell(int(1.5*x_unit), int(2*x_unit))
transforms.center_at(small_shape_1, ((base_x + 6 ) * x_unit,(base_y + 4) * y_unit))
small_shape_2 = p_cell(int(1.5*x_unit), int(2*x_unit))
transforms.center_at(small_shape_2, ((base_x + 17 ) * x_unit,(base_y + 15) * y_unit))
small_shape_3 = p_cell(int(1.5*x_unit), int(2*x_unit))
transforms.center_at(small_shape_3, ((base_x + 21 ) * x_unit,(base_y + 6) * y_unit))

medium_shape_1 = p_cell(int(3*x_unit), int(4*x_unit))
transforms.center_at(medium_shape_1, ((base_x + 3 ) * x_unit,(base_y + 13) * y_unit))
medium_shape_2 = p_cell(int(3*x_unit), int(4*x_unit))
transforms.center_at(medium_shape_2, ((base_x + 15 ) * x_unit,(base_y + 3) * y_unit))
medium_shape_3 = p_cell(int(3*x_unit), int(4*x_unit))
transforms.center_at(medium_shape_3, ((base_x + 20 ) * x_unit,(base_y + 11) * y_unit))

big_shape = p_cell(int(7*x_unit), int(8*x_unit))
transforms.center_at(big_shape, ((base_x + 12 ) * x_unit,(base_y + 12) * y_unit))

porphyridium_page_pattern.append(title_shape)
porphyridium_page_pattern.append(small_shape_1)
porphyridium_page_pattern.append(small_shape_2)
porphyridium_page_pattern.append(small_shape_3)
porphyridium_page_pattern.append(medium_shape_1)
porphyridium_page_pattern.append(medium_shape_2)
porphyridium_page_pattern.append(medium_shape_3)
porphyridium_page_pattern.append(big_shape)

plotter.write(porphyridium_page_pattern)

base_x = 24
base_y = 24

## PAGE 5 ##
## PATTERNS ##

plotter.select_pen(6)

chlorella_page_pattern = shapes.group([])
title_shape = chlorella_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 3)
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = chlorella_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 3)
transforms.center_at(small_shape_1, ((base_x + 4 ) * x_unit,(base_y + 4) * y_unit))
small_shape_2 = chlorella_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 3)
transforms.center_at(small_shape_2, ((base_x + 8 ) * x_unit,(base_y + 11) * y_unit))
small_shape_3 = chlorella_cell(random.randint(int(1.5*x_unit), int(2*x_unit)), 3)
transforms.center_at(small_shape_3, ((base_x + 23 ) * x_unit,(base_y + 16) * y_unit))

medium_shape_1 = chlorella_cell(random.randint(int(3*x_unit), int(4*x_unit)), 4)
transforms.center_at(medium_shape_1, ((base_x + 3 ) * x_unit,(base_y + 13) * y_unit))
medium_shape_2 = chlorella_cell(random.randint(int(3*x_unit), int(4*x_unit)), 4)
transforms.center_at(medium_shape_2, ((base_x + 11 ) * x_unit,(base_y + 3) * y_unit))
medium_shape_3 = chlorella_cell(random.randint(int(3*x_unit), int(4*x_unit)), 4)
transforms.center_at(medium_shape_3, ((base_x + 14 ) * x_unit,(base_y + 14) * y_unit))

big_shape = chlorella_cell(random.randint(int(7*x_unit), int(8*x_unit)), 5)
transforms.center_at(big_shape, ((base_x + 18 ) * x_unit,(base_y + 6) * y_unit))

chlorella_page_pattern.append(title_shape)
chlorella_page_pattern.append(small_shape_1)
chlorella_page_pattern.append(small_shape_2)
chlorella_page_pattern.append(small_shape_3)
chlorella_page_pattern.append(medium_shape_1)
chlorella_page_pattern.append(medium_shape_2)
chlorella_page_pattern.append(medium_shape_3)
chlorella_page_pattern.append(big_shape)

plotter.write(chlorella_page_pattern)

base_x = 48
base_y = 24
## PAGE 6 ##
## PATTERNS ## 

plotter.select_pen(7)
haematococcus_page_pattern = shapes.group([])

title_shape = haema_cell(random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(title_shape, ((base_x + 2 ) * x_unit,(base_y + 22) * y_unit))

small_shape_1 = haema_cell(random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_1, ((base_x + 11 ) * x_unit,(base_y + 2) * y_unit))
small_shape_2 = haema_cell(random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_2, ((base_x + 12 ) * x_unit,(base_y + 14) * y_unit))
small_shape_3 = haema_cell(random.randint(int(1.5*x_unit), int(2*x_unit)))
transforms.center_at(small_shape_3, ((base_x + 20 ) * x_unit,(base_y + 11) * y_unit))

medium_shape_1 = haema_cell(random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_1, ((base_x + 10 ) * x_unit,(base_y + 5) * y_unit))
medium_shape_2 = haema_cell(random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_2, ((base_x + 15 ) * x_unit,(base_y + 14) * y_unit))
medium_shape_3 = haema_cell(random.randint(int(3*x_unit), int(4*x_unit)))
transforms.center_at(medium_shape_3, ((base_x + 18 ) * x_unit,(base_y + 9) * y_unit))

big_shape = haema_cell(random.randint(int(7*x_unit), int(8*x_unit)))
transforms.center_at(big_shape, ((base_x + 5 ) * x_unit,(base_y + 12) * y_unit))

haematococcus_page_pattern.append(title_shape)
haematococcus_page_pattern.append(small_shape_1)
haematococcus_page_pattern.append(small_shape_2)
haematococcus_page_pattern.append(small_shape_3)
haematococcus_page_pattern.append(medium_shape_1)
haematococcus_page_pattern.append(medium_shape_2)
haematococcus_page_pattern.append(medium_shape_3)
haematococcus_page_pattern.append(big_shape)

plotter.write(haematococcus_page_pattern)



plotter.select_pen(0)

io.view(plotter)


