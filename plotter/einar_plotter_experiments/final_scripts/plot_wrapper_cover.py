# -*- coding: utf-8 -*-

from configureMemory import configureMemory
from chiplotle.hpgl import commands
from chiplotle import *
import math

VIRTUAL = False

def mm(amount):
    return amount * 40

def cm(amount):
    return amount * 400

def units_to_cm(amount):
    return amount / 400.0

# Select pen

  # Move to far right
  # Ask to start
      
      # Plot


def kask(): #Draws the KASK logo
  logo = shapes.group([])
  half = shapes.group([])
  lower_half = shapes.group([])

  charheight = mm(4)
  charwidth = mm(2.5)

  vertical_indent = charheight / 2.0

  k_1 = shapes.label("K", units_to_cm(charwidth), units_to_cm(charheight),0,0)
  a = shapes.label("A", units_to_cm(charwidth), units_to_cm(charheight),0,0)
  transforms.offset(a, (1.75 * charwidth, vertical_indent))
  s = shapes.label("S", units_to_cm(charwidth), units_to_cm(charheight),0,0)
  transforms.offset(s, (3.5 * charwidth, vertical_indent))
  k_2 = shapes.label("K", units_to_cm(charwidth), units_to_cm(charheight),0,0)
  transforms.offset(k_2, (5 * charwidth, 0))

  half.append(k_1)
  half.append(a)
  half.append(s)
  half.append(k_2)
  
  k_3 = shapes.label("K", units_to_cm(charwidth), units_to_cm(charheight),0,0)
  a_1 = shapes.label("A", units_to_cm(charwidth), units_to_cm(charheight),0,0)
  transforms.offset(a_1, (1.75 * charwidth, vertical_indent))
  s_1 = shapes.label("S", units_to_cm(charwidth), units_to_cm(charheight),0,0)
  transforms.offset(s_1, (3.5 * charwidth, vertical_indent))
  k_4 = shapes.label("K", units_to_cm(charwidth), units_to_cm(charheight),0,0)
  transforms.offset(k_4, (5 * charwidth, 0))

  lower_half.append(k_3)
  lower_half.append(a_1)
  lower_half.append(s_1)
  lower_half.append(k_4)
  
  logo.append(half)
  transforms.rotate(lower_half, math.radians(180))
  transforms.offset(lower_half, (3.8 * charheight,- 0.2 * charheight))
  logo.append(lower_half)

  border = shapes.ellipse(10 * charwidth, 4.5 * charheight)
  transforms.offset(border, ((3.8 * charheight)/2, (-0.2 * charheight)/2))
  logo.append(border)

  # transforms.rotate(logo, math.radians(90))
  transforms.center_at(logo, (0,0))
  return logo

def conservatorium ():
  charwidth = mm(2.25) * .5
  charheight = mm(3) * .5
  shift = 0.5 * charheight

  parts = ['CON', 'SER', 'VA', 'TO', 'RIUM']

  logo = Group()
  lines = Group()

  outline = shapes.rectangle(16 * 1.5 * charwidth, 7 * 2 * charheight)
  transforms.offset(outline, (7 * 1.5 * charwidth, -2.1 * 2 * charheight))
  logo.append(outline)

  for r in range(6):
    omit = len(parts) - r
    line = Group()
    x = 0
    for p, text in enumerate(parts):
      if p <> omit:
        label = shapes.label(text, charwidth=units_to_cm(charwidth), charheight=units_to_cm(charheight), charspace=0, linespace=0)
        if p % 2 == 1:
          transforms.offset(label, (x, shift))
        else:
          transforms.offset(label, (x, 0))
        line.append(label)

      x += len(text) * charwidth * 1.55

    transforms.offset(line, (0, -charheight * 2 * r))

    lines.append(line)
  
  # transforms.center_at(lines, (0,0))
  # transforms.center_at(outline, (0, 0))
  logo.append(lines)

  # transforms.rotate(logo, math.radians(90))

  return logo

def hogent():
  charwidth = mm(4) * .5
  charheight = mm(6) * .5

  hogent = shapes.label('HO\r\nGENT', charwidth=units_to_cm(charwidth), charheight=units_to_cm(charheight), charspace=0, linespace=0)
  howest = shapes.label('howest', charwidth=units_to_cm(charwidth) * .75, charheight=units_to_cm(charheight), charspace=-.1, linespace=0)
  
  # transforms.offset(hogent, (0, charheight * -1))
  transforms.offset(howest, (0, charheight * -5))

  logo = Group([hogent, howest])

  # transforms.rotate(logo, math.radians(90))
  # transforms.center_at(logo, (0,0))

  return logo


# Transform text lines into plotter labels
def make_labels (lines, charwidth, charheight, charspace=None, linespace=None):
    text = '{}{}'.format(chr(10), chr(13)).join(lines)
    label = shapes.label(text, charwidth=units_to_cm(charwidth), charheight=units_to_cm(charheight), charspace=charspace, linespace=linespace)


    return label

def cover ():
    lines = ['LIVING', 'COLORS']
    charwidth = mm(30)
    charheight = mm(80)

    buff = Group()
    text = Group()

    for l, line in enumerate(lines):
      print(line)
      for i in range(6):
        label = make_labels([line], charwidth, charheight, 0, 0)
        transforms.offset(label, Coordinate(i * mm(1), l * charheight * -1.2))
        text.append(label)
    
    transforms.offset(text, (mm(26), mm(160)))
    # transforms.center_at(buff, (mm(175), mm(175)))
    buff.append(text)

    logo_kask = kask()
    # transforms
    transforms.offset(logo_kask, (mm(217), mm(43.5)))
    buff.append(logo_kask)

    logo_conservatorium = conservatorium()
    transforms.offset(logo_conservatorium, (mm(236), mm(50)))
    buff.append(logo_conservatorium)
    
    logo_hogent = hogent()
    transforms.offset(logo_hogent, (mm(267), mm(50)))
    buff.append(logo_hogent)
    # plotter.write(logo_hogent)
    return buff


if __name__ == '__main__':
  if VIRTUAL:
    from chiplotle.tools.plottertools import instantiate_virtual_plotter
    plotter = instantiate_virtual_plotter(left_bottom = Coordinate(0, 0), right_top = Coordinate(mm(310), mm(310)), type="DPX-3300")
    plotter.margins.hard.draw_outline()

  else:
    plotter = instantiate_plotters()[0]
    configureMemory(plotter, 9678, 2048, 0, 0, 0, 1024)
    plotter.set_origin_bottom_left()

  x = mm(500)
  y = mm(150)


  if VIRTUAL:
      plotter.write(cover())
      io.view(plotter)
  
  else:
      while True:
        plotter.select_pen(1)
        plotter.write('AP0;VS7;FS4;')
        plotter.write('PA{},{};'.format(x,y))
        start = str(raw_input('place cover...'))
        plotter.write(cover())
        plotter.write('PA{},{};'.format(x,y))
        end = str(raw_input('remove cover...'))
        plotter.select_pen(0)
        next = str(raw_input('plot next?'))
