#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
    Script generating the colortest poster.
"""

from chiplotle.hpgl import commands
from chiplotle import instantiate_plotters
from chiplotle.tools.plottertools import instantiate_virtual_plotter
from chiplotle.geometry.core.coordinate import Coordinate
import math

"""
  Return amount of rectangles that fit and space in between them
"""

VIRTUAL = 'virtual'
HARDWARE = 'hardware'

mode = VIRTUAL

plotter = instantiate_virtual_plotter(left_bottom=(-8400,-12360), right_top=(8400, 11400),
    type='HP7576A') if mode == VIRTUAL else instantiate_plotters()[0]

text = "LIVING COLORS"
charwidth = 3
charheight = 4.5
slant = 20 # Slant in angles
colors = 1
offset = 100

for i in range(0, colors):
  plotter.write([
    commands.SP(i+1),
    commands.PA([(i*offset, 0)]),
    commands.SI(charwidth, charheight),
    commands.SL(math.tan(math.radians(slant))),
    commands.LB(text)
])

if mode == VIRTUAL:
    from chiplotle import io
    io.view(plotter)